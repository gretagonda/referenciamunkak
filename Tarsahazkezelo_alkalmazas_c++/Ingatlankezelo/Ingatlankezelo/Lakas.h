#pragma once
#include "IIngatlan.h"
#include <string>

using namespace std;

class Lakas : public IIngatlan
{
public:
	Lakas( double terulet,   int szobakSzama,  int lakok,  int nmAr);
	Lakas( double terulet,   int szobakSzama,  int nmAr);

	virtual bool bekoltozik(int bekoltozok);
	double osszesKoltseg();
	int lakokSzama();
	virtual string toString();

protected:
	double _terulet;
	int _szobaSzam;
	int _lakok = 0;
	int _nmAr;
};

