#include "Garazs.h"

Garazs::Garazs( double terulet,  int nmAr,  bool futottE,  int honapSzam,  bool autoE)
    :_terulet(terulet),
     _nmAr(nmAr),
    _futottE(futottE),
    _honapSzam(honapSzam),
    _autoE(autoE)
{
}
Garazs::Garazs( double terulet,  int nmAr,  bool futottE)
    :_terulet(terulet),
    _nmAr(nmAr),
    _futottE(futottE)
{
}
double Garazs::osszesKoltseg()
{
        return _terulet * _nmAr;
}

double Garazs::mennyibeKerul(int honapokSzama)
{
    if (_futottE == true)
    {
        return honapokSzama * osszesKoltseg() * 1, 5;
    }
    else
    {
        return honapokSzama * osszesKoltseg();
    }
}

bool Garazs::lefoglaltE()
{
    if (_honapSzam > 0 || (_autoE == true))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Garazs::lefoglal(int lefoglaltHonapok)
{
    if (lefoglaltE() != false)
    {
        _honapSzam = lefoglaltHonapok;
        return true;
    }
    else
    {
        return false;
    }
}

string Garazs::toString()
{
    return "Terulet: " + to_string(_terulet) + "\n" + "Negyzetmeterar: " + to_string( _nmAr) +"\n"+"Foglalt honapok szama: " + to_string(_honapSzam) + "\n" + "All benne auto:" + to_string(_autoE);
}

void Garazs::autoKiBeAll()
{
    if (_autoE == true)
    {
        _autoE = false;
    }
    else
    {
        _autoE = true;
    }
}
