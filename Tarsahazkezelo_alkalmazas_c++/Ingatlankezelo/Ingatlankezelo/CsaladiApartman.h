#pragma once
#include "Lakas.h"
class CsaladiApartman : public Lakas
{
public:
	CsaladiApartman( double terulet,   int szobakSzama,  int lakok,  int nmAr,  int gyerekekSzama);
	CsaladiApartman( double terulet,   int szobakSzama,  int nmAr);

	bool gyerekSzuletik();
	bool bekoltozik(int bekoltozok) override;
	string toString() override;
private:
	int _gyerekekSzama = 0;
};

