#pragma once
#include "Lakas.h"
#include "IBerelheto.h"
class Alberlet : public Lakas, public IBerelheto
{
public:
	Alberlet( double terulet,   int szobakSzama,  int lakok,  int nmAr,  int honapSzam);
	Alberlet( double terulet,   int szobakSzama,  int nmAr);
	double mennyibeKerul(int honapokSzama);
	bool lefoglaltE();
	bool lefoglal(int lefoglaltHonapok);
	bool bekoltozik(int bekoltozok) override;
	string toString() override;

private:
	int _honapSzam = 0;
};

