#pragma once
#include "IIngatlan.h"
#include "IBerelheto.h"
#include <string>
using namespace std;
class Garazs : public IIngatlan, public IBerelheto
{
public: 
	Garazs(double terulet,  int nmAr,  bool futottE,  int honapSzam,  bool autoE);
	Garazs( double terulet,  int nmAr,  bool futottE);

	double osszesKoltseg();
	double mennyibeKerul(int honapokSzama);
	bool lefoglaltE();
	bool lefoglal(int lefoglaltHonapok);
	string toString();
	void autoKiBeAll();

private:
	double _terulet;
	int _nmAr;
	bool _futottE;
	int _honapSzam = 0;
	bool _autoE=false;
};

