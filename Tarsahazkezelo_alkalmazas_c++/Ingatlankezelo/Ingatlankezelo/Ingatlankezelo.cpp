#include <iostream>
#include "Hazmester.h"
#include "Lakas.h"
#include "Alberlet.h"
#include "CsaladiApartman.h"
#include "Garazs.h"
#include "Tarsashaz.h"

int main()
{
    Hazmester h("file.txt");
    h.karbantart();

    cout << endl;
    cout << "Lakas: " << endl;
    Lakas l1(11.2,2,500);
    cout << "koltozes: " << boolalpha << l1.bekoltozik(1000)<<endl;
    cout << "koltseg: " << l1.osszesKoltseg() << endl;
    cout << l1.toString() << endl;

    cout << endl;
    cout << "Alberlet: " << endl;
    Alberlet a1(50.6,3,894);
    cout << "Sikerult-e foglalni: " << a1.lefoglal(8) << endl;
    cout << "Sikerult-e koltozni: " << a1.bekoltozik(3) << endl;
    cout <<"Foglalt-e: "<< a1.lefoglaltE() << endl;;
    cout << "Ara: " << a1.mennyibeKerul(8) << endl;
    cout << "Osszes koltseg: " << a1.osszesKoltseg() << endl;
    cout<< a1.toString() << endl;

    cout << endl;
    cout << "Csaladi Apartman -  1 - gyereknelkul: " << endl;
    CsaladiApartman cs1(150.5,6,1000);
    cout << "Sikerult-e koltozni: " << cs1.bekoltozik(8) << endl;
    cout << "Szuletett-e gyerek: " << cs1.gyerekSzuletik() << endl;
    cout << "Osszes koltseg: " << cs1.osszesKoltseg() << endl;
    cout << cs1.toString() << endl;

    cout << endl;
    cout << "Csaladi Apartman - 2 - gyerekkel: " << endl;
    CsaladiApartman cs2(150.5, 6, 6,800,4);
    cout << "Sikerult-e koltozni: " << cs2.bekoltozik(8) << endl;
    cout << "Szuletett-e gyerek: " << cs2.gyerekSzuletik() << endl;
    cout <<"Osszes koltseg: "<< cs2.osszesKoltseg() << endl;
    cout << cs1.toString() << endl;

    cout << endl;
    cout << "Garazs:  " << endl;
    Garazs g1(10.2,5,true);
    g1.autoKiBeAll();
    cout << "All-e auto: "<< g1.lefoglaltE() << endl;
    cout<<"Ara: "<< g1.mennyibeKerul(5)<<endl;
    cout<<g1.toString()<<endl;

    cout << endl;
    cout << "Tarsashaz:  " << endl;
    Tarsashaz t2(5,5);
    t2.garazsHozzaad(g1);
    t2.lakasHozzaad(a1);
    t2.lakasHozzaad(cs1);
    t2.lakasHozzaad(cs2);
    cout<<"Osszes lako:"<<t2.osszesLako()<<endl;
    cout<< "Ingatlan ertek: " << t2.ingatlanErtek() << endl;
}

