#include "Hazmester.h"
#include "Tarsashaz.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include "Alberlet.h"
#include "CsaladiApartman.h"
#include <string>
using namespace std;

Hazmester::Hazmester(string file)
	: _file(file)
{
}

void Hazmester::karbantart()
{
	Tarsashaz t1(100,100);
	ifstream inputFile(_file);
	if (inputFile)
	{
		while (!inputFile.eof())
		{
			string s;
			getline(inputFile, s);
			istringstream iss(s);
			
			
			string osztaly;
			iss >> osztaly;
			if (osztaly == "Alberlet")
			{

				/*string t;
				iss >> t;
				double ter = stod(t);
				string sz;
				iss >> sz;
				int szobak = stoi(sz);
				string nm;
				iss >> nm;
				int nmar = stoi(nm);*/
				double d;
				iss >> d;
				int i;
				iss >> i;
				int i2;
				iss >> i2;
				Alberlet a(d, i, i2);
				t1.lakasHozzaad(a);
			}

			if (osztaly == "CsaladiApartman")
			{
				double d;
				iss >> d;
				int i;
				iss >> i;
				int i2;
				iss >> i2;
				CsaladiApartman cs(d, i, i2);
				t1.lakasHozzaad(cs);
			}
			if (osztaly == "Garazs")
			{
				double d;
				iss >> d;

				int i;
				iss >> i;

				string f;
				bool futes;
				iss >> f;
				if (f == "futott")
				{
				
					futes = true;
				}
				else
				{
					futes = false;
				}
				Garazs g(d, i, futes);
				t1.garazsHozzaad(g);
			}
		}

	}
	else
	{
		cout << "file is not exist" << endl;
	}

	double ertek = t1.osszertek();
	cout <<"Ertek: "+  to_string(ertek) << endl;
	inputFile.close();

}
