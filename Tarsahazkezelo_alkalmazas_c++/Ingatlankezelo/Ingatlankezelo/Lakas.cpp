#include "Lakas.h"

Lakas::Lakas( double terulet,   int szobakSzama,  int lakok,  int nmAr) :
	_terulet(terulet),
	_szobaSzam(szobakSzama),
	_lakok(lakok),
	_nmAr(nmAr)
{
}
Lakas::Lakas( double terulet,   int szobakSzama,  int nmAr) :
	_terulet(terulet),
	_szobaSzam(szobakSzama),
	_nmAr(nmAr)
{
}
bool Lakas::bekoltozik(int bekoltozok)
{
	return false;
}
double Lakas::osszesKoltseg()
{
	return _terulet * _nmAr;
}
int Lakas::lakokSzama()
{
	return _lakok;
}

string Lakas::toString()
{
	return "Terulet: " + to_string(_terulet) + "\n" + "Szobak szama: " + to_string(_szobaSzam) + "\n" + "Lakok szama: " + to_string( _lakok) + "\n"+ "Negyzetmeterar: " + to_string(_nmAr);
}