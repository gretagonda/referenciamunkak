#include "Tarsashaz.h"

Tarsashaz::Tarsashaz(const int maxLakas, const int maxGarazs)
	:_maxLakas(maxLakas),
	_maxGarazs(maxLakas)
{
	_lakasok.reserve(_maxLakas);
	_garazsok.reserve(maxLakas);
}

bool Tarsashaz::lakasHozzaad(Lakas l)
{
	int s = _lakasok.size();
	if (s < _maxLakas)
	{
		_lakasok.push_back(l);
		return true;
	}
	else
	{
		return false;
	}
}

bool Tarsashaz::garazsHozzaad(Garazs g)
{
	int s = _garazsok.size();
	if (s < _maxGarazs)
	{
		_garazsok.push_back(g);
		return true;
	}
	else
	{
		return false;
	}
}

int Tarsashaz::osszesLako()
{
	int osszesLako = 0;
	int lakasokSzama = _lakasok.size();
	for (int i = 0; i < lakasokSzama; i++) {
		osszesLako = osszesLako + _lakasok[i].lakokSzama();
	}
	return osszesLako;
}

double Tarsashaz::ingatlanErtek()
{
	double ertek = 0;
	int lakasokSzama = _lakasok.size();
	int garazsokSzama = _garazsok.size();
	for (int i = 0; i < lakasokSzama; i++) {
		if (_lakasok[i].lakokSzama() >= 1)
		{
			int e = _lakasok[i].osszesKoltseg();
			ertek = ertek + e;
		}
	}
	for (int i = 0; i < garazsokSzama; i++) {
		if (_garazsok[i].lefoglaltE()==1)
		{
			int e = _garazsok[i].osszesKoltseg();
			ertek = ertek + e;
		}
	}
	return ertek;
	
}

int Tarsashaz::osszertek()
{
	int ertek = 0;
	int lakasokSzama = _lakasok.size();
	int garazsokSzama = _garazsok.size();
	for (int i = 0; i < lakasokSzama; i++) {
			int e = _lakasok[i].osszesKoltseg();
			ertek = ertek + e;
	}
	for (int i = 0; i < garazsokSzama; i++) {
			int e = _garazsok[i].osszesKoltseg();
			ertek = ertek + e;
	}
	return ertek;
}
