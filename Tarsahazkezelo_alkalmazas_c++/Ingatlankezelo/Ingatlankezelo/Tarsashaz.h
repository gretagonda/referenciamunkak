#pragma once
#include "IBerelheto.h"
#include <vector>
#include "Lakas.h"
#include "Garazs.h"
class Tarsashaz
{
public: 
	Tarsashaz(const int maxLakas, const int maxGarazs);
	bool lakasHozzaad(Lakas l);
	bool garazsHozzaad(Garazs g);
	int osszesLako();
	double ingatlanErtek();
	int osszertek();

private:
	vector<Lakas> _lakasok;
	vector<Garazs> _garazsok;
	//vector<IBerelheto> kollekcio;
	int _maxLakas=10;
	int _maxGarazs=10;
};

