#include "Alberlet.h"
#include <string>
using namespace std;

Alberlet::Alberlet( double terulet,  int szobakSzama,  int lakok,  int nmAr,  int honapSzam)
	:Lakas(terulet, szobakSzama, lakok, nmAr),
	_honapSzam(honapSzam)
{
}

Alberlet::Alberlet( double terulet,  int szobakSzama,  int nmAr)
	: Lakas(terulet, szobakSzama, nmAr)
{
}

double Alberlet::mennyibeKerul(int honapokSzama)
{
	if (_lakok >= 1)
	{
		return osszesKoltseg() / lakokSzama();
	}
	else
	{
		return 0;
	}
}

bool Alberlet::lefoglaltE()
{
	return _honapSzam != 0;
}

bool Alberlet::lefoglal(int lefoglaltHonapok)
{
	if (lefoglaltE() == 1)
	{
		_honapSzam = lefoglaltHonapok;
		return true;
	}
	else
	{
		return false;
	}
}

bool Alberlet::bekoltozik(int bekoltozok)
{
	if (((bekoltozok / _szobaSzam) <= 8.0) && ((_terulet / bekoltozok) >= 2.0))
	{
		_lakok = bekoltozok;
		return true;
	}
	else
	{
		return false;
	}
		
}

string Alberlet::toString()
{
	return Lakas::toString() + "\n" + "Honapszam: " + to_string(_honapSzam) + "\n";
}
