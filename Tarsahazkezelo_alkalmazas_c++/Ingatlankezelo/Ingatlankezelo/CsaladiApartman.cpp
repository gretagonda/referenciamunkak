#include "CsaladiApartman.h"

CsaladiApartman::CsaladiApartman( double terulet,  int szobakSzama,  int lakok,  int nmAr,  int gyerekekSzama)
	:Lakas(terulet, szobakSzama, lakok, nmAr),
	_gyerekekSzama(gyerekekSzama)
{
}
CsaladiApartman::CsaladiApartman(double terulet, int szobakSzama, int nmAr)
	: Lakas(terulet, szobakSzama, nmAr)
{
}

bool CsaladiApartman::gyerekSzuletik()
{
	if ((_lakok - _gyerekekSzama) >= 2)
	{
		
		_lakok = _lakok + 1;
		_gyerekekSzama++;
		return true;
	}
	else
	{
		return false;
	}
}

bool CsaladiApartman::bekoltozik(int bekoltozok)
{
	int fo = (_gyerekekSzama * 2) + (bekoltozok - _gyerekekSzama);
	double ter = (_terulet -  ((double)_gyerekekSzama * 5));
	if (((fo / _szobaSzam) <= 2.0) && ((ter / (bekoltozok - (double)_gyerekekSzama)) >= 10.0))
	{
		_lakok = bekoltozok;
		return true;
	}
	else
	{
		return false;
	}
}

string CsaladiApartman::toString()
{
	return Lakas::toString() + "\n" + "Gyerekek szama: " + to_string( _gyerekekSzama);
}
