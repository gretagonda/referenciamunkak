CREATE TABLE Helyszin(
Helyszin_ID number(10),
Nev varchar2(50),
Cim varchar2(50),
CONSTRAINT helyszin_pk PRIMARY KEY(Helyszin_ID)
);

CREATE TABLE Korhaz(
Korhaz_ID number(10),
Nev varchar2(50),
Cim varchar2(50),
CONSTRAINT korhaz_pk PRIMARY KEY(Korhaz_ID)
);


CREATE TABLE Alkalmazott(
Nev varchar2(50),
Alkalmazott_ID number(10),
Beosztas varchar2(50),
Munkahelyszin number(10),
CONSTRAINT alkalmazott_pk PRIMARY KEY(Alkalmazott_ID),
CONSTRAINT alkalmazott_fk FOREIGN KEY(Munkahelyszin) REFERENCES Helyszin(Helyszin_ID)
);

CREATE TABLE Donor_jelolt (
Nev varchar2(50),
Szuletesi_datum date,
Allando_lakcim varchar2(50),
Tajszam number(9),
Orvos_azonosito number(10),
Alkalmas_e number(1),
CONSTRAINT donorjelolt_pk PRIMARY KEY(Tajszam),
CONSTRAINT donorjelolt_fk FOREIGN KEY(Orvos_azonosito) REFERENCES Alkalmazott(Alkalmazott_ID),
CONSTRAINT donorjelolt_ck1 CHECK (Alkalmas_e=0 or Alkalmas_e=1)
);

CREATE TABLE Donor(
Donor_ID number(10),
Tajszam number(9),
Utolso_veradas_datuma date,
Apolo_azonosito number(10),
Vercsoport varchar2(10),
Regisztralt_helyszin number(10),
CONSTRAINT donor_pk PRIMARY KEY(Donor_ID),
CONSTRAINT donor_fk1 FOREIGN KEY(Tajszam) REFERENCES Donor_jelolt(Tajszam),
CONSTRAINT donor_fk2 FOREIGN KEY(Apolo_azonosito) REFERENCES Alkalmazott(Alkalmazott_ID),
CONSTRAINT donor_fk3 FOREIGN KEY(Regisztralt_helyszin) REFERENCES Helyszin(Helyszin_ID),
CONSTRAINT donor_ck1 CHECK (Vercsoport='0 Rh-' or Vercsoport='0 Rh+' or Vercsoport='A Rh-' or Vercsoport='A Rh+' or Vercsoport='B Rh-' or Vercsoport='B Rh+' or Vercsoport='AB Rh-' or Vercsoport='AB Rh+')
);

CREATE TABLE Recipiens(
Nev varchar2(50),
Szuletesi_datum date,
Allando_lakcim varchar2(50),
Tajszam number(9),
CONSTRAINT recipien_pk PRIMARY KEY(Tajszam)
);

CREATE TABLE Verkeszitmeny(
Verkeszitmeny_ID number(10),
Donor number(10),
Lejarat_datuma date,
Fajtaja varchar2(20),
Cel_korhaz number(10),
Recipiens number(10),
CONSTRAINT verkeszitmeny_pk PRIMARY KEY(Verkeszitmeny_ID),
CONSTRAINT verkeszitmeny_fk1 FOREIGN KEY(Donor) REFERENCES Donor(Donor_ID),
CONSTRAINT verkeszitmeny_fk2 FOREIGN KEY(Cel_korhaz) REFERENCES Korhaz(Korhaz_ID),
CONSTRAINT verkeszitmeny_fk3 FOREIGN KEY(Recipiens) REFERENCES Recipiens(Tajszam),
CONSTRAINT verkeszitmeny_ck CHECK (Fajtaja='vvt-koncentratum' or Fajtaja='thr-koncentratum' or Fajtaja='ffp-koncentratum')
);




INSERT INTO Helyszin
VALUES (1111111111, 'Elméleti Orvostudományi központ','1094 Budapest, Tűzoltó u. 37');
INSERT INTO Helyszin
VALUES (1111111112, 'Heim Pál Kórház','1089 Budapest, Üllői út 86');
INSERT INTO Helyszin
VALUES (1111111113, 'Délpesti Területi Vérellátó Intézet','1204 Budapest, Köves út. 2-4');
INSERT INTO Helyszin
VALUES (1111111114, 'Újpesti Ifjúsági Ház','1042 Budapest, István út 17-19.');
INSERT INTO Helyszin
VALUES (1111111115, 'Sugár Üzletközpont','1148 Budapest, Örs vezér tere 24.');
INSERT INTO Helyszin
VALUES (1111111116, 'Corvin Mozi ','1082 Budapest, Corvin köz 1.');
INSERT INTO Helyszin
VALUES (1111111117, 'Salgótarjáni Területi Vérellátó Intézet','3100 Salgótarján, Füleki út 54.');
INSERT INTO Helyszin
VALUES (1111111118, 'Magyarnándor Művelődési Ház','2694 Magyarnándor, Fő út 72.');
INSERT INTO Helyszin
VALUES (1111111119, 'Salgótarjáni Területi Vérellátó Intézet','3100 Salgótarján, Füleki út 54.');
INSERT INTO Helyszin
VALUES (1111111110, 'Kincsesbánya-Ifjúsági Klub','8044 Kincsesbánya, Kincses u. 39.');


INSERT INTO Korhaz
VALUES(2222222220,'Almási Balogh Pál Kórház','3600 Ózd, Béke u. 1.');
INSERT INTO Korhaz
VALUES(2222222221,'Békés Megyei Központi Kórház','5700 Gyula, Semmelweis u. 1.');
INSERT INTO Korhaz
VALUES(2222222222,'Csolnoky Ferenc Kórház','8200 Veszprém, Kórház u. 1.');
INSERT INTO Korhaz
VALUES(2222222223,'Dr. Kenessey Albert Kórház-Rendelőintézet','2660 Balassagyarmat, Rákóczi u. 125-127.');
INSERT INTO Korhaz
VALUES(2222222224,'Fejér Megyei Szent György Egyetemi Oktató Kórház','8000 Székesfehérvár, Seregélyesi út 3.');
INSERT INTO Korhaz
VALUES(2222222225,'Jahn Ferenc Dél-Pesti Kórház és Rendelőintézet','1204 Budapest, Köves u. 1.');
INSERT INTO Korhaz
VALUES(2222222226,'Jászberényi Szent Erzsébet Kórház','5100 Jászberény, Szelei út 2.');
INSERT INTO Korhaz
VALUES(2222222227,'Károlyi Sándor Kórház','1041 Budapest, Nyár u. 103.');
INSERT INTO Korhaz
VALUES(2222222228,'Selye János Kórház','Komárom, Széchenyi István utca 2., 2900');
INSERT INTO Korhaz
VALUES(2222222229,'Siófoki Kórház-Rendelőintézet','8600 Siófok, Semmelweis u.1.');
INSERT INTO Korhaz
VALUES(2222222230,'Uzsoki utcai Kórház','1145 Budapest, Uzsoki utca 29-41.');

INSERT INTO Alkalmazott
VALUES('Dr. Bihari László',0000000001,'Orvos', 1111111111);
INSERT INTO Alkalmazott
VALUES('Bunford Mária',0000000002,'Nover', 1111111112);
INSERT INTO Alkalmazott
VALUES('Dr. Fazakas János',0000000003,'Orvos', 1111111113);
INSERT INTO Alkalmazott
VALUES('Dr. Illés Sándor',0000000004,'Orvos', 1111111114);
INSERT INTO Alkalmazott
VALUES('Dr. Lénárd Zsuzsa',0000000005,'Orvos', 1111111115);
INSERT INTO Alkalmazott
VALUES('Dr. Máthé Zsolt',0000000006,'Orvos', 1111111116);
INSERT INTO Alkalmazott
VALUES('Dr. Piros László',0000000007,'Orvos', 1111111117);
INSERT INTO Alkalmazott
VALUES('Nagy Valéria',0000000008,'Nover', 1111111118);
INSERT INTO Alkalmazott
VALUES('Toth Krisztina',0000000009,'Nover', 1111111119);
INSERT INTO Alkalmazott
VALUES('Orosz Ildiko',00000000010,'Nover', 1111111119);

INSERT INTO Donor_jelolt
VALUES('Kovács Kálmán', to_date('1996.08.25','YYYY.MM.DD'),'1146 Budapest, Thököly út 50.', 025678943,0000000001,1);
INSERT INTO Donor_jelolt
VALUES('Kiss Katalin', to_date('1988.08.10','YYYY.MM.DD'),'1144 Budapest, Csongor út 6.', 025623893,0000000005,1);
INSERT INTO Donor_jelolt
VALUES('Horváth Gabriella', to_date('1996.08.25','YYYY.MM.DD'),'1186 Budapest, Csengeri utca 20.', 135678943,0000000003,1);
INSERT INTO Donor_jelolt
VALUES('Vajda Virgil', to_date('1954.04.25','YYYY.MM.DD'),'1123 Budapest, Bécsi út 14.', 027628943,0000000004,1);
INSERT INTO Donor_jelolt
VALUES('Losonci Lajos', to_date('1996.08.25','YYYY.MM.DD'),'1057 Budapest, Szondi utca 11.,', 025678587,0000000006,1);
INSERT INTO Donor_jelolt
VALUES('Török Imre', to_date('1945.06.2','YYYY.MM.DD'),'1675 Budapest, Kölcsey Ferenc utca 10.', 025615793,0000000007,1);
INSERT INTO Donor_jelolt
VALUES('Arany Virág', to_date('2000.01.10','YYYY.MM.DD'),'1024 Budapest, Ajtósi Dürer sor 7.', 025186323,0000000001,1);
INSERT INTO Donor_jelolt
VALUES('Szoros Sándor', to_date('1930.05.25','YYYY.MM.DD'),'1651 Budapest, Bácskai utca 4.', 025674986,0000000003,1);
INSERT INTO Donor_jelolt
VALUES('Fejes Antónia', to_date('1991.01.20','YYYY.MM.DD'),'3600 Ózd, Petőfi utca 5.', 186475282,0000000001,1);
INSERT INTO Donor_jelolt
VALUES('Nemes Erik', to_date('1990.11.11','YYYY.MM.DD'),'3000 Hatvan, Ballasi Bálint út 7.', 118964724,0000000001,1);
INSERT INTO Donor_jelolt
VALUES('Ferences Kázmér', to_date('1998.04.06','YYYY.MM.DD'),'3980 Sátoraljaújhely, Mártírok útja 71.', 125479534,0000000004,0);
INSERT INTO Donor_jelolt
VALUES('Szabó Szilvia', to_date('1988.01.30','YYYY.MM.DD'),'2900 Komárom, Széchenyi István utca 2.', 047794461,0000000006,0);
INSERT INTO Donor_jelolt
VALUES('Kedves Eszter', to_date('1999.12.22','YYYY.MM.DD'),'7400 Kaposvár, Tallián Gyula utca 18.', 078923647,0000000007,0);
INSERT INTO Donor_jelolt
VALUES('Tudor Franciska', to_date('1991.10.28','YYYY.MM.DD'),'9400 Sopron, Győri utca 20.', 114643156,0000000003,0);
INSERT INTO Donor_jelolt
VALUES('Tóth Tamás', to_date('1975.12.30','YYYY.MM.DD'),'4400 Nyíregyháza, Szent István utca 68.', 157546475,0000000004,0);

INSERT INTO Donor
VALUES(5555555551,025678943, to_date('2020.10.30','YYYY.MM.DD'),0000000002, '0 Rh+',1111111111 );
INSERT INTO Donor
VALUES(5555555552,025623893, to_date('2020.10.10','YYYY.MM.DD'),0000000002, 'B Rh+',1111111112 );
INSERT INTO Donor
VALUES(5555555553,135678943, to_date('2020.10.15','YYYY.MM.DD'),0000000008, 'AB Rh+',1111111113 );
INSERT INTO Donor
VALUES(5555555554,027628943, to_date('2020.10.30','YYYY.MM.DD'),0000000008, '0 Rh-',1111111114 );
INSERT INTO Donor
VALUES(5555555555,025678587, to_date('2020.10.22','YYYY.MM.DD'),0000000002, 'A Rh+',1111111115 );
INSERT INTO Donor
VALUES(5555555556,025615793, to_date('2020.10.17','YYYY.MM.DD'),0000000009, 'A Rh-',1111111116 );
INSERT INTO Donor
VALUES(5555555557,025186323, to_date('2020.10.29','YYYY.MM.DD'),0000000009, '0 Rh+',1111111117 );
INSERT INTO Donor
VALUES(5555555558,025674986, to_date('2020.10.01','YYYY.MM.DD'),0000000010, 'AB Rh-',1111111118 );
INSERT INTO Donor
VALUES(5555555559,186475282, to_date('2020.10.16','YYYY.MM.DD'),0000000010, 'B Rh+',1111111118 );
INSERT INTO Donor
VALUES(5555555560,118964724, to_date('2020.10.02','YYYY.MM.DD'),0000000008, '0 Rh+',1111111111 );


INSERT INTO Recipiens
VALUES('Heves Hedvig',to_date('1990.09.02','YYYY.MM.DD'),'4400 Nyíregyháza, Virág utca 10.',125476547);
INSERT INTO Recipiens
VALUES('Soros Sára',to_date('1985.01.12','YYYY.MM.DD'),'7400 Kaposvár, Ady Endre utca 25.',145678954);
INSERT INTO Recipiens
VALUES('Oláh Ottó',to_date('1978.03.05','YYYY.MM.DD'),'2900 Komárom, Vezér utca 8.',064785468);
INSERT INTO Recipiens
VALUES('Varja János',to_date('1964.09.25','YYYY.MM.DD'),'3000 Hatvan, Árpád utca 40',076412853);
INSERT INTO Recipiens
VALUES('Kerepes Csenge',to_date('1990.09.02','YYYY.MM.DD'),'4400 Nyíregyháza, Katona utca 6.',114835642);
INSERT INTO Recipiens
VALUES('Futó Márton',to_date('1990.09.02','YYYY.MM.DD'),'1144 Budapest, Szentendrei út 6.',111479655);
INSERT INTO Recipiens
VALUES('Báthori Eszter',to_date('1990.09.02','YYYY.MM.DD'),'1045 Budapest, Kossuth Lajos utca 35.',116648234);
INSERT INTO Recipiens
VALUES('Neves Noémi',to_date('1990.09.02','YYYY.MM.DD'),'1184 Budapest, Kisfaludy utca 28.',122468953);
INSERT INTO Recipiens
VALUES('Szentesi Róbert',to_date('1990.09.02','YYYY.MM.DD'),'1167 Budapest, Dózsa György utca 62.',154325448);
INSERT INTO Recipiens
VALUES('Csillag Ferenc',to_date('1990.09.02','YYYY.MM.DD'),'1304 Budapest, Tímár utca 60.',111496281);



INSERT INTO Verkeszitmeny
VALUES(7777777771,5555555551, to_date('2020.11.02','YYYY.MM.DD'),'vvt-koncentratum', 2222222220,null);
INSERT INTO Verkeszitmeny
VALUES(7777777772,5555555551, to_date('2020.11.02','YYYY.MM.DD'),'thr-koncentratum', 2222222220,null);
INSERT INTO Verkeszitmeny
VALUES(7777777773,5555555551, to_date('2020.11.02','YYYY.MM.DD'),'ffp-koncentratum', 2222222220,125476547);
INSERT INTO Verkeszitmeny
VALUES(7777777774,5555555552, to_date('2020.11.02','YYYY.MM.DD'),'vvt-koncentratum', 2222222221,null);
INSERT INTO Verkeszitmeny
VALUES(7777777884,5555555552, to_date('2020.11.02','YYYY.MM.DD'),'thr-koncentratum', 2222222221,154325448);
INSERT INTO Verkeszitmeny
VALUES(7777778884,5555555552, to_date('2020.11.02','YYYY.MM.DD'),'ffp-koncentratum', 2222222221,154325448);
INSERT INTO Verkeszitmeny
VALUES(7777777775,5555555553, to_date('2020.12.02','YYYY.MM.DD'),'vvt-koncentratum', 2222222222,111496281);
INSERT INTO Verkeszitmeny
VALUES(7777777776,5555555553, to_date('2020.12.02','YYYY.MM.DD'),'thr-koncentratum', 2222222222,111496281);
INSERT INTO Verkeszitmeny
VALUES(7777777777,5555555553, to_date('2020.12.02','YYYY.MM.DD'),'ffp-koncentratum', 2222222222,null);
INSERT INTO Verkeszitmeny
VALUES(7777777780,5555555554, to_date('2020.12.02','YYYY.MM.DD'),'vvt-koncentratum', 2222222223,null);
INSERT INTO Verkeszitmeny
VALUES(7777777781,5555555554, to_date('2020.12.02','YYYY.MM.DD'),'thr-koncentratum', 2222222223,111479655);
INSERT INTO Verkeszitmeny
VALUES(7777777782,5555555554, to_date('2020.12.02','YYYY.MM.DD'),'ffp-koncentratum', 2222222223,111479655);
INSERT INTO Verkeszitmeny
VALUES(7777777783,5555555555, to_date('2020.11.02','YYYY.MM.DD'),'vvt-koncentratum', 2222222224,null);
INSERT INTO Verkeszitmeny
VALUES(7777771884,5555555555, to_date('2020.11.02','YYYY.MM.DD'),'thr-koncentratum', 2222222224,null);
INSERT INTO Verkeszitmeny
VALUES(7777777785,5555555555, to_date('2020.11.02','YYYY.MM.DD'),'ffp-koncentratum', 2222222224,null);
INSERT INTO Verkeszitmeny
VALUES(7777777786,5555555556, to_date('2020.12.02','YYYY.MM.DD'),'vvt-koncentratum', 2222222225,145678954);
INSERT INTO Verkeszitmeny
VALUES(7777777787,5555555556, to_date('2020.12.02','YYYY.MM.DD'),'thr-koncentratum', 2222222225,145678954);
INSERT INTO Verkeszitmeny
VALUES(7777777788,5555555556, to_date('2020.12.02','YYYY.MM.DD'),'ffp-koncentratum', 2222222225,145678954);
INSERT INTO Verkeszitmeny
VALUES(7777777789,5555555557, to_date('2020.12.02','YYYY.MM.DD'),'vvt-koncentratum', 2222222226,064785468);
INSERT INTO Verkeszitmeny
VALUES(7777777790,5555555557, to_date('2020.12.02','YYYY.MM.DD'),'thr-koncentratum', 2222222226,064785468);
INSERT INTO Verkeszitmeny
VALUES(7777777791,5555555557, to_date('2020.12.02','YYYY.MM.DD'),'ffp-koncentratum', 2222222226,064785468);
INSERT INTO Verkeszitmeny
VALUES(7777777792,5555555558, to_date('2021.01.02','YYYY.MM.DD'),'vvt-koncentratum', 2222222227,null);
INSERT INTO Verkeszitmeny
VALUES(7777777992,5555555558, to_date('2021.01.02','YYYY.MM.DD'),'thr-koncentratum', 2222222227,null);
INSERT INTO Verkeszitmeny
VALUES(7777779992,5555555558, to_date('2021.01.02','YYYY.MM.DD'),'ffp-koncentratum', 2222222227,null);
INSERT INTO Verkeszitmeny
VALUES(7777777793,5555555559, to_date('2021.01.02','YYYY.MM.DD'),'vvt-koncentratum', 2222222228,076412853);
INSERT INTO Verkeszitmeny
VALUES(7777777794,5555555559, to_date('2021.01.02','YYYY.MM.DD'),'thr-koncentratum', 2222222228,null);
INSERT INTO Verkeszitmeny
VALUES(7777777795,5555555559, to_date('2021.01.02','YYYY.MM.DD'),'ffp-koncentratum', 2222222228,076412853);
INSERT INTO Verkeszitmeny
VALUES(7777777796,5555555560, to_date('2021.01.02','YYYY.MM.DD'),'vvt-koncentratum', 2222222229,114835642);
INSERT INTO Verkeszitmeny
VALUES(7777777797,5555555560, to_date('2021.01.02','YYYY.MM.DD'),'thr-koncentratum', 2222222229,114835642);
INSERT INTO Verkeszitmeny
VALUES(7777777798,5555555560, to_date('2021.01.02','YYYY.MM.DD'),'ffp-koncentratum', 2222222229,null);



SELECT * FROM Donor_jelolt;
SELECT * FROM Donor;
SELECT * FROM Verkeszitmeny;
SELECT * FROM Recipiens;
SELECT * FROM Helyszin;
SELECT * FROM Alkalmazott;
SELECT * FROM Korhaz;

--egyszerű lekérdezések:

SELECT Nev, Szuletesi_datum
FROM Donor_jelolt
WHERE szuletesi_datum <to_date('1970-01-01','YYYY-MM-DD') 
ORDER BY szuletesi_datum;


SELECT Verkeszitmeny_ID, Lejarat_datuma
FROM Verkeszitmeny
WHERE lejarat_datuma >to_date('2021-01-01','YYYY-MM-DD') AND
lejarat_datuma <to_date('2022-01-01','YYYY-MM-DD')
ORDER BY lejarat_datuma;

SELECT Nev, Alkalmazott_ID
FROM Alkalmazott
WHERE LOWER(Beosztas) LIKE 'orvos'
ORDER BY Nev;

--többtáblás lekérdezések:

SELECT Alkalmazott.Nev, Helyszin.Nev AS "Munkahely neve"
FROM Alkalmazott INNER JOIN Helyszin ON Alkalmazott.Munkahelyszin = Helyszin.Helyszin_ID;

SELECT Donor_jelolt.Nev as "Donor jelölt neve", Alkalmazott.Nev AS "Orvos neve"
FROM Donor_jelolt INNER JOIN Alkalmazott ON Alkalmazott.Alkalmazott_ID = Donor_jelolt.Orvos_azonosito;

SELECT Donor_jelolt.Nev, Donor.Utolso_veradas_datuma
FROM Donor_jelolt INNER JOIN DONOR ON Donor_jelolt.Tajszam = Donor.Tajszam
WHERE TO_CHAR (Donor.Utolso_veradas_datuma,'MM.DD')= '10.30';


SELECT Verkeszitmeny.Donor, Verkeszitmeny_ID, Recipiens.Tajszam as Recipiens
FROM Verkeszitmeny LEFT OUTER JOIN Recipiens ON Verkeszitmeny.Recipiens = Recipiens.Tajszam
WHERE Recipiens.Tajszam IS NULL;

SELECT Recipiens.Nev AS Recipiens, Verkeszitmeny_ID
FROM Recipiens LEFT OUTER JOIN Verkeszitmeny ON Verkeszitmeny.Recipiens = Recipiens.Tajszam
WHERE Verkeszitmeny.Verkeszitmeny_ID IS NULL;

SELECT Donor_jelolt.Nev, Donor.Donor_ID
FROM Donor RIGHT OUTER JOIN Donor_jelolt ON Donor_jelolt.Tajszam = Donor.Tajszam
where Donor.Donor_ID IS NULL;

--csoportosító lekérdezések és függvények:

SELECT Donor.Vercsoport, Donor_jelolt.Nev as "Donor nev", min_datumok.min_datum 
FROM Donor_jelolt INNER JOIN Donor on Donor_jelolt.Tajszam = Donor.Tajszam,
(SELECT Donor.Vercsoport as vercsoport,  min(Donor_jelolt.Szuletesi_datum)  min_datum
FROM Donor_jelolt INNER JOIN Donor on Donor_jelolt.Tajszam = Donor.Tajszam
GROUP BY Donor.Vercsoport) min_datumok
WHERE Donor_jelolt.Szuletesi_datum = min_datumok.min_datum and Donor.Vercsoport = min_datumok.vercsoport
ORDER BY Donor.Vercsoport;

SELECT lista.neve, Count(*) db 
FROM 
(SELECT Donor.Apolo_azonosito as apolo_id, Alkalmazott.Nev neve
FROM Donor INNER JOIN Alkalmazott on Donor.Apolo_azonosito = Alkalmazott.Alkalmazott_ID
WHERE Donor.utolso_veradas_datuma > to_date('2020.01.01','YYYY.MM.DD')) lista
GROUP BY lista.neve
HAVING Count(*) > 0;

SELECT altabla.alkalmassag, Count(*) db
FROM 
(SELECT Donor_jelolt.Alkalmas_e as alkalmassag, Donor_jelolt.Allando_lakcim, Donor_jelolt.SZULETESI_DATUM as szuletes
FROM Donor_jelolt
WHERE SUBSTR(LOWER(Donor_jelolt.Allando_lakcim),6,8)='budapest' OR SUBSTR(LOWER(Donor_jelolt.Allando_lakcim),6,6)='sopron') altabla
GROUP BY altabla.alkalmassag
HAVING Count(*) >= 1;

SELECT evek.fajta, ROUND(AVG(evek.lejaratiev),0) AS atlag_év
FROM 
(SELECT  Verkeszitmeny.Fajtaja as fajta, EXTRACT(YEAR FROM Verkeszitmeny.Lejarat_datuma) as lejaratiev
FROM Verkeszitmeny
) evek
GROUP BY evek.fajta
HAVING ROUND(AVG(evek.lejaratiev),0) < 2021;

--allekérdezések és halmaz operátorok:

Select Alkalmazott.Nev, Alkalmazott.Alkalmazott_ID
from Alkalmazott
where Alkalmazott.Alkalmazott_ID in (select Alkalmazott.Alkalmazott_ID 
                                     from Alkalmazott 
                                     where Alkalmazott.Alkalmazott_ID  between 0000000003 and 0000000007);

Select * from Verkeszitmeny
where  Verkeszitmeny.lejarat_datuma  = any (select Verkeszitmeny.lejarat_datuma from Verkeszitmeny
                                            where extract(year from Verkeszitmeny.lejarat_datuma) > 2020);


Select Donor_jelolt.Nev, Donor_jelolt.szuletesi_datum
from Donor_jelolt inner join Donor on (Donor_jelolt.tajszam = Donor.Tajszam)
where Donor_jelolt.szuletesi_datum > all(select recipiens.Szuletesi_datum from recipiens);

select donor_jelolt.nev, donor.donor_id 
from donor inner join donor_jelolt on (Donor_jelolt.tajszam = Donor.Tajszam)
where exists (select * from verkeszitmeny
              where donor.donor_id  = verkeszitmeny.donor 
              and verkeszitmeny.fajtaja like 'ffp-koncentratum' 
              and verkeszitmeny.recipiens is null);

select helyszin.nev from helyszin
union
select korhaz.nev from korhaz;

Select verkeszitmeny.recipiens from verkeszitmeny
intersect
select recipiens.tajszam from recipiens;


Select donor_jelolt.tajszam from donor_jelolt
minus
select donor.tajszam from donor;

--nézetek:

Create view v1 as
Select verkeszitmeny.verkeszitmeny_id
from verkeszitmeny
where verkeszitmeny.recipiens is not null;

Create view v2 as
select recipiens.tajszam from recipiens
minus
Select verkeszitmeny.recipiens from verkeszitmeny;

Create view v3 as
select VERKESZITMENY.verkeszitmeny_id, korhaz.nev, korhaz.korhaz_id, korhaz.cim
from VERKESZITMENY inner join korhaz on VERKESZITMENY.cel_korhaz = korhaz.korhaz_id;

--DDL utasítások:

alter table donor_jelolt add
(neme varchar2(5))
add CONSTRAINT nem_ck CHECK (neme='nő' or neme='férfi' or neme=null);

alter table donor_jelolt
modify neme CHECK (neme='woman' or neme='man' or neme=null);

alter table donor_jelolt
disable constraint nem_ck;

alter table donor_jelolt
drop constraint nem_ck;

alter table donor_jelolt
rename column neme to nem;

alter table donor_jelolt 
drop column nem;

rename donor_jelolt to Potencialis_donor;

--DML utasítások:

update Alkalmazott
set Alkalmazott.nev = 'Dr. Bunford Mária', Alkalmazott.beosztas = 'orvos'
where ALKALMAZOTT_ID = 0000000002;

update VERKESZITMENY
set RECIPIENS = 116648234
where VERKESZITMENY_ID =7777777792;

update POTENCIALIS_DONOR
set ALKALMAS_E = 0
where Potencialis_donor.SZULETESI_DATUM = (SELECT Min(Potencialis_donor.SZULETESI_DATUM) Szuletesi_ido FROM POTENCIALIS_DONOR);

delete from HELYSZIN
where HELYSZIN_ID = 1111111110;

delete from RECIPIENS
where tajszam =122468953;

delete from VERKESZITMENY
where LEJARAT_DATUMA = 
(select min(LEJARAT_DATUMA) mindatum
from VERKESZITMENY
where RECIPIENS is null);


--PL/SQL triggerek:

SET serveroutput ON;
create or replace trigger Beszuras
before insert on Recipiens
for each row
begin
dbms_output.put_line('Beszúrás történt!');
end;

INSERT INTO recipiens
VALUES('Kopasz Klotild', to_date('1985.05.12','YYYY.MM.DD'),'1145 Budapest, Korong utca 45.', 124562111);

--NEXT

create or replace trigger UpToDate
after update on recipiens
begin
dbms_output.put_line('A recipiens táblában módosítás történt!');
end;

update recipiens
set nev = 'Kopasz Klotilda' where nev = 'Kopasz Klotild';

--NEXT


create or replace trigger Torles
before delete on Recipiens
for each row
begin
dbms_output.put_line('Törlés történt!');
end;


DELETE FROM recipeins
WHERE tajszam = '124562111';

--Jogosultság kezelés, Tranzakció kezelés:


Create role veradohely_admin;
Create role korhaz_admin;
Create user apolo1 identified by apolo_1_pw;
Create user apolo2 identified by apolo_2_pw;
Grant create session, insert any table to veradohely_admin;
Grant create session, delete any table to korhaz_admin;
Grant update any table to apolo1;
Grant update any table to apolo2;
Grant veradohely_admin to apolo1;
Grant korhaz_admin to apolo2;


--user:apolo1

INSERT INTO DB.POTENCIALIS_DONOR
VALUES('Lezser Lénárd', to_date('1986.08.25','YYYY.MM.DD'),'1146 Budapest, Thököly út 192.', 025666643,0000000003,1);

Commit;


--user:apolo1
update POTENCIALIS_DONOR
set ALKALMAS_E = 0
where tajszam= 025666643;


Commit;


--user:apolo2
update POTENCIALIS_DONOR
set ALKALMAS_E = 1
where tajszam= 025666643;

--user:apolo2

Savepoint sv1;

delete from DB.POTENCIALIS_DONOR
where tajszam= 025666643;

Rollback to sv1;


