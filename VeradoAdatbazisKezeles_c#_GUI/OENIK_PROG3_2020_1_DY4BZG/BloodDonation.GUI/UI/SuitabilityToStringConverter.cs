﻿// <copyright file="SuitabilityToStringConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.GUI.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// SuitabilityToStringConverter class.
    /// </summary>
    public class SuitabilityToStringConverter : IValueConverter
    {
        /// <summary>
        /// Convert suitability to string.
        /// </summary>
        /// <param name="value">object parameter.</param>
        /// <param name="targetType">Type.</param>
        /// <param name="parameter">object type parameter.</param>
        /// <param name="culture">System.Globalization.CultureInfo.</param>
        /// <returns>returns string.</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string h = "None";
            if (value != null)
            {
                h = value.ToString();
            }

            return h;
        }

        /// <summary>
        /// convert back method.
        /// </summary>
        /// <param name="value">object parameter.</param>
        /// <param name="targetType">Type.</param>
        /// <param name="parameter">object type parameter.</param>
        /// <param name="culture">System.Globalization.CultureInfo.</param>
        /// <returns>returns nothing.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
