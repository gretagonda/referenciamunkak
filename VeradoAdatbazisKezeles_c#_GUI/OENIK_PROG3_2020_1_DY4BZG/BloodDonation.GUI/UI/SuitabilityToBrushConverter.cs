﻿// <copyright file="SuitabilityToBrushConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.GUI.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using System.Windows.Media;
    using BloodDonation.GUI.Data;

    /// <summary>
    /// SuitabilityToBrushConverter class.
    /// </summary>
    public class SuitabilityToBrushConverter : IValueConverter
    {
        /// <summary>
        /// Convert suitability to color.
        /// </summary>
        /// <param name="value">object parameter.</param>
        /// <param name="targetType">Type.</param>
        /// <param name="parameter">object type parameter.</param>
        /// <param name="culture">System.Globalization.CultureInfo.</param>
        /// <returns>returns brush color.</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            SuitabilityType pos = (SuitabilityType)value;
            switch (pos)
            {
                default:
                case SuitabilityType.Suitable: return Brushes.LightGreen;
                case SuitabilityType.NonSuitable: return Brushes.Salmon;
            }
        }

        /// <summary>
        /// convert back method.
        /// </summary>
        /// <param name="value">object parameter.</param>
        /// <param name="targetType">Type.</param>
        /// <param name="parameter">object type parameter.</param>
        /// <param name="culture">System.Globalization.CultureInfo.</param>
        /// <returns>returns nothing.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
