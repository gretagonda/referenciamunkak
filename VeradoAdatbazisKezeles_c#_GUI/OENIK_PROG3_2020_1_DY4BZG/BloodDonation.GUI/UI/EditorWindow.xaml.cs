﻿// <copyright file="EditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.GUI.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BloodDonation.GUI.Data;
    using BloodDonation.GUI.VM;

    /// <summary>
    /// Interaction logic for EditorWindow.xaml.
    /// </summary>
    public partial class EditorWindow : Window
    {
        private EditorViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// EditorWindow constructor.
        /// </summary>
        public EditorWindow()
        {
            this.InitializeComponent();
            this.vm = this.FindResource("VM") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// EditorWindow constructor with parameter.
        /// </summary>
        /// <param name="dcandidate">DonorCandidateS parameter.</param>
        public EditorWindow(DonorCandidateS dcandidate)
            : this()
        {
            this.vm.Dcandidate = dcandidate;
        }

        /// <summary>
        /// Gets candidate constructor.
        /// </summary>
        public DonorCandidateS Candidate { get => this.vm.Dcandidate; }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
