﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.GUI.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BloodDonation.GUI.BL;
    using BloodDonation.GUI.Data;

    /// <summary>
    /// EditorServiceViaWindow class.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// impelentation of EditDonorCandidate method.
        /// </summary>
        /// <param name="dc">DonorCandidateS parameter.</param>
        /// <returns>return dialog.</returns>
        public bool EditDonorCandidate(DonorCandidateS dc)
        {
            EditorWindow win = new EditorWindow(dc);
            return win.ShowDialog() ?? false;
        }
    }
}
