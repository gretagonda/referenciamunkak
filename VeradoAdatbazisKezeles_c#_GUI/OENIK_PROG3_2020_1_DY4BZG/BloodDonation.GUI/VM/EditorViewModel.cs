﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.GUI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BloodDonation.GUI.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// EditorViewModel class.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private DonorCandidateS dcandidate;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// EditorViewWindow constructor.
        /// </summary>
        public EditorViewModel()
        {
            this.dcandidate = new DonorCandidateS();
            if (this.IsInDesignMode)
            {
                this.dcandidate.CandidateName = "Kiss Kalocsa";
                this.dcandidate.ConstantAddress = "1145 Budapest Ameriaki út 46.";
                this.dcandidate.BirthDate = new DateTime(1998, 05, 05);
                this.dcandidate.DoctorsName = "Dr. Veres Orsolya";
                this.dcandidate.Suitability = SuitabilityType.NonSuitable;
                this.dcandidate.Tajnumber = "191919191";
            }
        }

        /// <summary>
        /// Gets array that contains SuitabilityType enum values.
        /// </summary>
        public Array CandidateSuitability
        {
            get { return Enum.GetValues(typeof(SuitabilityType)); }
        }

        /// <summary>
        /// Gets or Sets dcandidate.
        /// </summary>
        public DonorCandidateS Dcandidate
        {
            get { return this.dcandidate; }
            set { this.Set(ref this.dcandidate, value); }
        }
    }
}
