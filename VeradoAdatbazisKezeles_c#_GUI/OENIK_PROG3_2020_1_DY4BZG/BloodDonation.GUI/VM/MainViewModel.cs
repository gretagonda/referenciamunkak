﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.GUI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using BloodDonation.GUI.BL;
    using BloodDonation.GUI.Data;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// MainViewModel class declaration.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private IDonorCandidateSLogic logic;
        private DonorCandidateS selected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// constructor of MainWindowModel.
        /// </summary>
        /// <param name="logic">IDonorCandidateSLogic parameter.</param>
        public MainViewModel(IDonorCandidateSLogic logic)
        {
            this.logic = logic;
            this.CandidateCollection = new ObservableCollection<DonorCandidateS>();
            if (this.IsInDesignMode)
            {
                   DonorCandidateS dc1 = new DonorCandidateS() { CandidateName = "Kiss Kalocsa", ConstantAddress = "1145 Budapest Ameriaki út 46.", BirthDate = new DateTime(1998, 05, 05), DoctorsName = "Dr. Veres Orsolya", Suitability = SuitabilityType.NonSuitable, Tajnumber = "191919191" };
                   DonorCandidateS dc2 = new DonorCandidateS() { CandidateName = "Nagy Várad", ConstantAddress = "1145 Budapest Columbus utca 10.", BirthDate = new DateTime(1990, 07, 24), DoctorsName = "Dr. Veres Orsolya", Suitability = SuitabilityType.NonSuitable, Tajnumber = "919191919" };
                   this.CandidateCollection.Add(dc1);
                   this.CandidateCollection.Add(dc2);
            }
            else
            {
                var tempList = this.logic.GetAllCandidates();
                this.CandidateCollection = DonorCandidateSLogic.ToObservableCollection(tempList);
            }

            this.AddCmd = new RelayCommand(() => this.logic.AddDonorCandidateS(this.CandidateCollection));
            this.ModCmd = new RelayCommand(() => this.logic.ModDonorCandidate(this.Selected));
            this.DelCmd = new RelayCommand(() => this.logic.DelCandidate(this.CandidateCollection, this.Selected));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// constructor for automatic generation.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IDonorCandidateSLogic>())
        {
        }

        /// <summary>
        /// Gets collection with candidates.
        /// </summary>
        public ObservableCollection<DonorCandidateS> CandidateCollection { get; private set; }

        /// <summary>
        /// Gets or Sets selected candidate.
        /// </summary>
        public DonorCandidateS Selected
        {
            get { return this.selected; }
            set { this.Set(ref this.selected, value); }
        }

        /// <summary>
        /// Gets or Stes ADD command to add new candidate.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets or Stes MODIFY command to modify specifiy candidate.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets or Stes DELETE command to delete specific candidate.
        /// </summary>
        public ICommand DelCmd { get; private set; }
    }
}
