﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.GUI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BloodDonation.GUI.Data;

    /// <summary>
    /// IEditor service interface added to edit DataBase.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// EditDonorCandidate method added.
        /// </summary>
        /// <param name="dc">DonorCandidateS parameter.</param>
        /// <returns>returns bool value.</returns>
        bool EditDonorCandidate(DonorCandidateS dc);
    }
}
