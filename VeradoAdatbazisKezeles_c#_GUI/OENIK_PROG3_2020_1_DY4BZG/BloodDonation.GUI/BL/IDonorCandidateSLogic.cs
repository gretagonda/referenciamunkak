﻿// <copyright file="IDonorCandidateSLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.GUI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BloodDonation.GUI.Data;

    /// <summary>
    /// IDonorCandidateSLogic interface with modifying methods.
    /// </summary>
    public interface IDonorCandidateSLogic
    {
        /// <summary>
        /// Method to add donor candidate to the candidate list.
        /// </summary>
        /// <param name="list">list where we add the new candidate.</param>
        void AddDonorCandidateS(IList<DonorCandidateS> list);

        /// <summary>
        /// method to modify existing candidate.
        /// </summary>
        /// <param name="candidateToModify">The candidate we want to modify.</param>
        void ModDonorCandidate(DonorCandidateS candidateToModify);

        /// <summary>
        /// method to delete a  specific candidate.
        /// </summary>
        /// <param name="list">the list we want to delete from.</param>
        /// <param name="candidate">the candidate we want to delete.</param>
        void DelCandidate(IList<DonorCandidateS> list, DonorCandidateS candidate);

        /// <summary>
        /// method to collect candidates from the database.
        /// </summary>
        /// <returns>returns a list with all the existing candidates.</returns>
        IList<DonorCandidateS> GetAllCandidates();
    }
}
