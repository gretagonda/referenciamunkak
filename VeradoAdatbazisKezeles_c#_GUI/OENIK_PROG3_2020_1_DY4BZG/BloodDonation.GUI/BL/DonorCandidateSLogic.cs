﻿// <copyright file="DonorCandidateSLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.GUI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BloodDonation.Data;
    using BloodDonation.GUI.Data;
    using BloodDonation.Program;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// imlementing DonorCandidateLogic class.
    /// </summary>
    public class DonorCandidateSLogic : IDonorCandidateSLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private Factory dcFactory = new Factory();
        private IList<DonorCandidateS> myCandidateList;

        /// <summary>
        /// Initializes a new instance of the <see cref="DonorCandidateSLogic"/> class.
        /// </summary>
        /// <param name="e">IEditorService parameter.</param>
        /// <param name="im">IMessenger parameter.</param>
        public DonorCandidateSLogic(IEditorService e, IMessenger im)
        {
            this.myCandidateList = this.GetAllCandidates();
            this.editorService = e;
            this.messengerService = im;
        }

        /// <summary>
        /// Gets myCandidateList.
        /// </summary>
        public IList<DonorCandidateS> MyCandidateList
        {
            get { return this.myCandidateList; }

            // set { this.myCandidateList = value; }
        }

        /// <summary>
        /// convet IList to ObservableCollection.
        /// </summary>
        /// <param name="enumerable">IEnumerable collection to be converted.</param>
        /// <returns>observablecollection with donorcandidates.</returns>
        public static ObservableCollection<DonorCandidateS> ToObservableCollection(IEnumerable<DonorCandidateS> enumerable)
        {
            var col = new ObservableCollection<DonorCandidateS>();
            if (enumerable != null)
            {
                foreach (var cur in enumerable)
                {
                    col.Add(cur);
                }
            }

            return col;
        }

        /// <summary>
        /// Method to add donor candidate to the candidate list.
        /// </summary>
        /// <param name="list">list where we add the new candidate.</param>
        public void AddDonorCandidateS(IList<DonorCandidateS> list)
        {
            if (list != null)
            {
                DonorCandidateS newCandidate = new DonorCandidateS();
                newCandidate.BirthDate = DateTime.Now;
                if (this.editorService.EditDonorCandidate(newCandidate) == true)
                {
                    list.Add(newCandidate);
                    DonorCandidate newDC = new DonorCandidate();
                    newDC.BirthDate = newCandidate.BirthDate;
                    newDC.CandidateName = newCandidate.CandidateName;
                    newDC.ConstantAddress = newCandidate.ConstantAddress;
                    newDC.Tajnumber = newCandidate.Tajnumber;
                    newDC.DoctorsName = newCandidate.DoctorsName;
                    if (newCandidate.Suitability.Equals(SuitabilityType.Suitable))
                    {
                        newDC.Suitability = true;
                    }
                    else
                    {
                        newDC.Suitability = false;
                    }

                    this.dcFactory.InOrOutCandidate.InsertDC(newDC);
                    this.messengerService.Send("ADD OK", "LogicResult");
                }
                else
                {
                    this.messengerService.Send("ADD CANCELED", "LogicResult");
                }
            }
        }

        /// <summary>
        /// method to delete a  specific candidate.
        /// </summary>
        /// <param name="list">the list we want to delete from.</param>
        /// <param name="candidate">the candidate we want to delete.</param>
        public void DelCandidate(IList<DonorCandidateS> list, DonorCandidateS candidate)
        {
            if (list != null)
            {
                if (candidate != null && list.Remove(candidate))
                {
                    DonorCandidate removable = new DonorCandidate();
                    removable.BirthDate = DateTime.Now;
                    removable.BirthDate = candidate.BirthDate;
                    removable.CandidateName = candidate.CandidateName;
                    removable.ConstantAddress = candidate.ConstantAddress;
                    removable.Tajnumber = candidate.Tajnumber;
                    removable.DoctorsName = candidate.DoctorsName;
                    if (candidate.Suitability.Equals(SuitabilityType.Suitable))
                    {
                        removable.Suitability = true;
                    }
                    else
                    {
                        removable.Suitability = false;
                    }

                    this.dcFactory.InOrOutCandidate.RemoveDC(removable);
                    this.messengerService.Send("DELETE OK", "LogicResult");
                }
                else
                {
                    this.messengerService.Send("DELETE FAILED", "LogicResult");
                }
            }
        }

        /// <summary>
        /// method to collect candidates from the database.
        /// </summary>
        /// <returns>returns a list with all the existing candidates.</returns>
        public IList<DonorCandidateS> GetAllCandidates()
        {
            IList<DonorCandidateS> newList = new List<DonorCandidateS>();
            var list = this.dcFactory.GetDC.GetAllDC();
            foreach (var item in list)
            {
                DonorCandidateS tempDC = new DonorCandidateS();
                tempDC.CandidateName = item.CandidateName;
                tempDC.BirthDate = (DateTime)item.BirthDate;
                tempDC.ConstantAddress = item.ConstantAddress;
                tempDC.Tajnumber = item.Tajnumber;
                tempDC.DoctorsName = item.DoctorsName;
                if (item.Suitability.Equals(true))
                {
                    tempDC.Suitability = SuitabilityType.Suitable;
                }
                else
                {
                    tempDC.Suitability = SuitabilityType.NonSuitable;
                }

                newList.Add(tempDC);
            }

            return newList;
        }

        /// <summary>
        /// method to modify existing candidate.
        /// </summary>
        /// <param name="candidateToModify">The candidate we want to modify.</param>
        public void ModDonorCandidate(DonorCandidateS candidateToModify)
        {
            if (candidateToModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            DonorCandidateS dcClone = new DonorCandidateS();
            dcClone.BirthDate = DateTime.Now;
            dcClone.CopyFrom(candidateToModify);
            if (this.editorService.EditDonorCandidate(dcClone) == true)
            {
                candidateToModify.CopyFrom(dcClone);
                this.dcFactory.ChangeCandidate.ChangeConstantAddressForCandidate(dcClone.Tajnumber, dcClone.ConstantAddress);
                if (dcClone.Suitability.Equals(SuitabilityType.Suitable))
                {
                    this.dcFactory.ChangeCandidate.ChangeSuitability(dcClone.Tajnumber, true);
                }
                else
                {
                    this.dcFactory.ChangeCandidate.ChangeSuitability(dcClone.Tajnumber, false);
                }

                this.messengerService.Send("MODIFY OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("MODIFY CANCELED", "LogicResult");
            }
        }
    }
}
