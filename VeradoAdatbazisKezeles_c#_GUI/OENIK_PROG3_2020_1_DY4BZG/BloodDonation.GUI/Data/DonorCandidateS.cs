﻿// <copyright file="DonorCandidateS.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.GUI.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// enum to contai candidate suitability.
    /// </summary>
    public enum SuitabilityType
    {
        /// <summary>
        /// candidate is suitable.
        /// </summary>
      Suitable,

      /// <summary>
      /// candidate is not suitable.
      /// </summary>
      NonSuitable,
    }

    /// <summary>
    /// declaring DonorCanidateS class.
    /// </summary>
    public class DonorCandidateS : ObservableObject
    {
        private string candidateName;
        private DateTime birthDate;
        private string constantAddress;
        private string tajnumber;
        private string doctorsName;
        private SuitabilityType suitability;

        /// <summary>
        /// Gets or sets candidateName.
        /// </summary>
        public string CandidateName
        {
            get { return this.candidateName; }
            set { this.Set(ref this.candidateName, value); }
        }

        /// <summary>
        /// Gets or sets birthDate.
        /// </summary>
        public DateTime BirthDate
        {
            get { return this.birthDate; }
            set { this.Set(ref this.birthDate, value); }
        }

        /// <summary>
        /// Gets or sets constantAddress.
        /// </summary>
        public string ConstantAddress
        {
            get { return this.constantAddress; }
            set { this.Set(ref this.constantAddress, value); }
        }

        /// <summary>
        /// Gets or sets tajnumber.
        /// </summary>
        public string Tajnumber
        {
            get { return this.tajnumber; }
            set { this.Set(ref this.tajnumber, value); }
        }

        /// <summary>
        /// Gets or sets doctorsName.
        /// </summary>
        public string DoctorsName
        {
            get { return this.doctorsName; }
            set { this.Set(ref this.doctorsName, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether suitable.
        /// </summary>
        public SuitabilityType Suitability
        {
            get { return this.suitability; }
            set { this.Set(ref this.suitability, value); }
        }

        /// <summary>
        /// method to copy the object.
        /// </summary>
        /// <param name="other">object to be copied.</param>
        public void CopyFrom(DonorCandidateS other)
        {
            this.GetType().GetProperties().ToList().ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
