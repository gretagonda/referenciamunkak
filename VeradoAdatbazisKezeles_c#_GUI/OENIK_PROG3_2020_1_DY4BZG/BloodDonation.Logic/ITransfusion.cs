﻿// <copyright file="ITransfusion.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Logic
{
    using System.Collections.Generic;
    using BloodDonation.Data;

    /// <summary>
    /// declares ITransfusion interface.
    /// </summary>
    /// <typeparam name="T">generic type.</typeparam>
    public interface ITransfusion<T>
        where T : class
    {
        /// <summary>
        /// declares InsertR for ITranfusion.
        /// </summary>
        /// <param name="entity">Recipient object.</param>
        void InsertR(Recipient entity);

        /// <summary>
        /// declares RemoveR for ITranfusion.
        /// </summary>
        /// <param name="entity">Recipient object.</param>
        /// <returns>returns bool value.</returns>
        bool RemoveR(Recipient entity);

        /// <summary>
        /// declares RemoveBP for ITranfusion.
        /// </summary>
        /// <param name="entity">Bloodproduct object.</param>
        /// <returns>returns bool value.</returns>
        bool RemoveBP(Bloodproduct entity);

        /// <summary>
        /// declares GetOneR for ITranfusion.
        /// </summary>
        /// <param name="id">recipient id as string.</param>
        /// <returns>returns one specific Recipient.</returns>
        Recipient GetOneR(string id);

        /// <summary>
        /// declares GetOneBP for ITranfusion.
        /// </summary>
        /// <param name="id">blood product id as string.</param>
        /// <returns>returns one specific Bloodproduct.</returns>
        Bloodproduct GetOneBP(string id);

        /// <summary>
        /// declares GetAllR for ITranfusion.
        /// </summary>
        /// <returns>returns all Recipient objects.</returns>
        IList<Recipient> GetAllR();

        /// <summary>
        /// declares GetAllBp for ITranfusion.
        /// </summary>
        /// <returns>returns all Bloodproduct objects.</returns>
        IList<Bloodproduct> GetAllBP();

        /// <summary>
        /// declares ChangeConstantAddressForRecipient for ITranfusion.
        /// </summary>
        /// <param name="id">recipient id as string..</param>
        /// <param name="newAddress">new address as string.</param>
        void ChangeConstantAddressForRecipient(string id, string newAddress);

        /// <summary>
        /// declares ChangeTargetHospitalsName for ITranfusion.
        /// </summary>
        /// <param name="productID"> blood product id as string.</param>
        /// <param name="hospitalname">new target hospital name as string.</param>
        void ChangeTargetHospitalsName(string productID, string hospitalname);

        /// <summary>
        /// declares ChangeTargetHospitalsAddress for ITranfusion.
        /// </summary>
        /// <param name="productID">blood product id as string.</param>
        /// <param name="hospitaladdresss">new target hospital address as string.</param>
        void ChangeTargetHospitalsAddress(string productID, string hospitaladdresss);

        /// <summary>
        /// declares ChangeRecipient for ITranfusion.
        /// </summary>
        /// <param name="productID">blood product id as string.</param>
        /// <param name="taj">tajnumber of new recipient as string.</param>
        void ChangeRecipient(string productID, string taj);
    }
}
