﻿// <copyright file="Transfusion.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using BloodDonation.Data;
    using BloodDonation.Repository;

    /// <summary>
    /// declare Transfusion class.
    /// </summary>
    /// <typeparam name="T">generic type.</typeparam>
    public class Transfusion<T> : ITransfusion<T>
        where T : class
    {
        private IRecipientRepository recipientRepository;
        private IBloodProductRepository bloodProductRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Transfusion{T}"/> class.
        /// </summary>
        /// <param name="recipientRepository">IRecipientRepository as parameter.</param>
        public Transfusion(IRecipientRepository recipientRepository)
        {
            this.recipientRepository = recipientRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Transfusion{T}"/> class.
        /// </summary>
        /// <param name="bloodProductRepository">IBloodProductRepository as parameter.</param>
        public Transfusion(IBloodProductRepository bloodProductRepository)
        {
            this.bloodProductRepository = bloodProductRepository;
        }

        /// <summary>
        /// implemets ChangeConstantAddressForRecipient method.
        /// </summary>
        /// <param name="id">recipient id as string.</param>
        /// <param name="newAddress">new address as string.</param>
        public void ChangeConstantAddressForRecipient(string id, string newAddress)
        {
            this.recipientRepository.ChangeConstantAddress(id, newAddress);
        }

        /// <summary>
        /// implemets ChangeRecipient method.
        /// </summary>
        /// <param name="productID">product id as string.</param>
        /// <param name="taj">tajnumber of recipient as string.</param>
        public void ChangeRecipient(string productID, string taj)
        {
            this.bloodProductRepository.ChangeRecipient(productID, taj);
        }

        /// <summary>
        /// implemets ChangeTargetHospitalsAddress method.
        /// </summary>
        /// <param name="productID">product id as string.</param>
        /// <param name="hospitaladdresss">new hospital address as string.</param>
        public void ChangeTargetHospitalsAddress(string productID, string hospitaladdresss)
        {
            this.bloodProductRepository.ChangeTargetHospitalsAddress(productID, hospitaladdresss);
        }

        /// <summary>
        /// implemets ChangeTargetHospitalsName method.
        /// </summary>
        /// <param name="productID">priduct id a s string.</param>
        /// <param name="hospitalname">new hospital name as string.</param>
        public void ChangeTargetHospitalsName(string productID, string hospitalname)
        {
            this.bloodProductRepository.ChangeTargetHospitalsName(productID, hospitalname);
        }

        /// <summary>
        /// implemets GettAllR method.
        /// </summary>
        /// <returns>returns IList parameter with all the Recipient objects.</returns>
        public IList<Recipient> GetAllR()
        {
            return this.recipientRepository.GetAll().ToList();
        }

        /// <summary>
        /// implements GetAllBP method.
        /// </summary>
        /// <returns>returns IList parameter with all the Bloodproduct objects.</returns>
        public IList<Bloodproduct> GetAllBP()
        {
            return this.bloodProductRepository.GetAll().ToList();
        }

        /// <summary>
        /// implemets GetOneR method.
        /// </summary>
        /// <param name="id">recipient id as string.</param>
        /// <returns>returns one specific recipient object.</returns>
        public Recipient GetOneR(string id)
        {
            return this.recipientRepository.GetOne(id);
        }

        /// <summary>
        /// implemets GetOneBP method.
        /// </summary>
        /// <param name="id">Bloodproduct id as string.</param>
        /// <returns>returns one specific Bloodpoduct object.</returns>
        public Bloodproduct GetOneBP(string id)
        {
            return this.bloodProductRepository.GetOne(id);
        }

        /// <summary>
        /// implemets InsertR method.
        /// </summary>
        /// <param name="entity">new Recipient object as parameter.</param>
        public void InsertR(Recipient entity)
        {
            this.recipientRepository.Insert(entity);
        }

        /// <summary>
        /// implemets RemoveR method.
        /// </summary>
        /// <param name="entity">removable Recipient object as paramter.</param>
        /// <returns>returns bool value.</returns>
        public bool RemoveR(Recipient entity)
        {
            return this.recipientRepository.Remove(entity);
        }

        /// <summary>
        /// imlements RemoveBP method.
        /// </summary>
        /// <param name="entity">removable Bloodproduct object as parameter.</param>
        /// <returns>return bool value.</returns>
        public bool RemoveBP(Bloodproduct entity)
        {
            return this.bloodProductRepository.Remove(entity);
        }
    }
}
