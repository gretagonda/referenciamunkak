﻿// <copyright file="Donation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using BloodDonation.Data;
    using BloodDonation.Repository;

    /// <summary>
    /// declares Donation class.
    /// </summary>
    /// <typeparam name="T">generic parameter.</typeparam>
    public class Donation<T> : IDonation<T>
        where T : class
    {
        private IDonorRepository donorRepository;
        private IBloodProductRepository bloodProductRepository;
        private IDonorCandidateRepository donorCandidateRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Donation{T}"/> class.
        /// </summary>
        /// <param name="donorRepository">IDonorRepository parameter.</param>
        public Donation(IDonorRepository donorRepository)
        {
            this.donorRepository = donorRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Donation{T}"/> class.
        /// </summary>
        /// <param name="bloodProductRepository">IBloodProductRepository parameter.</param>
        public Donation(IBloodProductRepository bloodProductRepository)
        {
            this.bloodProductRepository = bloodProductRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Donation{T}"/> class.
        /// </summary>
        /// <param name="donorRepository">IDonorRepository parameter.</param>
        /// <param name="donorCandidateRepository">IDonorCandidateRepository parameter.</param>
        public Donation(IDonorRepository donorRepository, IDonorCandidateRepository donorCandidateRepository)
        {
            this.donorRepository = donorRepository;
            this.donorCandidateRepository = donorCandidateRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Donation{T}"/> class.
        /// </summary>
        /// <param name="bloodProductRepository">IBloodProductRepository parameter.</param>
        /// <param name="donorRepository">IDonorRepository parameter.</param>
        /// <param name="donorCandidateRepository">IDonorCandidateRepository parameter.</param>
        public Donation(IBloodProductRepository bloodProductRepository, IDonorRepository donorRepository, IDonorCandidateRepository donorCandidateRepository)
        {
            this.bloodProductRepository = bloodProductRepository;
            this.donorRepository = donorRepository;
            this.donorCandidateRepository = donorCandidateRepository;
        }

        /// <summary>
        /// implemets ChangeDateOfLastDonation method.
        /// </summary>
        /// <param name="donorID">donor id as string.</param>
        /// <param name="lastdonation">date of last donation as DateTime.</param>
        public void ChangeDateOfLastDonation(string donorID, DateTime lastdonation)
        {
            this.donorRepository.ChangeDateOfLastDonation(donorID, lastdonation);
        }

        /// <summary>
        /// implements ChangeNursesName method.
        /// </summary>
        /// <param name="donorID">donor id as string.</param>
        /// <param name="nursesname">name of the nurse as string.</param>
        public void ChangeNursesName(string donorID, string nursesname)
        {
            this.donorRepository.ChangeNursesName(donorID, nursesname);
        }

        /// <summary>
        /// Gives back all the donation places in Budapest.
        /// </summary>
        /// <returns>Returns with the candidates name and the exact address where they were registered.</returns>
        public Dictionary<string, string> DonationPlacesInBudapest()
        {
            var locationBP = from place in this.donorRepository.GetAll()
                             join candidate in this.donorCandidateRepository.GetAll() on place.Tajnumber equals candidate.Tajnumber
                             where place.AddressOfRegistration.ToUpper().Contains("BUDAPEST")
                             select new { name = candidate.CandidateName, bp = place.AddressOfRegistration };

            Dictionary<string, string> placeList = new Dictionary<string, string>();

            foreach (var item in locationBP)
            {
                placeList.Add(item.name, item.bp);
            }

            return placeList;
        }

        /// <summary>
        /// Gives back a task to retund all the donation places in budapest.
        /// </summary>
        /// <returns>Returns with task.</returns>
        public Task<Dictionary<string, string>> DonationPlacesInBudapestAsync()
        {
            return Task<Dictionary<string, string>>.Factory.StartNew(() =>
            {
                var locationBP = from place in this.donorRepository.GetAll()
                                 join candidate in this.donorCandidateRepository.GetAll() on place.Tajnumber equals candidate.Tajnumber
                                 where place.AddressOfRegistration.ToUpper().Contains("BUDAPEST")
                                 select new { name = candidate.CandidateName, bp = place.AddressOfRegistration };

                Dictionary<string, string> placeList = new Dictionary<string, string>();

                foreach (var item in locationBP)
                {
                    placeList.Add(item.name, item.bp);
                }

                return placeList;
            });
        }

        /// <summary>
        /// implemets GetBloodTypeNumbers method.
        /// </summary>
        /// <returns>returns with a dicionary containing blood type numbers.</returns>
        public Dictionary<string, int> GetBloodTypeNumbers()
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();

            if (this.donorRepository == null)
            {
                string ex = "bloods";
                throw new ArgumentNullException(ex);
            }

            var commons = from blood in this.donorRepository.GetAll()
                          group blood by blood.BloodType into grp
                          select new { Type = grp.Key, Count = grp.Count() };

            foreach (var item in commons)
            {
                dictionary.Add(item.Type, item.Count);
            }

            return dictionary;
        }

        /// <summary>
        /// the method gives back a task with how many blood product are in the blood bank per blood type.
        /// </summary>
        /// <returns>returns with a task..</returns>
        public Task<Dictionary<string, int>> GetBloodTypeNumbersAsync()
        {
            return Task<Dictionary<string, int>>.Factory.StartNew(() =>
            {
                Dictionary<string, int> dictionary = new Dictionary<string, int>();

                var commons = from blood in this.donorRepository.GetAll()
                              group blood by blood.BloodType into grp
                              select new { Type = grp.Key, Count = grp.Count() };

                foreach (var item in commons)
                {
                    dictionary.Add(item.Type, item.Count);
                }

                return dictionary;
            });
        }

        /// <summary>
        /// implemets GetAllDonormethod.
        /// </summary>
        /// <returns>returns all Donor objects in a List.</returns>
        public IList<Donor> GetAllDonor()
        {
            return this.donorRepository.GetAll().ToList();
        }

        /// <summary>
        /// implemets GetOneDonor method.
        /// </summary>
        /// <param name="id">donor id as string.</param>
        /// <returns>returns one specific donor object.</returns>
        public Donor GetOneDonor(string id)
        {
            return this.donorRepository.GetOne(id);
        }

        /// <summary>
        /// implemets InsertDonor method.
        /// </summary>
        /// <param name="entity">new Donor object as parameter.</param>
        public void InsertDonor(Donor entity)
        {
            this.donorRepository.Insert(entity);
        }

        /// <summary>
        /// implemets InsertBP method.
        /// </summary>
        /// <param name="entity"> new Bloodproduct object as parameter.</param>
        public void InsertBP(Bloodproduct entity)
        {
            this.bloodProductRepository.Insert(entity);
        }

        /// <summary>
        /// implemets RemoveDonor method.
        /// </summary>
        /// <param name="entity">removable Donor object as parameter.</param>
        /// <returns>returns bool value.</returns>
        public bool RemoveDonor(Donor entity)
        {
            return this.donorRepository.Remove(entity);
        }

        /// <summary>
        /// implemets UnUsedBloodProducts method.
        /// </summary>
        /// <returns>returns the unused product ids with it's donor's name.</returns>
        public Dictionary<string, string> UnUsedBloodProducts()
        {
            var unUsed = from product in this.bloodProductRepository.GetAll()
                         join donor in this.donorRepository.GetAll() on product.Donor equals donor.DonorId
                         join candidate in this.donorCandidateRepository.GetAll() on donor.Tajnumber equals candidate.Tajnumber
                         where product.Recipient == null
                         select new { id = product.BloodproductId, donor = candidate.CandidateName };

            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            foreach (var item in unUsed)
            {
                dictionary.Add(item.id, item.donor);
            }

            return dictionary;
        }

        /// <summary>
        /// implemets task to return unused blood product.
        /// </summary>
        /// <returns>returns a task.</returns>
        public Task<Dictionary<string, string>> UnUsedBloodProductsAsync()
        {
            return Task<Dictionary<string, string>>.Factory.StartNew(() =>
            {
                var unUsed = from product in this.bloodProductRepository.GetAll()
                             join donor in this.donorRepository.GetAll() on product.Donor equals donor.DonorId
                             join candidate in this.donorCandidateRepository.GetAll() on donor.Tajnumber equals candidate.Tajnumber
                             where product.Recipient == null
                             select new
                             {
                                 id = product.BloodproductId,
                                 donor = candidate.CandidateName,
                             };

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (var item in unUsed)
                {
                    dictionary.Add(item.id, item.donor);
                }

                return dictionary;
            });
        }
    }
}
