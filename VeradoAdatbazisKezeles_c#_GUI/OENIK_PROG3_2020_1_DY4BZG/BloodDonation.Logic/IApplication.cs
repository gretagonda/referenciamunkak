﻿// <copyright file="IApplication.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Logic
{
    using System.Collections.Generic;
    using BloodDonation.Data;

    /// <summary>
    /// declares IApplication interface.
    /// </summary>
    /// <typeparam name="T">generic type.</typeparam>
    public interface IApplication<T>
        where T : class
    {
        /// <summary>
        /// declares ChangeSuitability method for IApplication interface.
        /// </summary>
        /// <param name="id">id as string parameter.</param>
        /// <param name="suitablility">suitability as bool parameter.</param>
        void ChangeSuitability(string id, bool suitablility);

        /// <summary>
        /// declares ChangeConstantAddressForCandidate method for IApplication interface.
        /// </summary>
        /// <param name="id">id as string parameter.</param>
        /// <param name="newAddress">address as string parameter.</param>
        /// <returns>bool value.</returns>
        bool ChangeConstantAddressForCandidate(string id, string newAddress);

        /// <summary>
        /// declares InsertDC method for IApplication interface.
        /// </summary>
        /// <param name="entity">parameter as DonorCandidate.</param>
        void InsertDC(DonorCandidate entity);

        /// <summary>
        /// declares RemoveDC method for IApplication interface.
        /// </summary>
        /// <param name="entity">parameter as DonorCandidate.</param>
        /// <returns>return bool value.</returns>
        bool RemoveDC(DonorCandidate entity);

        /// <summary>
        /// declares GetOneDC method for IApplication interface.
        /// </summary>
        /// <param name="id">id as string parameter.</param>
        /// <returns>returns one specific DonorCandidate.</returns>
        DonorCandidate GetOneDC(string id);

        /// <summary>
        /// declares GetAllDC method for IApplication interface.
        /// </summary>
        /// <returns>returns all DonorCandidates.</returns>
        IList<DonorCandidate> GetAllDC();
    }
}
