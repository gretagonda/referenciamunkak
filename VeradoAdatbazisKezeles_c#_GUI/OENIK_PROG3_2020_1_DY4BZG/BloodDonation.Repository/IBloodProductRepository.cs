﻿// <copyright file="IBloodProductRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Repository
{
    using BloodDonation.Data;

    /// <summary>
    /// declaring IBloodProductRepository interface.
    /// </summary>
    public interface IBloodProductRepository : IRepository<Bloodproduct>
    {
        /// <summary>
        /// declaring ChangeTargetHospitalsName method.
        /// </summary>
        /// <param name="productID">first string parameter.</param>
        /// <param name="hospitalname">second string parameter.</param>
        void ChangeTargetHospitalsName(string productID, string hospitalname);

        /// <summary>
        /// declaring ChangeTargetHospitalsAddress method.
        /// </summary>
        /// <param name="productID">first string id.</param>
        /// <param name="hospitaladdresss">second string id.</param>
        void ChangeTargetHospitalsAddress(string productID, string hospitaladdresss);

        /// <summary>
        /// declaring ChangeRecipient method.
        /// </summary>
        /// <param name="productID">first string id.</param>
        /// <param name="taj">second string id.</param>
        void ChangeRecipient(string productID, string taj);
    }
}
