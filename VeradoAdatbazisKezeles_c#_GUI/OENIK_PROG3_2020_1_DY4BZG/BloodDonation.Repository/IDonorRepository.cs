﻿// <copyright file="IDonorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Repository
{
    using System;
    using BloodDonation.Data;

    /// <summary>
    /// declaring IDonorRepository interface.
    /// </summary>
    public interface IDonorRepository : IRepository<Donor>
    {
        /// <summary>
        /// declaring ChangeDateOfLastDonation method.
        /// </summary>
        /// <param name="donorID">first string parameter.</param>
        /// <param name="lastdonation">second string parameter.</param>
        void ChangeDateOfLastDonation(string donorID, DateTime lastdonation);

        /// <summary>
        /// declaring ChangeNursesName method.
        /// </summary>
        /// <param name="donorID">first string parameter.</param>
        /// <param name="nursesname">second string parameter.</param>
        void ChangeNursesName(string donorID, string nursesname);
    }
}
