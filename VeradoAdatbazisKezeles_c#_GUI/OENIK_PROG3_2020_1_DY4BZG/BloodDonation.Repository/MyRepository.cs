﻿// <copyright file="MyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
[assembly: System.CLSCompliant(false)]

namespace BloodDonation.Repository
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// declaring abstract Repository class.
    /// </summary>
    /// <typeparam name="T">generic type parameter.</typeparam>
    public abstract class MyRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// declaring ctx.
        /// </summary>
        protected DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="MyRepository{T}"/> class.
        /// </summary>
        /// <param name="ctx">DbContext parameter.</param>
        protected MyRepository(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// implementing generic GetAll method.
        /// </summary>
        /// <returns>ctx members.</returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }

        /// <summary>
        /// implementing generic GetOne method.
        /// </summary>
        /// <param name="id">string parameter.</param>
        /// <returns>return one specific member.</returns>
        public abstract T GetOne(string id);

        /// <summary>
        /// implementing generic Insert method.
        /// </summary>
        /// <param name="entity">generic parameter.</param>
        public void Insert(T entity)
        {
            this.ctx.Set<T>().Add(entity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// implementing generic Remove method.
        /// </summary>
        /// <param name="entity">generic parameter.</param>
        /// <returns>bool value.</returns>
        public bool Remove(T entity)
        {
            try
            {
                this.ctx.Set<T>().Remove(entity);

                this.ctx.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
