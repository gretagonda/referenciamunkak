﻿// <copyright file="DonorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BloodDonation.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// declares DonorRepository class.
    /// </summary>
    public class DonorRepository : MyRepository<Donor>, IDonorRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DonorRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext parameter.</param>
        public DonorRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// implements ChangeDateOfLastDonation to DonorRepository.
        /// </summary>
        /// <param name="donorID">donorID as string.</param>
        /// <param name="lastdonation">time of last donation as DateTime.</param>
        public void ChangeDateOfLastDonation(string donorID, DateTime lastdonation)
        {
            var donor = this.GetOne(donorID);
            if (donor == null)
            {
                throw new InvalidOperationException("NOT FOUND");
            }
            else
            {
                donor.DateOfLastDonation = lastdonation;
                this.ctx.SaveChanges();
            }
        }

        /// <summary>
        /// implements ChangeNursesName to DonorRepository.
        /// </summary>
        /// <param name="donorID">donorID as string.</param>
        /// <param name="nursesname">nurses name as string.</param>
        public void ChangeNursesName(string donorID, string nursesname)
        {
            var donor = this.GetOne(donorID);
            donor.NursesName = nursesname;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// implements GetOne to DonorRepository.
        /// </summary>
        /// <param name="id">id as string.</param>
        /// <returns>return one specific object.</returns>
        public override Donor GetOne(string id)
        {
            return this.GetAll().SingleOrDefault(x => x.DonorId == id);
        }
    }
}
