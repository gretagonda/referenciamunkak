﻿// <copyright file="BloodProductRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BloodDonation.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// declaring BloodProductRepository class.
    /// </summary>
    public class BloodProductRepository : MyRepository<Bloodproduct>, IBloodProductRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BloodProductRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext parameter.</param>
        public BloodProductRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Initializes a new ChangeRecipient method of BloodProductRepository.
        /// </summary>
        /// <param name="productID">string parameter contains productId.</param>
        /// <param name="taj">string parameter contains tajnumber.</param>
        public void ChangeRecipient(string productID, string taj)
        {
            var product = this.GetOne(productID);
            product.Recipient = taj;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Initializes a new ChangeTargetHospitalsAddress method of BloodProductRepository.
        /// </summary>
        /// <param name="productID">string parameter contains productId.</param>
        /// <param name="hospitaladdresss">string parameter contains hospitaladdress.</param>
        public void ChangeTargetHospitalsAddress(string productID, string hospitaladdresss)
        {
            var product = this.GetOne(productID);
            product.TargetHospitalsAddress = hospitaladdresss;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Initializes a new ChangeTargetHospitalsName method of BloodProductRepository.
        /// </summary>
        /// <param name="productID">string parameter contains productId.</param>
        /// <param name="hospitalname">string parameter contains hospitalname.</param>
        public void ChangeTargetHospitalsName(string productID, string hospitalname)
        {
            var product = this.GetOne(productID);
            product.TargetHospitalsName = hospitalname;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// implements GetOne method.
        /// </summary>
        /// <param name="id">string id parameter.</param>
        /// <returns>returns one Bloodproduct object.</returns>
        public override Bloodproduct GetOne(string id)
        {
            return this.GetAll().SingleOrDefault(x => x.BloodproductId == id);
        }
    }
}
