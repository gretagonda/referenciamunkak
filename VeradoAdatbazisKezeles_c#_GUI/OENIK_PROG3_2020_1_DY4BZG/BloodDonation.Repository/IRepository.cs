﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Repository
{
    using System.Linq;

    /// <summary>
    /// declaring IRepository interface.
    /// </summary>
    /// <typeparam name="T">generic parameter.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        // Create-Read-Delete

        /// <summary>
        /// declaring generic GetOne method.
        /// </summary>
        /// <param name="id">given string parameter.</param>
        /// <returns>returns one element.</returns>
        T GetOne(string id);

        /// <summary>
        /// declaring generic GetAll method.
        /// </summary>
        /// <returns>return all elements from specific data source.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// declaring generic Insert method.
        /// </summary>
        /// <param name="entity">generic parameter.</param>
        void Insert(T entity);

        /// <summary>
        /// declaring generic Remove method.
        /// </summary>
        /// <param name="entity">generic parameter.</param>
        /// <returns>bool type.</returns>
        bool Remove(T entity);
    }
}
