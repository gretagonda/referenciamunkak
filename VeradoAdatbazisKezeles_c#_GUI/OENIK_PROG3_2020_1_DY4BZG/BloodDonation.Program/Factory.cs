﻿// <copyright file="Factory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Program
{
    using BloodDonation.Data;
    using BloodDonation.Logic;
    using BloodDonation.Repository;

    /// <summary>
    /// declares Factory class.
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Factory"/> class.
        /// </summary>
        public Factory()
        {
            this.Ctx = new BloodDbContext();

            this.RepoBloodProduct = new BloodProductRepository(this.Ctx);
            this.RepoDonorCandidate = new DonorCandidateRepository(this.Ctx);
            this.RepoDonor = new DonorRepository(this.Ctx);
            this.RepoRecipient = new RecipientRepository(this.Ctx);

            this.GetDC = new Application<DonorCandidate>(this.RepoDonorCandidate);
            this.GetD = new Donation<Donor>(this.RepoDonor);
            this.GetBP = new Transfusion<Bloodproduct>(this.RepoBloodProduct);
            this.GetR = new Transfusion<Recipient>(this.RepoRecipient);

            this.ChangeCandidate = new Application<DonorCandidate>(this.RepoDonorCandidate);
            this.ChangeDonor = new Donation<Donor>(this.RepoDonor);
            this.ChangeRecipient = new Transfusion<Recipient>(this.RepoRecipient);
            this.ChangeProduct = new Transfusion<Bloodproduct>(this.RepoBloodProduct);

            this.InOrOutCandidate = new Application<DonorCandidate>(this.RepoDonorCandidate);
            this.InOrOutDonor = new Donation<Donor>(this.RepoDonor);
            this.InOrOutRecipient = new Transfusion<Recipient>(this.RepoRecipient);
            this.InProduct = new Donation<Bloodproduct>(this.RepoBloodProduct);
            this.OutProduct = new Transfusion<Bloodproduct>(this.RepoBloodProduct);

            this.CommonBlood = new Donation<Donor>(this.RepoDonor);

            this.UnUsedBloods = new Donation<Bloodproduct>(this.RepoBloodProduct, this.RepoDonor, this.RepoDonorCandidate);

            this.LocationBP = new Donation<Donor>(this.RepoDonor, this.RepoDonorCandidate);
        }

        /// <summary>
        /// Gets - declares BloodDbContext variable.
        /// </summary>
        public BloodDbContext Ctx { get; }

        /// <summary>
        /// Gets RepoBloodProduct declaration.
        /// </summary>
        public BloodProductRepository RepoBloodProduct { get; }

        /// <summary>
        /// Gets RepoDonorCandidate declaration.
        /// </summary>
        public DonorCandidateRepository RepoDonorCandidate { get; }

        /// <summary>
        /// Gets RepoDonor declaration.
        /// </summary>
        public DonorRepository RepoDonor { get; }

        /// <summary>
        /// Gets RepoRecipient declaration.
        /// </summary>
        public RecipientRepository RepoRecipient { get; }

        /// <summary>
        /// Gets or Sets GetDC declartion with DonorCandidate type.
        /// </summary>
        public Application<DonorCandidate> GetDC { get; set; }

        /// <summary>
        /// Gets or Sets GetD declartion with DonorCandidate type.
        /// </summary>
        public Donation<Donor> GetD { get; set; }

        /// <summary>
        /// Gets or Sets GetBP declartion with Bloodproduct type.
        /// </summary>
        public Transfusion<Bloodproduct> GetBP { get; set; }

        /// <summary>
        /// Gets or Sets GetR declartion with Recipient type.
        /// </summary>
        public Transfusion<Recipient> GetR { get; set; }

        /// <summary>
        /// Gets or Sets ChangeCandidate declartion with DonorCandidate type.
        /// </summary>
        public Application<DonorCandidate> ChangeCandidate { get; set; }

        /// <summary>
        /// Gets or Sets ChangeDonor declartion with Donor type.
        /// </summary>
        public Donation<Donor> ChangeDonor { get; set; }

        /// <summary>
        /// Gets or Sets ChangeRecipient declartion with Recipient type.
        /// </summary>
        public Transfusion<Recipient> ChangeRecipient { get; set; }

        /// <summary>
        /// Gets or Sets ChangeProduct declartion with Bloodproduct type.
        /// </summary>
        public Transfusion<Bloodproduct> ChangeProduct { get; set; }

        /// <summary>
        /// Gets or Sets InOrOutCandidate declartion with DonorCandidate type.
        /// </summary>
        public Application<DonorCandidate> InOrOutCandidate { get; set; }

        /// <summary>
        /// Gets or Sets InOrOutDonor declartion with Donor type.
        /// </summary>
        public Donation<Donor> InOrOutDonor { get; set; }

        /// <summary>
        /// Gets or Sets InOrOutRecipient declartion with Recipient type.
        /// </summary>
        public Transfusion<Recipient> InOrOutRecipient { get; set; }

        /// <summary>
        /// Gets or Sets InProduct declartion with BloodProduct type.
        /// </summary>
        public Donation<Bloodproduct> InProduct { get; set; }

        /// <summary>
        /// Gets or Sets OutProduct declartion with BloodProduct type.
        /// </summary>
        public Transfusion<Bloodproduct> OutProduct { get; set; }

        /// <summary>
        /// Gets or Sets CommonBlood declartion with Donor type.
        /// </summary>
        public Donation<Donor> CommonBlood { get; set; }

        /// <summary>
        /// Gets or Sets UnUsedBloods declartion with BloodProduct type.
        /// </summary>
        public Donation<Bloodproduct> UnUsedBloods { get; set; }

        /// <summary>
        /// Gets or Sets LocationBP declartion with Donor type.
        /// </summary>
        public Donation<Donor> LocationBP { get; set; }
    }
}
