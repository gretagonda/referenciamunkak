﻿// <copyright file="MyProgram.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
[assembly: System.CLSCompliant(false)]

namespace BloodDonation.Program
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using BloodDonation.Data;
    using Castle.Core.Internal;
    using ConsoleTools;

    /// <summary>
    /// declares Program class.
    /// </summary>
    public class MyProgram
    {
        [Conditional("MAIN")]
        private static void Main(string[] args)
        {
            // Scaffold-DbContext "Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename = |DataDirectory|\BloodDonationDatabase.mdf; Integrated Security = True" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models
            Factory donationFactory = new Factory();

            // IList<Donor> donorList = donationFactory.getD.GetAll();
            var menu = new ConsoleMenu().
                Add("Write donor candidate table to console", () => GetDonorCandidateList(donationFactory)).
                Add("Write donor table to console", () => GetDonorList(donationFactory)).
                Add("Write blood product table to console", () => GetBloodproductList(donationFactory)).
                Add("Write recipient table to console", () => GetRecipientList(donationFactory)).
                Add("Add new donor candidate to donor candidate table", () => NewCandidate(donationFactory)).
                Add("Return specific donor candidate", () => OneCandidate(donationFactory)).
                Add("Remove candidate with specified tajnumber", () => RemoveCandidate(donationFactory)).
                Add("Change the address of a donor canidate", () => AddressChangeCandidate(donationFactory)).
                Add("Change the suitability of a donor candidate", () => SuitabilityChangeCandidate(donationFactory)).
                Add("Add new donor to donor table", () => NewDonor(donationFactory)).
                Add("Return specific donor", () => OneDonor(donationFactory)).
                Add("Remove donor  with specified donor ID", () => RemoveDonor(donationFactory)).
                Add("Change the date of the last donation", () => LastdonationChangeDonor(donationFactory)).
                Add("Change the nurse of the donor", () => NurseChangeDonor(donationFactory)).
                Add("Add new blood product to blood product table", () => NewBloodProduct(donationFactory)).
                Add("Return specific blood product", () => OneProduct(donationFactory)).
                Add("Remove blood product with specified blood product ID", () => RemoveBloodProduct(donationFactory)).
                Add("Change the recipient of the product", () => RecipientChangeProduct(donationFactory)).
                Add("Change the target hospital's name", () => HospitalNameChangeProduct(donationFactory)).
                Add("Change the target hospital's location", () => HospitalAddressProduct(donationFactory)).
                Add("Add new recipient to recipient table", () => NewRecipient(donationFactory)).
                Add("Return specific recipient", () => OneRecipient(donationFactory)).
                Add("Remove recipient  with specified tajnumber", () => RemoveRecipient(donationFactory)).
                Add("Change recipient's address", () => AddressChangeRecipient(donationFactory)).
                Add("Tell me how many donor has a specific blood type", () => CommonBlood(donationFactory)).
                Add("Tell me how many donor has a specific blood type - Async", () => CommonBloodAsync(donationFactory)).
                Add("List the IDs if the unused blood products with their donor's names: ", () => UnusedBloods(donationFactory)).
                Add("List the IDs if the unused blood products with their donor's names - Async: ", () => UnusedBloodsAsync(donationFactory)).
                Add("List the donors from the donation locations in Budapest: ", () => BPdonation(donationFactory)).
                Add("List the donors from the donation locations in Budapest - Async: ", () => BPdonationAsync(donationFactory)).
                Add("Close", ConsoleMenu.Close);
            menu.Show();
        }

        private static void GetDonorCandidateList(Factory factory)
        {
            var list = factory.GetDC.GetAllDC();
            Console.WindowWidth = Console.LargestWindowWidth;

            Console.Clear();

            Console.SetCursorPosition(0, 20);
            Console.WriteLine("Candidate's name ");
            Console.SetCursorPosition(25, 20);
            Console.Write("Date of birth");
            Console.SetCursorPosition(50, 20);
            Console.Write("Address");
            Console.SetCursorPosition(90, 20);
            Console.Write("Tajnumber");
            Console.SetCursorPosition(110, 20);
            Console.Write("Doctor's name");
            Console.SetCursorPosition(130, 20);
            Console.Write("Suitability");

            int put = 1;

            foreach (var item in list)
            {
                Console.SetCursorPosition(0, 20 + put);
                Console.WriteLine(item.CandidateName);
                Console.SetCursorPosition(25, 20 + put);
                Console.WriteLine(item.BirthDate);
                Console.SetCursorPosition(50, 20 + put);
                Console.WriteLine(item.ConstantAddress);
                Console.SetCursorPosition(90, 20 + put);
                Console.WriteLine(item.Tajnumber);
                Console.SetCursorPosition(110, 20 + put);
                Console.WriteLine(item.DoctorsName);
                Console.SetCursorPosition(130, 20 + put);
                Console.WriteLine(item.Suitability);
                put++;
            }

            Console.ReadLine();
        }

        private static void OneCandidate(Factory factory)
        {
            try
            {
                Console.WriteLine("Tajnumber of the candidate:");
                string id = Console.ReadLine();
                DonorCandidate candidate = factory.GetDC.GetOneDC(id);

                Console.WriteLine("Candidate's name: " + candidate.CandidateName);

                Console.WriteLine("Date of birth: " + candidate.BirthDate);

                Console.WriteLine("Address: " + candidate.ConstantAddress);

                Console.WriteLine("Tajnumber:" + candidate.Tajnumber);

                Console.WriteLine("Doctor's name: " + candidate.DoctorsName);

                Console.WriteLine("Suitability: " + candidate.Suitability);

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void GetDonorList(Factory factory)
        {
            var list = factory.GetD.GetAllDonor();
            Console.WindowWidth = Console.LargestWindowWidth;

            Console.Clear();

            Console.SetCursorPosition(0, 5);
            Console.WriteLine("Donor ID ");
            Console.SetCursorPosition(35, 5);
            Console.Write("Gender");
            Console.SetCursorPosition(45, 5);
            Console.Write("Tajnumber");
            Console.SetCursorPosition(59, 5);
            Console.Write("Date of last donation");
            Console.SetCursorPosition(85, 5);
            Console.Write("Nurse's name");
            Console.SetCursorPosition(105, 5);
            Console.Write("Bloodtype");
            Console.SetCursorPosition(120, 5);
            Console.Write("Location of registration");
            Console.SetCursorPosition(170, 5);
            Console.Write("Address of registration");

            int put = 1;

            foreach (var item in list)
            {
                Console.SetCursorPosition(0, 5 + put);
                Console.Write(item.DonorId);
                Console.SetCursorPosition(35, 5 + put);
                Console.Write(item.Gender);
                Console.SetCursorPosition(45, 5 + put);
                Console.Write(item.Tajnumber);
                Console.SetCursorPosition(59, 5 + put);
                Console.Write(item.DateOfLastDonation);
                Console.SetCursorPosition(85, 5 + put);
                Console.Write(item.NursesName);
                Console.SetCursorPosition(105, 5 + put);
                Console.Write(item.BloodType);
                Console.SetCursorPosition(120, 5 + put);
                Console.Write(item.LocationOfRegistration);
                Console.SetCursorPosition(170, 5 + put);
                Console.Write(item.AddressOfRegistration);

                put++;
            }

            Console.ReadLine();
        }

        private static void OneDonor(Factory factory)
        {
            try
            {
                Console.WriteLine("Donor id of the donor:");
                string id = Console.ReadLine();
                Donor donor = new Donor();
                donor = factory.GetD.GetOneDonor(id);

                Console.WriteLine("Donor id: " + donor.DonorId);

                Console.WriteLine("Gender: " + donor.Gender);

                Console.WriteLine("Tajnumber: " + donor.Tajnumber);

                Console.WriteLine("Date of last donation:" + donor.DateOfLastDonation);

                Console.WriteLine("Nurse's name: " + donor.NursesName);

                Console.WriteLine("Blood type: " + donor.BloodType);

                Console.WriteLine("Location od registration: " + donor.LocationOfRegistration);

                Console.WriteLine("Address of registration: " + donor.AddressOfRegistration);

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void GetBloodproductList(Factory factory)
        {
            var list = factory.GetBP.GetAllBP();
            Console.WindowWidth = Console.LargestWindowWidth;

            Console.Clear();

            Console.SetCursorPosition(0, 5);
            Console.WriteLine("Blood product ID ");
            Console.SetCursorPosition(20, 5);
            Console.Write("Donor ID");
            Console.SetCursorPosition(40, 5);
            Console.Write("Expiration date");
            Console.SetCursorPosition(70, 5);
            Console.Write("Type of the blood product");
            Console.SetCursorPosition(100, 5);
            Console.Write("Name of target hospital");
            Console.SetCursorPosition(150, 5);
            Console.Write("Address of target hospital");
            Console.SetCursorPosition(190, 5);
            Console.Write("Recipient's tajnumber");

            int put = 1;

            foreach (var item in list)
            {
                Console.SetCursorPosition(0, 5 + put);
                Console.WriteLine(item.BloodproductId);
                Console.SetCursorPosition(20, 5 + put);
                Console.WriteLine(item.Donor);
                Console.SetCursorPosition(40, 5 + put);
                Console.WriteLine(item.ExpireDate);
                Console.SetCursorPosition(70, 5 + put);
                Console.WriteLine(item.ProductType);
                Console.SetCursorPosition(100, 5 + put);
                Console.WriteLine(item.TargetHospitalsName);
                Console.SetCursorPosition(150, 5 + put);
                Console.WriteLine(item.TargetHospitalsAddress);
                Console.SetCursorPosition(195, 5 + put);
                Console.WriteLine(item.Recipient);
                put++;
            }

            Console.ReadLine();
        }

        private static void OneProduct(Factory factory)
        {
            try
            {
                Console.WriteLine("ID of Blood product:");
                string id = Console.ReadLine();
                Bloodproduct product = factory.GetBP.GetOneBP(id);

                Console.WriteLine("Product id: " + product.BloodproductId);

                Console.WriteLine("Donor id: " + product.Donor);

                Console.WriteLine("Expiration date: " + product.ExpireDate);

                Console.WriteLine("Type of blood product:" + product.ProductType);

                Console.WriteLine("Name of teh target hospital: " + product.TargetHospitalsName);

                Console.WriteLine("Address of the target hospital: " + product.TargetHospitalsAddress);

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void GetRecipientList(Factory factory)
        {
            var list = factory.GetR.GetAllR();
            Console.WindowWidth = Console.LargestWindowWidth;

            Console.Clear();

            Console.SetCursorPosition(0, 5);
            Console.WriteLine("Recipient's name ");
            Console.SetCursorPosition(20, 5);
            Console.Write("Gender");
            Console.SetCursorPosition(40, 5);
            Console.Write("Date of birth");
            Console.SetCursorPosition(70, 5);
            Console.Write("Place of birth");
            Console.SetCursorPosition(120, 5);
            Console.Write("Address");
            Console.SetCursorPosition(165, 5);
            Console.Write("Tajnumber");
            int put = 1;

            foreach (var item in list)
            {
                Console.SetCursorPosition(0, 5 + put);
                Console.WriteLine(item.RecipientName);
                Console.SetCursorPosition(20, 5 + put);
                Console.WriteLine(item.Gender);
                Console.SetCursorPosition(40, 5 + put);
                Console.WriteLine(item.BirthDate);
                Console.SetCursorPosition(70, 5 + put);
                Console.WriteLine(item.BirthPlace);
                Console.SetCursorPosition(120, 5 + put);
                Console.WriteLine(item.ConstantAddress);
                Console.SetCursorPosition(165, 5 + put);
                Console.WriteLine(item.Tajnumber);

                put++;
            }

            Console.ReadLine();
        }

        private static void OneRecipient(Factory factory)
        {
            try
            {
                Console.WriteLine("Tajnumber of the recipient:");
                string id = Console.ReadLine();
                Recipient recipient = factory.GetR.GetOneR(id);

                Console.WriteLine("Name: " + recipient.RecipientName);

                Console.WriteLine("Gender: " + recipient.Gender);

                Console.WriteLine("Date of birth: " + recipient.BirthDate);

                Console.WriteLine("Place of birth:" + recipient.BirthPlace);

                Console.WriteLine("Address: " + recipient.ConstantAddress);

                Console.WriteLine("Tajnumber: " + recipient.Tajnumber);

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void NewCandidate(Factory factory)
        {
            DonorCandidate newCandidate = new DonorCandidate();

            try
            {
                Console.WriteLine("new candidate's name:");

                string name = Console.ReadLine();

                Console.WriteLine("Date of Birth:");

                DateTime birthDate = DateTime.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                Console.WriteLine("Address");

                string address = Console.ReadLine();

                Console.WriteLine("Tajnumber:");

                string tajnumber = Console.ReadLine();

                Console.WriteLine("Doctor's name:");

                string doctor = Console.ReadLine();

                Console.WriteLine("Suitability (true if suitable or false if not):");

                bool suit = bool.Parse(Console.ReadLine());

                newCandidate.CandidateName = name;

                newCandidate.BirthDate = birthDate;

                newCandidate.ConstantAddress = address;

                newCandidate.Tajnumber = tajnumber;

                newCandidate.DoctorsName = doctor;

                newCandidate.Suitability = suit;

                factory.InOrOutCandidate.InsertDC(newCandidate);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void RemoveCandidate(Factory factory)
        {
            Console.WriteLine("Tajnumer of removable donor candidate");

            string taj = Console.ReadLine();

            try
            {
                DonorCandidate removableDonor = factory.GetDC.GetOneDC(taj);
                factory.InOrOutCandidate.RemoveDC(removableDonor);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void AddressChangeCandidate(Factory factory)
        {
            DonorCandidate uptodate = new DonorCandidate();

            try
            {
                Console.WriteLine("Tajnumer of donor candidate");

                string taj = Console.ReadLine();

                Console.WriteLine("New address");

                string address = Console.ReadLine();

                uptodate = factory.GetDC.GetOneDC(taj);

                factory.ChangeCandidate.ChangeConstantAddressForCandidate(taj, address);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void SuitabilityChangeCandidate(Factory factory)
        {
            try
            {
                Console.WriteLine("Tajnumer of donor candidate");

                string taj = Console.ReadLine();

                Console.WriteLine("New suitability status (false - not suitable, true - suitable) :");

                bool suit = bool.Parse(Console.ReadLine());

                DonorCandidate uptodate = new DonorCandidate();

                uptodate = factory.GetDC.GetOneDC(taj);

                factory.ChangeCandidate.ChangeSuitability(taj, suit);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void CommonBlood(Factory factory)
        {
                var commons = factory.CommonBlood.GetBloodTypeNumbers();

                foreach (var item in commons)
                {
                    Console.WriteLine(item.Key + ":  " + item.Value);
                }

                Console.ReadLine();
        }

        private static void CommonBloodAsync(Factory factory)
        {
            var commons = factory.CommonBlood.GetBloodTypeNumbersAsync().Result;

            foreach (var item in commons)
            {
                Console.WriteLine(item.Key + ":  " + item.Value);
            }

            Console.ReadLine();
        }

        private static void UnusedBloods(Factory factory)
        {
            var unUsed = factory.UnUsedBloods.UnUsedBloodProducts();

            foreach (var item in unUsed)
            {
                Console.WriteLine(item.Key + ":  " + item.Value);
            }

            Console.ReadLine();
        }

        private static void UnusedBloodsAsync(Factory factory)
        {
            var unUsed = factory.UnUsedBloods.UnUsedBloodProductsAsync().Result;

            foreach (var item in unUsed)
            {
                Console.WriteLine(item.Key + ":  " + item.Value);
            }

            Console.ReadLine();
        }

        private static void BPdonation(Factory factory)
        {
            var locBP = factory.LocationBP.DonationPlacesInBudapest();

            foreach (var item in locBP)
            {
                Console.WriteLine(item.Key + ":  " + item.Value);
            }

            Console.ReadLine();
        }

        private static void BPdonationAsync(Factory factory)
        {
            var locBP = factory.LocationBP.DonationPlacesInBudapestAsync().Result;

            foreach (var item in locBP)
            {
                Console.WriteLine(item.Key + ":  " + item.Value);
            }

            Console.ReadLine();
        }

        private static void NewDonor(Factory factory)
        {
            try
            {
                Donor newDonor = new Donor();

                Console.WriteLine("Donor ID of the new donor:");

                string id = Console.ReadLine();

                Console.WriteLine("Gender: (man or woman)");

                string gender = Console.ReadLine();

                Console.WriteLine("Tajnumber - (secondary key to DonorCandidate table!)");

                string taj = Console.ReadLine();

                Console.WriteLine("Date of last donation:");

                DateTime lastDonation = DateTime.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                Console.WriteLine("Nurse's name:");

                string nurse = Console.ReadLine();

                Console.WriteLine("Blood type:");

                string bloodtype = Console.ReadLine();

                Console.WriteLine("Location of registration");

                string regLoc = Console.ReadLine();

                Console.WriteLine("Address of registration");

                string regAddr = Console.ReadLine();

                newDonor.DonorId = id;
                newDonor.Gender = gender;
                newDonor.Tajnumber = taj;
                newDonor.DateOfLastDonation = lastDonation;
                newDonor.NursesName = nurse;
                newDonor.BloodType = bloodtype;
                newDonor.LocationOfRegistration = regLoc;
                newDonor.AddressOfRegistration = regAddr;

                factory.InOrOutDonor.InsertDonor(newDonor);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void RemoveDonor(Factory factory)
        {
            try
            {
                Donor removableDonor = new Donor();

                Console.WriteLine("Donor ID of removable donor:");

                string id = Console.ReadLine();

                removableDonor = factory.GetD.GetOneDonor(id);

                factory.InOrOutDonor.RemoveDonor(removableDonor);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void LastdonationChangeDonor(Factory factory)
        {
            try
            {
                Console.WriteLine("Donor ID of donor");

                string id = Console.ReadLine();

                Console.WriteLine("new date of last donation");

                DateTime date = DateTime.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                Donor uptodate = new Donor();

                uptodate = factory.GetD.GetOneDonor(id);

                factory.ChangeDonor.ChangeDateOfLastDonation(id, date);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void NurseChangeDonor(Factory factory)
        {
            try
            {
                Console.WriteLine("Donor ID of donor");

                string id = Console.ReadLine();

                Console.WriteLine("New nurse's name");

                string nurse = Console.ReadLine();

                Donor uptodate = new Donor();

                uptodate = factory.GetD.GetOneDonor(id);

                factory.ChangeDonor.ChangeNursesName(id, nurse);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void NewBloodProduct(Factory factory)
        {
            try
            {
                Bloodproduct newProduct = new Bloodproduct();

                Console.WriteLine("Blood product ID of the new product:");

                string id = Console.ReadLine();

                Console.WriteLine("Donor: - (Secondary key to Donor table!)");

                string donorID = Console.ReadLine();

                Console.WriteLine("Expiration date");

                DateTime date = DateTime.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                Console.WriteLine("Type of the bood product ('rbc-concentrate' or 'thr-concentrate' or 'ffp-concentrate':");

                string type = Console.ReadLine();

                Console.WriteLine("Name of the target hospital:");

                string hospitalName = Console.ReadLine();

                Console.WriteLine("Location of the target hospital:");

                string hospitalLocation = Console.ReadLine();

                Console.WriteLine("Recipient - (Secondary key to Recipient table!)");

                string recipient = Console.ReadLine();

                newProduct.BloodproductId = id;
                newProduct.Donor = donorID;
                newProduct.ExpireDate = date;
                newProduct.ProductType = type;
                newProduct.TargetHospitalsName = hospitalName;
                newProduct.TargetHospitalsAddress = hospitalLocation;
                if (recipient.IsNullOrEmpty())
                {
                    newProduct.Recipient = null;
                }
                else
                {
                    newProduct.Recipient = recipient;
                }

                factory.InProduct.InsertBP(newProduct);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void RemoveBloodProduct(Factory factory)
        {
            try
            {
                Console.WriteLine("BloodPoduct ID of removable product:");

                string id = Console.ReadLine();

                Bloodproduct removableProduct = factory.GetBP.GetOneBP(id);

                factory.OutProduct.RemoveBP(removableProduct);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void RecipientChangeProduct(Factory factory)
        {
            try
            {
                Console.WriteLine("Blood Product ID of product");

                string id = Console.ReadLine();

                Console.WriteLine("Tajnumber of new recipient");

                string newTaj = Console.ReadLine();

                Bloodproduct uptodate = new Bloodproduct();

                uptodate = factory.GetBP.GetOneBP(id);

                factory.ChangeProduct.ChangeRecipient(id, newTaj);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void HospitalNameChangeProduct(Factory factory)
        {
            try
            {
                Console.WriteLine("Blood Product ID of product");

                string id = Console.ReadLine();

                Console.WriteLine("Name of the new hospital");

                string hopital = Console.ReadLine();

                Bloodproduct uptodate = new Bloodproduct();

                uptodate = factory.GetBP.GetOneBP(id);

                factory.ChangeProduct.ChangeTargetHospitalsName(id, hopital);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void HospitalAddressProduct(Factory factory)
        {
            try
            {
                Console.WriteLine("Blood Product ID of product");

                string id = Console.ReadLine();

                Console.WriteLine("Address of the new hospital");

                string hopital = Console.ReadLine();

                Bloodproduct uptodate = new Bloodproduct();

                uptodate = factory.GetBP.GetOneBP(id);

                factory.ChangeProduct.ChangeTargetHospitalsName(id, hopital);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void NewRecipient(Factory factory)
        {
            try
            {
                Recipient newRecipient = new Recipient();

                Console.WriteLine("Recipient's name:");

                string name = Console.ReadLine();

                Console.WriteLine("Recipient's gender: -  (woman or man)");

                string gender = Console.ReadLine();

                Console.WriteLine("Date of birth");

                DateTime date = DateTime.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                Console.WriteLine("Place of birth:");

                string place = Console.ReadLine();

                Console.WriteLine("Recipient's address:");

                string address = Console.ReadLine();

                Console.WriteLine("Tajnumber of the new recipient:");

                string taj = Console.ReadLine();

                newRecipient.RecipientName = name;
                newRecipient.Gender = gender;
                newRecipient.BirthDate = date;
                newRecipient.BirthPlace = place;
                newRecipient.ConstantAddress = address;
                newRecipient.Tajnumber = taj;
                factory.InOrOutRecipient.InsertR(newRecipient);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void RemoveRecipient(Factory factory)
        {
            try
            {
            Console.WriteLine("Tajnumber of removable recipient:");

            string id = Console.ReadLine();

            Recipient removableRecipient = factory.GetR.GetOneR(id);

            factory.InOrOutRecipient.RemoveR(removableRecipient);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }

        private static void AddressChangeRecipient(Factory factory)
        {
            try
            {
                Console.WriteLine("Tajnumber of recipient");

                string id = Console.ReadLine();

                Console.WriteLine("New address of the recipient");

                string address = Console.ReadLine();

                Recipient uptodate = new Recipient();

                uptodate = factory.GetR.GetOneR(id);

                factory.ChangeRecipient.ChangeConstantAddressForRecipient(id, address);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType() + "! Parameter was incorrect! Try an other one!");
                Console.ReadLine();
            }
        }
    }
}
