﻿// <copyright file="DonorCandidate.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// declaring DonorCandidate class.
    /// </summary>
    public partial class DonorCandidate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DonorCandidate"/> class.
        /// </summary>
        public DonorCandidate()
        {
            this.Donor = new HashSet<Donor>();
        }

        /// <summary>
        /// Gets or Sets - declaring CandidateName variable.
        /// </summary>
        public string CandidateName { get; set; }

        /// <summary>
        /// Gets or Sets - declaring BirthDate variable.
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Gets or Sets - declaring ConstantAddress variable.
        /// </summary>
        public string ConstantAddress { get; set; }

        /// <summary>
        /// Gets or Sets - declaring Tajnumber variable.
        /// </summary>
        public string Tajnumber { get; set; }

        /// <summary>
        /// Gets or Sets - declaring DoctorsName variable.
        /// </summary>
        public string DoctorsName { get; set; }

        /// <summary>
        /// Gets or Sets - declaring Suitability variable.
        /// </summary>
        public bool? Suitability { get; set; }

        /// <summary>
        /// Gets - declaring Donor variable.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Donor> Donor { get; }
    }
}
