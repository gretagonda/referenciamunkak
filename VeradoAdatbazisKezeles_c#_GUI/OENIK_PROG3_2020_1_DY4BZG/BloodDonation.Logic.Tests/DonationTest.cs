﻿// <copyright file="DonationTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BloodDonation.Data;
    using BloodDonation.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Testing to get one specific donor from donors.
    /// </summary>
    [TestFixture]
    public class DonationTest
    {
        private Mock<IDonorRepository> donorRepo;

        private Mock<IDonorCandidateRepository> candidateRepo;

        private Mock<IBloodProductRepository> productRepo;

        /// <summary>
        /// Testing GetAll method in recipient.
        /// </summary>
        [Test]
        public void GetOneSpecificDonor()
        {
            Mock<IDonorRepository> mockedRepo = new Mock<IDonorRepository>(MockBehavior.Loose);
            mockedRepo.Setup(repo => repo.GetOne("5555555551"));
            Donation<Donor> logic = new Donation<Donor>(mockedRepo.Object);
            Donor testdonor = logic.GetOneDonor("5555555551");
            mockedRepo.Verify(repo => repo.GetOne("5555555551"), Times.Once);
        }

        /// <summary>
        /// Testing to count how many unit blood are in the blood bank per blood type.
        /// </summary>
        [Test]
        public void BloodNumbers()
        {
            this.donorRepo = new Mock<IDonorRepository>();

            List<Donor> donors = new List<Donor>()
            {
                new Donor() { DonorId = "5555555551", Gender = "man", Tajnumber = "131313131", DateOfLastDonation = new DateTime(2020, 5, 4), NursesName = "Szabo Ilona", BloodType = "0 Rh+", LocationOfRegistration = "Szent Istvan Korhaz", AddressOfRegistration = "1097 Budapest, Nagyvarad ter 1." },
                new Donor() { DonorId = "5555555552", Gender = "woman", Tajnumber = "313131313", DateOfLastDonation = new DateTime(2020, 1, 10), NursesName = "Lakatos Laura", BloodType = "AB Rh+", LocationOfRegistration = "Hatvani Korhaz", AddressOfRegistration = "3000 Hatvan, Uzsoki utca 30." },
                new Donor() { DonorId = "5555555553", Gender = "man", Tajnumber = "141414141", DateOfLastDonation = new DateTime(2019, 11, 11), NursesName = "Fono Fiona", BloodType = "A Rh+", LocationOfRegistration = "Szent Laszlo Korhaz", AddressOfRegistration = "1097 Budapest, Albert Florian ut 5." },
            };

            Dictionary<string, string> expectedNumbers = new Dictionary<string, string>();
            expectedNumbers.Add("0 Rh+", "1");
            expectedNumbers.Add("AB Rh+", "1");
            expectedNumbers.Add("A Rh+", "1");

            this.donorRepo.Setup(repo => repo.GetAll()).Returns(donors.AsQueryable());
            Donation<Donor> logic = new Donation<Donor>(this.donorRepo.Object);
            var bloodDir = logic.GetBloodTypeNumbers();

            Assert.That(bloodDir.Count, Is.EqualTo(expectedNumbers.Count));

            this.donorRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Testing to return unused bloodproducts with product id and donor name.
        /// </summary>
        [Test]
        public void UnUsedBlood()
        {
            this.donorRepo = new Mock<IDonorRepository>();
            this.candidateRepo = new Mock<IDonorCandidateRepository>();
            this.productRepo = new Mock<IBloodProductRepository>();

            List<Donor> donors = new List<Donor>()
            {
                new Donor() { DonorId = "5555555551", Gender = "man", Tajnumber = "131313131", DateOfLastDonation = new DateTime(2020, 5, 4), NursesName = "Szabo Ilona", BloodType = "0 Rh+", LocationOfRegistration = "Szent Istvan Korhaz", AddressOfRegistration = "1097 Budapest, Nagyvarad ter 1." },
                new Donor() { DonorId = "5555555552", Gender = "woman", Tajnumber = "313131313", DateOfLastDonation = new DateTime(2020, 1, 10), NursesName = "Lakatos Laura", BloodType = "AB Rh+", LocationOfRegistration = "Hatvani Korhaz", AddressOfRegistration = "3000 Hatvan, Uzsoki utca 30." },
                new Donor() { DonorId = "5555555553", Gender = "man", Tajnumber = "141414141", DateOfLastDonation = new DateTime(2019, 11, 11), NursesName = "Fono Fiona", BloodType = "A Rh+", LocationOfRegistration = "Szent Laszlo Korhaz", AddressOfRegistration = "1097 Budapest, Albert Florian ut 5." },
            };

            List<DonorCandidate> candidates = new List<DonorCandidate>()
              {
                  new DonorCandidate() { CandidateName = "Horvath Hedvig", BirthDate = new DateTime(1990, 1, 7), ConstantAddress = "1082 Budapest, Kisfaludy utca 28.", Tajnumber = "131313131", DoctorsName = "Dr. Piros Piroska", Suitability = false },
                  new DonorCandidate() { CandidateName = "Takacs Tamara", BirthDate = new DateTime(1989, 10, 17), ConstantAddress = "1064 Budapest, Bajza utca 16.", Tajnumber = "313131313", DoctorsName = "Dr. Fekete Ferenc", Suitability = false },
                  new DonorCandidate() { CandidateName = "Farago Fanni", BirthDate = new DateTime(2001, 1, 1), ConstantAddress = "7400 Kaposvar, Pesti utca 3.", Tajnumber = "141414141", DoctorsName = "Dr. Pottyos Peter", Suitability = true },
              };

            List<Bloodproduct> bloods = new List<Bloodproduct>()
            {
                new Bloodproduct() { BloodproductId = "7777777771", Donor = "5555555551", ExpireDate = new DateTime(2020, 12, 1), ProductType = "thc-concentrate", TargetHospitalsName = "elso", TargetHospitalsAddress = "Budapest", Recipient = "125674235" },
                new Bloodproduct() { BloodproductId = "7777777772", Donor = "5555555552", ExpireDate = new DateTime(2020, 11, 2), ProductType = "rbc-concentrate", TargetHospitalsName = "masodik", TargetHospitalsAddress = "Kalocsa", Recipient = "111234658" },
                new Bloodproduct() { BloodproductId = "7777777773", Donor = "5555555553", ExpireDate = new DateTime(2020, 12, 2), ProductType = "thr-concentrate", TargetHospitalsName = "harmadik", TargetHospitalsAddress = "Veresegyhaz", Recipient = null },
            };
            Dictionary<string, string> expectedProducts = new Dictionary<string, string>();
            expectedProducts.Add("7777777773", "Horvath Hedvig");

            this.donorRepo.Setup(repo => repo.GetAll()).Returns(donors.AsQueryable());
            this.candidateRepo.Setup(repo => repo.GetAll()).Returns(candidates.AsQueryable());
            this.productRepo.Setup(repo => repo.GetAll()).Returns(bloods.AsQueryable());

            Donation<Bloodproduct> logic = new Donation<Bloodproduct>(this.productRepo.Object, this.donorRepo.Object, this.candidateRepo.Object);
            var bloodAvaible = logic.UnUsedBloodProducts();

            Assert.That(bloodAvaible.Count, Is.EqualTo(expectedProducts.Count));

            this.donorRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.candidateRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.productRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Testing to return all the donation places is Budapest.
        /// </summary>
        [Test]
        public void PlacesBudapest()
        {
            this.donorRepo = new Mock<IDonorRepository>();
            this.candidateRepo = new Mock<IDonorCandidateRepository>();

            List<Donor> donors = new List<Donor>()
            {
                new Donor() { DonorId = "5555555551", Gender = "man", Tajnumber = "131313131", DateOfLastDonation = new DateTime(2020, 5, 4), NursesName = "Szabo Ilona", BloodType = "0 Rh+", LocationOfRegistration = "Szent Istvan Korhaz", AddressOfRegistration = "1097 Budapest, Nagyvarad ter 1." },
                new Donor() { DonorId = "5555555552", Gender = "woman", Tajnumber = "313131313", DateOfLastDonation = new DateTime(2020, 1, 10), NursesName = "Lakatos Laura", BloodType = "AB Rh+", LocationOfRegistration = "Hatvani Korhaz", AddressOfRegistration = "3000 Hatvan, Uzsoki utca 30." },
                new Donor() { DonorId = "5555555553", Gender = "man", Tajnumber = "141414141", DateOfLastDonation = new DateTime(2019, 11, 11), NursesName = "Fono Fiona", BloodType = "A Rh+", LocationOfRegistration = "Szent Laszlo Korhaz", AddressOfRegistration = "1097 Budapest, Albert Florian ut 5." },
            };

            List<DonorCandidate> candidates = new List<DonorCandidate>()
              {
                  new DonorCandidate() { CandidateName = "Horvath Hedvig", BirthDate = new DateTime(1990, 1, 7), ConstantAddress = "1082 Budapest, Kisfaludy utca 28.", Tajnumber = "131313131", DoctorsName = "Dr. Piros Piroska", Suitability = false },
                  new DonorCandidate() { CandidateName = "Takacs Tamara", BirthDate = new DateTime(1989, 10, 17), ConstantAddress = "1064 Budapest, Bajza utca 16.", Tajnumber = "313131313", DoctorsName = "Dr. Fekete Ferenc", Suitability = false },
                  new DonorCandidate() { CandidateName = "Farago Fanni", BirthDate = new DateTime(2001, 1, 1), ConstantAddress = "7400 Kaposvar, Pesti utca 3.", Tajnumber = "141414141", DoctorsName = "Dr. Pottyos Peter", Suitability = true },
              };

            Dictionary<string, string> expectedP = new Dictionary<string, string>();
            expectedP.Add("Horvath Hedvig", "1097 Budapest, Nagyvarad ter 1.");
            expectedP.Add("Farago Fanni", "1097 Budapest, Albert Florian ut 5.");

            this.donorRepo.Setup(repo => repo.GetAll()).Returns(donors.AsQueryable());
            this.candidateRepo.Setup(repo => repo.GetAll()).Returns(candidates.AsQueryable());

            Donation<Donor> logic = new Donation<Donor>(this.donorRepo.Object, this.candidateRepo.Object);
            var placesB = logic.DonationPlacesInBudapest();

            Assert.That(placesB.Count, Is.EqualTo(expectedP.Count));
            this.donorRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.candidateRepo.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
