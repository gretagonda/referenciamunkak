﻿// <copyright file="TransfusionTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BloodDonation.Data;
    using BloodDonation.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Creatin constructor to test Transfusion Logic methods.
    /// </summary>
    [TestFixture]
    public class TransfusionTest
    {
        /// <summary>
        /// Testing GetAll method in recipient.
        /// </summary>
        [Test]
        public void GetAllRecipients()
        {
            Mock<IRecipientRepository> mockedRepo = new Mock<IRecipientRepository>(MockBehavior.Loose);
            List<Recipient> recipients = new List<Recipient>()
            {
                new Recipient() { RecipientName = "Oláh Ottó", Gender = "man", BirthDate = new DateTime(1978, 3, 7), BirthPlace = "Gyor", ConstantAddress = "2900 Komárom, Vezér utca 8.", Tajnumber = "064785468" },
                new Recipient() { RecipientName = "Varga János", Gender = "man", BirthDate = new DateTime(1999, 11, 11), BirthPlace = "Kalocsa", ConstantAddress = "440 Nyiregyhaza, Katona utca 6.", Tajnumber = "076412853" },
                new Recipient() { RecipientName = "Futó Márton", Gender = "man", BirthDate = new DateTime(1996, 6, 3), BirthPlace = "Siofok", ConstantAddress = "1045 Budapest, Kossuth Lajos utca 19.", Tajnumber = "111476588" },
                new Recipient() { RecipientName = "Csillag Franciska", Gender = "woman", BirthDate = new DateTime(2000, 4, 1), BirthPlace = "Budapest", ConstantAddress = "1084 Budapest, Práter utca 7.", Tajnumber = "145624873" },
            };

            List<Recipient> expectedR = new List<Recipient>() { recipients[0], recipients[1], recipients[2], recipients[3] };
            mockedRepo.Setup(repo => repo.GetAll()).Returns(recipients.AsQueryable());
            Transfusion<Recipient> logic = new Transfusion<Recipient>(mockedRepo.Object);
            var result = logic.GetAllR();
            Assert.That(result.Count, Is.EqualTo(expectedR.Count));
            Assert.That(result, Is.EquivalentTo(expectedR));
            mockedRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Testing the recipient change method on bloodproduct.
        /// </summary>
        [Test]
        public void TestChangeRecipientOnBloodProduct()
        {
            Mock<IBloodProductRepository> mockedRepo = new Mock<IBloodProductRepository>(MockBehavior.Loose);
            mockedRepo.Setup(repo => repo.ChangeRecipient("7777777771", "212122222"));
            Transfusion<Bloodproduct> logic = new Transfusion<Bloodproduct>(mockedRepo.Object);
            logic.ChangeRecipient("7777777771", "212122222");
            mockedRepo.Verify(repo => repo.ChangeRecipient("7777777771", "212122222"), Times.Once);
        }
    }
}
