﻿// <copyright file="ApplicationTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
[assembly: System.CLSCompliant(false)]

namespace BloodDonation.Logic.Tests
{
    using System;
    using BloodDonation.Data;
    using BloodDonation.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// declare ApplicationTest class.
    /// </summary>
    [TestFixture]
    public class ApplicationTest
    {
        /// <summary>
        /// Testing to insert a new donor candidate to candidate table.
        /// </summary>
        [Test]
        public void TestInsertCandidate()
        {
            Mock<IDonorCandidateRepository> mockedRepo = new Mock<IDonorCandidateRepository>(MockBehavior.Loose);
            mockedRepo.Setup(repo => repo.Insert(It.IsAny<DonorCandidate>()));

            DonorCandidate testC = new DonorCandidate();
            testC.CandidateName = "Boros Borbala";
            testC.BirthDate = new DateTime(1999, 1, 15);
            testC.ConstantAddress = "1145 Budapest, Tisza Istvan ter 5.";
            testC.Tajnumber = "131313131";
            testC.DoctorsName = "Dr. Lukacs Lajos";
            testC.Suitability = false;

            Application<DonorCandidate> logic = new Application<DonorCandidate>(mockedRepo.Object);
            logic.InsertDC(testC);
            mockedRepo.Verify(repo => repo.Insert(testC), Times.Once);
            mockedRepo.Verify(repo => repo.Insert(It.IsAny<DonorCandidate>()), Times.Once);
        }

        /// <summary>
        /// Testing to remove a donor candidate from candidate table.
        /// </summary>
        [Test]
        public void TestRemoveCandidate()
        {
            Mock<IDonorCandidateRepository> mockedRepo = new Mock<IDonorCandidateRepository>(MockBehavior.Loose);
            mockedRepo.Setup(repo => repo.Remove(It.IsAny<DonorCandidate>()));

            DonorCandidate testC = new DonorCandidate();
            testC.CandidateName = "Boros Borbala";
            testC.BirthDate = new DateTime(1999, 1, 15);
            testC.ConstantAddress = "1145 Budapest, Tisza Istvan ter 5.";
            testC.Tajnumber = "131313131";
            testC.DoctorsName = "Dr. Lukacs Lajos";
            testC.Suitability = false;

            Application<DonorCandidate> logic = new Application<DonorCandidate>(mockedRepo.Object);
            logic.RemoveDC(testC);
            mockedRepo.Verify(repo => repo.Remove(testC), Times.Once);
            mockedRepo.Verify(repo => repo.Remove(It.IsAny<DonorCandidate>()), Times.Once);
        }
    }
}
