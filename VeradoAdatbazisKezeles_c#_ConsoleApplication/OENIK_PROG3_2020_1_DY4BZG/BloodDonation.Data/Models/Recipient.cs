﻿// <copyright file="Recipient.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// declaring Recipient class.
    /// </summary>
    public partial class Recipient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Recipient"/> class.
        /// </summary>
        public Recipient()
        {
            this.Bloodproduct = new HashSet<Bloodproduct>();
        }

        /// <summary>
        /// Gets or Sets - declaring RecipientName variable.
        /// </summary>
        public string RecipientName { get; set; }

        /// <summary>
        /// Gets or Sets - declaring Gender variable.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or Sets - declaring BirthDate variable.
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Gets or Sets - declaring BirthPlace variable.
        /// </summary>
        public string BirthPlace { get; set; }

        /// <summary>
        /// Gets or Sets - declaring ConstantAddress variable.
        /// </summary>
        public string ConstantAddress { get; set; }

        /// <summary>
        /// Gets or Sets - declaring Tajnumber variable.
        /// </summary>
        public string Tajnumber { get; set; }

        /// <summary>
        /// Gets - declaring Bloodproduct variable.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Bloodproduct> Bloodproduct { get; }
    }
}
