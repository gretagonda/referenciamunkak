﻿// <copyright file="Donor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// declaring Donor class.
    /// </summary>
    public partial class Donor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Donor"/> class.
        /// </summary>
        public Donor()
        {
            this.Bloodproduct = new HashSet<Bloodproduct>();
        }

        /// <summary>
        ///  Gets or Sets - declaring DonorId variable.
        /// </summary>
        public string DonorId { get; set; }

        /// <summary>
        ///  Gets or Sets - declaring Gender variable.
        /// </summary>
        public string Gender { get; set; }

#nullable enable
        /// <summary>
        ///  Gets or Sets - declaring Tajnumber variable.
        /// </summary>
        public string? Tajnumber { get; set; }

#nullable disable
        /// <summary>
        ///  Gets or Sets - declaring DateOfLatDonation variable.
        /// </summary>
        public DateTime? DateOfLastDonation { get; set; }

        /// <summary>
        ///  Gets or Sets - declaring NursesName variable.
        /// </summary>
        public string NursesName { get; set; }

        /// <summary>
        ///  Gets or Sets - declaring BloodType variable.
        /// </summary>
        public string BloodType { get; set; }

        /// <summary>
        ///  Gets or Sets - declaring LocationOfRegistration variable.
        /// </summary>
        public string LocationOfRegistration { get; set; }

        /// <summary>
        /// Gets or Sets - declaring AddressOfRegistration variable.
        /// </summary>
        public string AddressOfRegistration { get; set; }

        /// <summary>
        /// Gets or Sets - declaring TajnumberNavigation variable.
        /// </summary>
        [NotMapped]
        public virtual DonorCandidate TajnumberNavigation { get; set; }

        /// <summary>
        /// Gets - declaring Bloodproduct variable.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Bloodproduct> Bloodproduct { get; }
    }
}
