﻿// <copyright file="BloodDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace BloodDonation.Data
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;

    /// <summary>
    /// Creating DbContext class.
    /// </summary>
    public partial class BloodDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BloodDbContext"/> class.
        /// </summary>
        public BloodDbContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BloodDbContext"/> class.
        /// </summary>
        /// <param name="options">given parameter.</param>
        public BloodDbContext(DbContextOptions<BloodDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or Sets Bloodproduct.
        /// </summary>
        public virtual DbSet<Bloodproduct> Bloodproduct { get; set; }

        /// <summary>
        /// Gets or Sets Donor.
        /// </summary>
        public virtual DbSet<Donor> Donor { get; set; }

        /// <summary>
        /// Gets or Sets DonorCandidate.
        /// </summary>
        public virtual DbSet<DonorCandidate> DonorCandidate { get; set; }

        /// <summary>
        /// Gets or Sets Recipient.
        /// </summary>
        public virtual DbSet<Recipient> Recipient { get; set; }

        /// <summary>
        /// initiating OnConfiguring method.
        /// </summary>
        /// <param name="optionsBuilder">given parameter.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           if (optionsBuilder == null)
            {
                string ex = "optionsBuilder";
                throw new ArgumentNullException(ex);
            }

           if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies().UseSqlServer("Data Source=(LocalDB)\\MSSQLLocalDB; AttachDbFilename = |DataDirectory|\\BloodDonationDatabase.mdf; Integrated Security = True");
            }
        }

        /// <summary>
        /// declaring OnModelCreating method.
        /// </summary>
        /// <param name="modelBuilder">given parameter.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                string ex = "modelBuilder";
                throw new ArgumentNullException(ex);
            }

            modelBuilder.Entity<Bloodproduct>(entity =>
            {
                entity.Property(e => e.BloodproductId)
                    .HasColumnName("Bloodproduct_ID")
                    .HasColumnType("varchar(10, 0)");

                entity.Property(e => e.Donor).HasColumnType("varchar(10, 0)");

                entity.Property(e => e.ExpireDate)
                    .HasColumnName("Expire_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProductType)
                    .HasColumnName("Product_Type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Recipient).HasColumnType("varchar(9, 0)");

                entity.Property(e => e.TargetHospitalsAddress)
                    .HasColumnName("Target_Hospitals_Address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TargetHospitalsName)
                    .HasColumnName("Target_Hospitals_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.DonorNavigation)
                    .WithMany(p => p.Bloodproduct)
                    .HasForeignKey(d => d.Donor)
                    .HasConstraintName("Bloodproduct_fk1");

                entity.HasOne(d => d.RecipientNavigation)
                    .WithMany(p => p.Bloodproduct)
                    .HasForeignKey(d => d.Recipient)
                    .HasConstraintName("Bloodproduct_fk3");
            });

            modelBuilder.Entity<Donor>(entity =>
            {
                entity.Property(e => e.DonorId)
                    .HasColumnName("Donor_ID")
                    .HasColumnType("varchar(10, 0)");

                entity.Property(e => e.AddressOfRegistration)
                    .HasColumnName("Address_of_registration")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BloodType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DateOfLastDonation)
                    .HasColumnName("Date_of_last_donation")
                    .HasColumnType("datetime");

                entity.Property(e => e.Gender)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocationOfRegistration)
                    .HasColumnName("Location_of_registration")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NursesName)
                    .HasColumnName("Nurses_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tajnumber).HasColumnType("varchar(9, 0)");

                entity.HasOne(d => d.TajnumberNavigation)
                    .WithMany(p => p.Donor)
                    .HasForeignKey(d => d.Tajnumber)
                    .HasConstraintName("donor_fk1");
            });

            modelBuilder.Entity<DonorCandidate>(entity =>
            {
                entity.HasKey(e => e.Tajnumber)
                    .HasName("donorcandidate_pk");

                entity.ToTable("Donor_candidate");

                entity.Property(e => e.Tajnumber).HasColumnType("varchar(9, 0)");

                entity.Property(e => e.BirthDate)
                    .HasColumnName("Birth_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CandidateName)
                    .HasColumnName("Candidate_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ConstantAddress)
                    .HasColumnName("Constant_address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DoctorsName)
                    .HasColumnName("Doctors_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Suitability).HasColumnType("varchar(1, 0)");
            });

            modelBuilder.Entity<Recipient>(entity =>
            {
                entity.HasKey(e => e.Tajnumber)
                    .HasName("recipient_pk");

                entity.Property(e => e.Tajnumber).HasColumnType("varchar(9, 0)");

                entity.Property(e => e.BirthDate)
                    .HasColumnName("Birth_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.BirthPlace)
                    .HasColumnName("Birth_place")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConstantAddress)
                    .HasColumnName("Constant_address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientName)
                    .HasColumnName("Recipient_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            this.OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}