﻿// <copyright file="Bloodproduct.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Data
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// declaring Bloodproduct class.
    /// </summary>
    public partial class Bloodproduct
    {
        /// <summary>
        /// Gets or Sets - declaring BloodproductId variable.
        /// </summary>
        public string BloodproductId { get; set; }

#nullable enable
        /// <summary>
        ///  Gets or Sets - declaring Donor variable.
        /// </summary>
        public string? Donor { get; set; }

#nullable disable

#nullable enable
        /// <summary>
        ///  Gets or Sets - declaring ExpireDate variable.
        /// </summary>
        public DateTime? ExpireDate { get; set; }

#nullable disable

        /// <summary>
        ///  Gets or Sets - declaring ProductType variable.
        /// </summary>
        public string ProductType { get; set; }

        /// <summary>
        ///  Gets or Sets - declaring TargetHospitalsName variable.
        /// </summary>
        public string TargetHospitalsName { get; set; }

        /// <summary>
        ///  Gets or Sets - declaring TargetHospitalsAddress variable.
        /// </summary>
        public string TargetHospitalsAddress { get; set; }

#nullable enable
        /// <summary>
        ///  Gets or Sets - declaring Recipient variable.
        /// </summary>
        public string? Recipient { get; set; }

#nullable disable

        /// <summary>
        ///  Gets or Sets - declaring DonorNavigation variable.
        /// </summary>
        [NotMapped]
        public virtual Donor DonorNavigation { get; set; }

        /// <summary>
        ///  Gets or Sets - declaring RecipientNavigation variable.
        /// </summary>
        [NotMapped]
        public virtual Recipient RecipientNavigation { get; set; }
    }
}
