﻿
CREATE TABLE Donor_candidate (
Candidate_Name varchar(50),
Birth_date datetime,
Constant_address varchar(50),
Tajnumber varchar(9),
Doctors_name varchar(50),
Suitability varchar(1),
CONSTRAINT donorcandidate_pk PRIMARY KEY(Tajnumber),
CONSTRAINT donorcandidate_ck1 CHECK (Suitability=0 or Suitability=1)
);

CREATE TABLE Donor(
Donor_ID varchar(10),
Gender varchar(50),
Tajnumber varchar(9),
Date_of_last_donation datetime,
Nurses_name varchar(50),
BloodType varchar(10),
Location_of_registration varchar(50),
Address_of_registration varchar(50),
CONSTRAINT donor_pk PRIMARY KEY(Donor_ID),
CONSTRAINT donor_fk1 FOREIGN KEY(Tajnumber) REFERENCES Donor_candidate(Tajnumber),
CONSTRAINT donor_ck1 CHECK (BloodType='0 Rh-' or BloodType='0 Rh+' or BloodType='A Rh-' or BloodType='A Rh+' or BloodType='B Rh-' or BloodType='B Rh+' or BloodType='AB Rh-' or BloodType='AB Rh+')
);

CREATE TABLE Recipient(
Recipient_Name varchar(50),
Gender varchar(10),
Birth_date datetime,
Birth_place varchar(30),
Constant_address varchar(50),
Tajnumber varchar(9),
CONSTRAINT recipient_pk PRIMARY KEY(Tajnumber)
);

CREATE TABLE Bloodproduct(
Bloodproduct_ID varchar(10),
Donor varchar(10),
Expire_date datetime,
Product_Type varchar(20),
Target_Hospitals_Name  varchar(50),
Target_Hospitals_Address varchar(50),
Recipient varchar(9),
CONSTRAINT Bloodproduct_pk PRIMARY KEY(Bloodproduct_ID),
CONSTRAINT Bloodproduct_fk1 FOREIGN KEY(Donor) REFERENCES Donor(Donor_ID),
CONSTRAINT Bloodproduct_fk3 FOREIGN KEY(Recipient) REFERENCES Recipient(Tajnumber),
CONSTRAINT Bloodproduct_ck CHECK (Product_Type='rbc-concentrate' or Product_Type='thr-concentrate' or Product_Type='ffp-concentrate')
);





INSERT INTO Donor_candidate
VALUES('Kovács Kálmán', '1996-08-25','1146 Budapest, Thököly út 50.', '025678943','Dr. Bihari László',1);
INSERT INTO Donor_candidate
VALUES('Kiss Katalin', '1988-08-10','1144 Budapest, Csongor út 6.', '025623893','Dr. Bihari László',1);
INSERT INTO Donor_candidate
VALUES('Horváth Gabriella','1996-08-25','1186 Budapest, Csengeri utca 20.', '135678943','Dr. Bihari László',1);
INSERT INTO Donor_candidate
VALUES('Vajda Virgil', '1954-04-25','1123 Budapest, Bécsi út 14.', '027628943','Dr. Fazakas János',1);
INSERT INTO Donor_candidate
VALUES('Losonci Lajos', '1996-08-25','1057 Budapest, Szondi utca 11.,', '025678587','Dr. Fazakas János',1);
INSERT INTO Donor_candidate
VALUES('Török Imre', '1945-06-02','1675 Budapest, Kölcsey Ferenc utca 10.', '025615793','Dr. Illés Sándor',1);
INSERT INTO Donor_candidate
VALUES('Arany Virág', '2000-01-10','1024 Budapest, Ajtósi Dürer sor 7.', '025186323','Dr. Illés Sándor',1);
INSERT INTO Donor_candidate
VALUES('Szoros Sándor', '1930-05-25','1651 Budapest, Bácskai utca 4.', '025674986','Dr. Illés Sándor',1);
INSERT INTO Donor_candidate
VALUES('Fejes Antónia', '1991-01-20','3600 Ózd, Petőfi utca 5.', '186475282','Dr. Lénárd Zsuzsa',1);
INSERT INTO Donor_candidate
VALUES('Nemes Erik', '1990-11-11','3000 Hatvan, Ballasi Bálint út 7.', '118964724','Dr. Lénárd Zsuzsa',1);
INSERT INTO Donor_candidate
VALUES('Ferences Kázmér','1998-04-06','3980 Sátoraljaújhely, Mártírok útja 71.', '125479534','Dr. Lénárd Zsuzsa',0);
INSERT INTO Donor_candidate
VALUES('Szabó Szilvia', '1988-01-30','2900 Komárom, Széchenyi István utca 2.', '047794461','Dr. Lénárd Zsuzsa',0);
INSERT INTO Donor_candidate
VALUES('Kedves Eszter', '1999-12-22','7400 Kaposvár, Tallián Gyula utca 18.', '078923647','Dr. Máthé Zsolt',0);
INSERT INTO Donor_candidate
VALUES('Tudor Franciska', '1991-10-28','9400 Sopron, Győri utca 20.', '114643156','Dr. Piros László',0);
INSERT INTO Donor_candidate
VALUES('Tóth Tamás', '1975-12-30','4400 Nyíregyháza, Szent István utca 68.', '157546475','Dr. Piros László',0);

INSERT INTO Donor
VALUES('5555555551','man','025678943', '2020-10-30','Nagy Valéria', '0 Rh+', 'Elméleti Orvostudományi központ','1094 Budapest, Tűzoltó u. 37' );
INSERT INTO Donor
VALUES('5555555552','woman','025623893', '2020-10-10','Bunford Mária', 'B Rh+','Heim Pál Kórház','1089 Budapest, Üllői út 86' );
INSERT INTO Donor
VALUES('5555555553','woman','135678943', '2020-10-15','Bunford Mária', 'AB Rh+','Heim Pál Kórház','1089 Budapest, Üllői út 86' );
INSERT INTO Donor
VALUES('5555555554','man','027628943', '2020-10-30','Nagy Valéria', '0 Rh-','Elméleti Orvostudományi központ','1094 Budapest, Tűzoltó u. 37');
INSERT INTO Donor
VALUES('5555555555','man','025678587', '2020-10-22','Toth Krisztina', 'A Rh+','Délpesti Területi Vérellátó Intézet','1204 Budapest, Köves út. 2-4');
INSERT INTO Donor
VALUES('5555555556','man','025615793', '2020-10-17','Nagy Valéria', 'A Rh-','Elméleti Orvostudományi központ','1094 Budapest, Tűzoltó u. 37');
INSERT INTO Donor
VALUES('5555555557','woman','025186323', '2020-10-29','Toth Krisztina', '0 Rh+','Délpesti Területi Vérellátó Intézet','1204 Budapest, Köves út. 2-4');
INSERT INTO Donor
VALUES('5555555558','man','025674986', '2020-10-01','Orosz Ildiko', 'AB Rh-','Salgótarjáni Területi Vérellátó Intézet','3100 Salgótarján, Füleki út 54.');
INSERT INTO Donor
VALUES('5555555559','woman','186475282', '2020-10-16','Orosz Ildiko', 'B Rh+' ,'Salgótarjáni Területi Vérellátó Intézet','3100 Salgótarján, Füleki út 54.');
INSERT INTO Donor
VALUES('5555555560','man','118964724', '2020-10-02','Nagy Valéria', '0 Rh+', 'Kincsesbánya-Ifjúsági Klub','8044 Kincsesbánya, Kincses u. 39.');

INSERT INTO Recipient
VALUES('Heves Hedvig','woman','1990-09-02','Nyíregyháza','4400 Nyíregyháza, Virág utca 10.','125476547');
INSERT INTO Recipient
VALUES('Soros Sára','woman','1985-01-12','Kaposvár','7400 Kaposvár, Ady Endre utca 25.','145678954');
INSERT INTO Recipient
VALUES('Oláh Ottó','man','1978-03-05','Győr','2900 Komárom, Vezér utca 8.','064785468');
INSERT INTO Recipient
VALUES('Varja János','man','1964-09-25','Budapest','3000 Hatvan, Árpád utca 40','076412853');
INSERT INTO Recipient
VALUES('Kerepes Csenge','woman','1990-09-02','Debrecen','4400 Nyíregyháza, Katona utca 6.','114835642');
INSERT INTO Recipient
VALUES('Futó Márton','man','1990-09-02','Budapest','1144 Budapest, Szentendrei út 6.','111479655');
INSERT INTO Recipient
VALUES('Báthori Eszter','woman','1990-09-02','Kecskemét','1045 Budapest, Kossuth Lajos utca 35.','116648234');
INSERT INTO Recipient
VALUES('Neves Noémi','woman','1990-09-02','Budapest','1184 Budapest, Kisfaludy utca 28.','122468953');
INSERT INTO Recipient
VALUES('Szentesi Róbert','man','1990-09-02','Salgótarján','1167 Budapest, Dózsa György utca 62.','154325448');
INSERT INTO Recipient
VALUES('Csillag Ferenc','man','1990-09-02','Budapest','1304 Budapest, Tímár utca 60.','111496281');



INSERT INTO Bloodproduct
VALUES('7777777771','5555555551', '2020-11-02','rbc-concentrate', 'Almási Balogh Pál Kórház','3600 Ózd, Béke u. 1.',null);
INSERT INTO Bloodproduct
VALUES('7777777772','5555555551', '2020-11-02','thr-concentrate', 'Csolnoky Ferenc Kórház','8200 Veszprém, Kórház u. 1.',null);
INSERT INTO Bloodproduct
VALUES('7777777773','5555555551', '2020-11-02','ffp-concentrate', 'Csolnoky Ferenc Kórház','8200 Veszprém, Kórház u. 1.','125476547');
INSERT INTO Bloodproduct
VALUES('7777777774','5555555552', '2020-11-02','rbc-concentrate', 'Jászberényi Szent Erzsébet Kórház','5100 Jászberény, Szelei út 2.',null);
INSERT INTO Bloodproduct
VALUES('7777777884','5555555552', '2020-11-02','thr-concentrate', 'Jászberényi Szent Erzsébet Kórház','5100 Jászberény, Szelei út 2.','154325448');
INSERT INTO Bloodproduct
VALUES('7777778884','5555555552', '2020-11-02','ffp-concentrate', 'Jászberényi Szent Erzsébet Kórház','5100 Jászberény, Szelei út 2.','154325448');
INSERT INTO Bloodproduct
VALUES('7777777775','5555555553', '2020-12-02','rbc-concentrate', 'Uzsoki utcai Kórház','1145 Budapest, Uzsoki utca 29-41.','111496281');
INSERT INTO Bloodproduct
VALUES('7777777776','5555555553', '2020-12-02','thr-concentrate', 'Uzsoki utcai Kórház','1145 Budapest, Uzsoki utca 29-41.','111496281');
INSERT INTO Bloodproduct
VALUES('7777777777','5555555553', '2020-12-02','ffp-concentrate', 'Uzsoki utcai Kórház','1145 Budapest, Uzsoki utca 29-41.',null);
INSERT INTO Bloodproduct
VALUES('7777777780','5555555554', '2020-12-02','rbc-concentrate', 'Dr. Kenessey Albert Kórház-Rendelőintézet','2660 Balassagyarmat, Rákóczi u. 125-127.',null);
INSERT INTO Bloodproduct
VALUES('7777777781','5555555554', '2020-12-02','thr-concentrate', 'Dr. Kenessey Albert Kórház-Rendelőintézet','2660 Balassagyarmat, Rákóczi u. 125-127.','111479655');
INSERT INTO Bloodproduct
VALUES('7777777782','5555555554', '2020-12-02','ffp-concentrate', 'Jahn Ferenc Dél-Pesti Kórház és Rendelőintézet','1204 Budapest, Köves u. 1.','111479655');
INSERT INTO Bloodproduct
VALUES('7777777783','5555555555', '2020-12-02','rbc-concentrate', 'Dr. Kenessey Albert Kórház-Rendelőintézet','2660 Balassagyarmat, Rákóczi u. 125-127.',null);
INSERT INTO Bloodproduct
VALUES('7777771884','5555555555', '2020-12-02','thr-concentrate', 'Jahn Ferenc Dél-Pesti Kórház és Rendelőintézet','1204 Budapest, Köves u. 1.',null);
INSERT INTO Bloodproduct
VALUES('7777777785','5555555555', '2020-12-02', 'ffp-concentrate', 'Jahn Ferenc Dél-Pesti Kórház és Rendelőintézet','1204 Budapest, Köves u. 1.',null);
INSERT INTO Bloodproduct
VALUES('7777777786','5555555556', '2020-12-02','rbc-concentrate', 'Jahn Ferenc Dél-Pesti Kórház és Rendelőintézet','1204 Budapest, Köves u. 1.','145678954');
INSERT INTO Bloodproduct
VALUES('7777777787','5555555556', '2020-12-02','thr-concentrate', 'Csolnoky Ferenc Kórház','8200 Veszprém, Kórház u. 1.','145678954');
INSERT INTO Bloodproduct
VALUES('7777777788','5555555556', '2020-12-02','ffp-concentrate', 'Csolnoky Ferenc Kórház','8200 Veszprém, Kórház u. 1.','145678954');
INSERT INTO Bloodproduct
VALUES('7777777789','5555555557', '2020-12-02','rbc-concentrate', 'Csolnoky Ferenc Kórház','8200 Veszprém, Kórház u. 1.','064785468');
INSERT INTO Bloodproduct
VALUES('7777777790','5555555557', '2020-12-02','thr-concentrate', 'Csolnoky Ferenc Kórház','8200 Veszprém, Kórház u. 1.','064785468');
INSERT INTO Bloodproduct
VALUES('7777777791','5555555557', '2020-12-02','ffp-concentrate', 'Károlyi Sándor Kórház','1041 Budapest, Nyár u. 103.','064785468');
INSERT INTO Bloodproduct
VALUES('7777777792','5555555558', '2020-01-02','rbc-concentrate', 'Károlyi Sándor Kórház','1041 Budapest, Nyár u. 103.',null);
INSERT INTO Bloodproduct
VALUES('7777777992','5555555558', '2020-01-02','thr-concentrate', 'Jahn Ferenc Dél-Pesti Kórház és Rendelőintézet','1204 Budapest, Köves u. 1.',null);
INSERT INTO Bloodproduct
VALUES('7777779992','5555555558', '2020-01-02','ffp-concentrate', 'Uzsoki utcai Kórház','1145 Budapest, Uzsoki utca 29-41.',null);
INSERT INTO Bloodproduct
VALUES('7777777793','5555555559', '2020-01-02','rbc-concentrate', 'Uzsoki utcai Kórház','1145 Budapest, Uzsoki utca 29-41.','076412853');
INSERT INTO Bloodproduct
VALUES('7777777794','5555555559', '2020-01-02','thr-concentrate', 'Károlyi Sándor Kórház','1041 Budapest, Nyár u. 103.',null);
INSERT INTO Bloodproduct
VALUES('7777777795','5555555559', '2020-01-02','ffp-concentrate', 'Károlyi Sándor Kórház','1041 Budapest, Nyár u. 103.','076412853');
INSERT INTO Bloodproduct
VALUES('7777777796','5555555560', '2020-01-02','rbc-concentrate', 'Károlyi Sándor Kórház','1041 Budapest, Nyár u. 103.','114835642');
INSERT INTO Bloodproduct
VALUES('7777777797','5555555560', '2020-01-02','thr-concentrate', 'Károlyi Sándor Kórház','1041 Budapest, Nyár u. 103.','114835642');
INSERT INTO Bloodproduct
VALUES('7777777798','5555555560', '2020-01-02','ffp-concentrate', 'Uzsoki utcai Kórház','1145 Budapest, Uzsoki utca 29-41.',null);

