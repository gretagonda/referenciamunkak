﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1304:Specify CultureInfo", Justification = "string given locally, no user settings", Scope = "member", Target = "~M:BloodDonation.Logic.Donation`1.DonationPlacesInBudapest~System.Collections.Generic.Dictionary{System.String,System.String}")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "string given locally, no user settings", Scope = "member", Target = "~M:BloodDonation.Logic.Donation`1.DonationPlacesInBudapest~System.Collections.Generic.Dictionary{System.String,System.String}")]
[assembly: SuppressMessage("Globalization", "CA1304:Specify CultureInfo", Justification = "string given locally, no user settings", Scope = "member", Target = "~M:BloodDonation.Logic.Donation`1.DonationPlacesInBudapestAsync~System.Threading.Tasks.Task{System.Collections.Generic.Dictionary{System.String,System.String}}")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "string given locally, no user settings", Scope = "member", Target = "~M:BloodDonation.Logic.Donation`1.DonationPlacesInBudapestAsync~System.Threading.Tasks.Task{System.Collections.Generic.Dictionary{System.String,System.String}}")]
