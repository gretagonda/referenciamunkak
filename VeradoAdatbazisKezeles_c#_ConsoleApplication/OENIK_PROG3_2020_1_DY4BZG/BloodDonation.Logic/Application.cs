﻿// <copyright file="Application.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
[assembly: System.CLSCompliant(false)]

namespace BloodDonation.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using BloodDonation.Data;
    using BloodDonation.Repository;

    /// <summary>
    /// declares Application class.
    /// </summary>
    /// <typeparam name="T">generic parameter.</typeparam>
    public class Application<T> : IApplication<T>
        where T : class
    {
        private IDonorCandidateRepository donorCandidateRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Application{T}"/> class.
        /// </summary>
        /// <param name="donorCandidateRepository">IDonorCandidateRepository parameter.</param>
        public Application(IDonorCandidateRepository donorCandidateRepository)
        {
            this.donorCandidateRepository = donorCandidateRepository;
        }

        /// <summary>
        /// implements ChangeConstantAddressForCandidate method.
        /// </summary>
        /// <param name="id">candidate id as string.</param>
        /// <param name="newAddress">new address as string.</param>
        /// <returns>bool value..</returns>
        public bool ChangeConstantAddressForCandidate(string id, string newAddress)
        {
            this.donorCandidateRepository.ChangeConstantAddress(id, newAddress);
            return true;
        }

        /// <summary>
        /// implements ChangeSuitability methid.
        /// </summary>
        /// <param name="id">candidate id as string.</param>
        /// <param name="suitablility">suitability as bool.</param>
        public void ChangeSuitability(string id, bool suitablility)
        {
            this.donorCandidateRepository.ChangeSuitability(id, suitablility);
        }

        /// <summary>
        /// implements GetAllDC method.
        /// </summary>
        /// <returns>returns IList with all DonorCandidates.</returns>
        public IList<DonorCandidate> GetAllDC()
        {
            return this.donorCandidateRepository.GetAll().ToList();
        }

        /// <summary>
        /// implements GetOneDC method.
        /// </summary>
        /// <param name="id">candidate is as string.</param>
        /// <returns>returns one specific DonorCandidate.</returns>
        public DonorCandidate GetOneDC(string id)
        {
            return this.donorCandidateRepository.GetOne(id);
        }

        /// <summary>
        /// implements InsertDC method.
        /// </summary>
        /// <param name="entity">DonorCandidate object.</param>
        public void InsertDC(DonorCandidate entity)
        {
            this.donorCandidateRepository.Insert(entity);
        }

        /// <summary>
        /// implements RemoveDC method.
        /// </summary>
        /// <param name="entity">removable DonorCandidate object.</param>
        /// <returns>return bool value.</returns>
        public bool RemoveDC(DonorCandidate entity)
        {
            return this.donorCandidateRepository.Remove(entity);
        }
    }
}
