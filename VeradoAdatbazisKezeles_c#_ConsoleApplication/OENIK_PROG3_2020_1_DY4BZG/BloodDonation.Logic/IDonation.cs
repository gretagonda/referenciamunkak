﻿// <copyright file="IDonation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Logic
{
    using System;
    using System.Collections.Generic;
    using BloodDonation.Data;

    /// <summary>
    /// declares IDonation interface.
    /// </summary>
    /// <typeparam name="T">generic parameter.</typeparam>
    public interface IDonation<T>
        where T : class
    {
        /// <summary>
        /// declares ChangeDateOfLastDonation method for IDonation.
        /// </summary>
        /// <param name="donorID">donorID as string parameter.</param>
        /// <param name="lastdonation">date of last donation as DateTime parameter.</param>
        void ChangeDateOfLastDonation(string donorID, DateTime lastdonation);

        /// <summary>
        /// declares ChangeNursesName method for IDonation.
        /// </summary>
        /// <param name="donorID">donorID as string parameter.</param>
        /// <param name="nursesname">name of the nurse as string.</param>
        void ChangeNursesName(string donorID, string nursesname);

        /// <summary>
        /// declares InsertDonor method for IDonation.
        /// </summary>
        /// <param name="entity">specific Donor.</param>
        void InsertDonor(Donor entity);

        /// <summary>
        /// declares InsertBP method for IDonation.
        /// </summary>
        /// <param name="entity">specific blood product.</param>
        void InsertBP(Bloodproduct entity);

        /// <summary>
        /// declares RemoveDonor method for IDonation.
        /// </summary>
        /// <param name="entity">specific Donor.</param>
        /// <returns>returns bool value.</returns>
        bool RemoveDonor(Donor entity);

        /// <summary>
        /// declares GetOneDonor method for IDonation.
        /// </summary>
        /// <param name="id">donor ID as string parameter.</param>
        /// <returns>returns one specific Donor.</returns>
        Donor GetOneDonor(string id);

        /// <summary>
        /// declares GetAllDonor method for IDonation.
        /// </summary>
        /// <returns>returns all Donors.</returns>
        IList<Donor> GetAllDonor();
    }
}
