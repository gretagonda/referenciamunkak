﻿// <copyright file="IRecipientRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Repository
{
    using BloodDonation.Data;

    /// <summary>
    /// declaring IRecipientRepository interface.
    /// </summary>
    public interface IRecipientRepository : IRepository<Recipient>
    {
        /// <summary>
        /// declaring ChangeConstantAddress method.
        /// </summary>
        /// <param name="taj">string parameter contains tajnumber.</param>
        /// <param name="newAddress">string parameter contains address.</param>
        void ChangeConstantAddress(string taj, string newAddress);
    }
}
