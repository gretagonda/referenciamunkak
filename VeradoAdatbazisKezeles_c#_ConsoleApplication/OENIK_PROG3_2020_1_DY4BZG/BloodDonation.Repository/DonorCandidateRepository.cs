﻿// <copyright file="DonorCandidateRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BloodDonation.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// declares DonorCandidateRepository class.
    /// </summary>
    public class DonorCandidateRepository : MyRepository<DonorCandidate>, IDonorCandidateRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DonorCandidateRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext parameter.</param>
        public DonorCandidateRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// implements a new ChangeConstantAddress method of DonorCandidateRepository.
        /// </summary>
        /// <param name="id">id as string parameter.</param>
        /// <param name="newAddress">address as string parameter.</param>
        public void ChangeConstantAddress(string id, string newAddress)
        {
            var donorcandidate = this.GetOne(id);
            donorcandidate.ConstantAddress = newAddress;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// implements a new ChangeSuitability method of DonorCandidateRepository.
        /// </summary>
        /// <param name="id">is as string parameter.</param>
        /// <param name="suitablility">suitability as bool parameter.</param>
        public void ChangeSuitability(string id, bool suitablility)
        {
            var donorcandidate = this.GetOne(id);
            donorcandidate.Suitability = suitablility;
            this.ctx.SaveChanges();
        }

        /// <summary>
        ///  implements a new GetOne method of DonorCandidateRepository.
        /// </summary>
        /// <param name="id">id as string parameter.</param>
        /// <returns>returns one specific object.</returns>
        public override DonorCandidate GetOne(string id)
        {
            return this.GetAll().SingleOrDefault(x => x.Tajnumber == id);
        }
    }
}
