﻿// <copyright file="RecipientRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Repository
{
    using System.Linq;
    using BloodDonation.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// declares RecipientRepository class.
    /// </summary>
    public class RecipientRepository : MyRepository<Recipient>, IRecipientRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RecipientRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext parameter.</param>
        public RecipientRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// implements ChangeConstantAddress method of RecipientRepository.
        /// </summary>
        /// <param name="taj">tajnumber as string parameter.</param>
        /// <param name="newAddress">address as string parameter.</param>
        public void ChangeConstantAddress(string taj, string newAddress)
        {
            var donorcandidate = this.GetOne(taj);
            donorcandidate.ConstantAddress = newAddress;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// implements GetOne method of RecipientRepository.
        /// </summary>
        /// <param name="id">id as string parameter.</param>
        /// <returns>returns one specific object.</returns>
        public override Recipient GetOne(string id)
        {
            return this.GetAll().SingleOrDefault(x => x.Tajnumber == id);
        }
    }
}
