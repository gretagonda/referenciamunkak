﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "ctx needs to be visible", Scope = "member", Target = "~F:BloodDonation.Repository.Repository`1.ctx")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "ctx needs to be visible", Scope = "member", Target = "~F:BloodDonation.Repository.MyRepository`1.ctx")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "ctx needs to be visible", Scope = "member", Target = "~F:BloodDonation.Repository.MyRepository`1.ctx")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "I do not need the type of the exception", Scope = "member", Target = "~M:BloodDonation.Repository.MyRepository`1.Remove(`0)~System.Boolean")]
