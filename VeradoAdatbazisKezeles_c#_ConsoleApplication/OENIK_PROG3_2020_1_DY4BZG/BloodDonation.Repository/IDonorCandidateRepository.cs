﻿// <copyright file="IDonorCandidateRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BloodDonation.Repository
{
    using BloodDonation.Data;

    /// <summary>
    /// declaring IDonorCandidateRepository interface.
    /// </summary>
    public interface IDonorCandidateRepository : IRepository<DonorCandidate>
    {
        /// <summary>
        /// declaring ChangeSuitability method.
        /// </summary>
        /// <param name="id">string parameter.</param>
        /// <param name="suitablility">bool parameter.</param>
        void ChangeSuitability(string id, bool suitablility);

        /// <summary>
        /// declaring ChangeConstantAddress method.
        /// </summary>
        /// <param name="id">first string parameter.</param>
        /// <param name="newAddress">second string parameter.</param>
        void ChangeConstantAddress(string id, string newAddress);
    }
}
