﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CNTK;

namespace ChronicKidneyDisease_CNTK
{
    class ChriniKidneyDiseaseClassification
    {

        const int beolvasottMeret = 40;
        //const int beolvasottMeret = 1;
        const int betanitasHossza = 1000;

        readonly Variable x;
        readonly Function y;

        public ChriniKidneyDiseaseClassification(int rejtettNeuronokSzama, int retegekSzama)
        {
            if (retegekSzama == 1)
            {
                int[] retegek = new int[] { Adatbazis.BemenetMeret, rejtettNeuronokSzama, Adatbazis.KimenetMeret };

                x = Variable.InputVariable(new int[] { retegek[0] }, DataType.Float, "x");

                Function utolsoReteg = x;
                for (int i = 0; i < retegek.Length - 1; i++)
                {
                    Parameter w = new Parameter(new int[] { retegek[i + 1], retegek[i] }, DataType.Float, CNTKLib.GlorotNormalInitializer());
                    Parameter b = new Parameter(new int[] { retegek[i + 1] }, DataType.Float, CNTKLib.GlorotNormalInitializer());

                    Function times = CNTKLib.Times(w, utolsoReteg);
                    Function plus = CNTKLib.Plus(times, b);
                    utolsoReteg = CNTKLib.Sigmoid(plus);
                }

                y = utolsoReteg;
            }
            if (retegekSzama == 3)
            {
                int[] retegek = new int[] { Adatbazis.BemenetMeret, rejtettNeuronokSzama, rejtettNeuronokSzama, rejtettNeuronokSzama, Adatbazis.KimenetMeret };

                x = Variable.InputVariable(new int[] { retegek[0] }, DataType.Float, "x");

                Function utolsoReteg = x;
                for (int i = 0; i < retegek.Length - 1; i++)
                {
                    Parameter w = new Parameter(new int[] { retegek[i + 1], retegek[i] }, DataType.Float, CNTKLib.GlorotNormalInitializer());
                    Parameter b = new Parameter(new int[] { retegek[i + 1] }, DataType.Float, CNTKLib.GlorotNormalInitializer());

                    Function times = CNTKLib.Times(w, utolsoReteg);
                    Function plus = CNTKLib.Plus(times, b);
                    utolsoReteg = CNTKLib.Sigmoid(plus);
                }

                y = utolsoReteg;
            }
            if (retegekSzama == 5)
            {
                int[] retegek = new int[] { Adatbazis.BemenetMeret, rejtettNeuronokSzama, rejtettNeuronokSzama, rejtettNeuronokSzama, rejtettNeuronokSzama, rejtettNeuronokSzama, Adatbazis.KimenetMeret };

                x = Variable.InputVariable(new int[] { retegek[0] }, DataType.Float, "x");

                Function utolsoReteg = x;
                for (int i = 0; i < retegek.Length - 1; i++)
                {
                    Parameter w = new Parameter(new int[] { retegek[i + 1], retegek[i] }, DataType.Float, CNTKLib.GlorotNormalInitializer());
                    Parameter b = new Parameter(new int[] { retegek[i + 1] }, DataType.Float, CNTKLib.GlorotNormalInitializer());

                    Function times = CNTKLib.Times(w, utolsoReteg);
                    Function plus = CNTKLib.Plus(times, b);
                    utolsoReteg = CNTKLib.Sigmoid(plus);
                }

                y = utolsoReteg;
            }

        }
        public ChriniKidneyDiseaseClassification(string filename)
        {
            y = Function.Load(filename, DeviceDescriptor.CPUDevice);
            x = y.Arguments.First(x => x.Name == "x");
        }

        public int GetBetanitashossza()
        {
            return betanitasHossza;
        }
        public int GetBeolvasottmeret()
        {
            return beolvasottMeret;
        }
        public void Train(Adatbazis adatok, double l1weight, double l2weight)
        {
            Variable yt = Variable.InputVariable(new int[] { Adatbazis.KimenetMeret }, DataType.Float);
            Function loss = CNTKLib.BinaryCrossEntropy(y, yt);

            Function y_rounded = CNTKLib.Round(y);
            Function y_yt_equal = CNTKLib.Equal(y_rounded, yt);

            AdditionalLearningOptions alo = new AdditionalLearningOptions();
            alo.l1RegularizationWeight = l1weight;
            alo.l2RegularizationWeight = l2weight;

            Learner learner = CNTKLib.SGDLearner(new ParameterVector(y.Parameters().ToArray()), new TrainingParameterScheduleDouble(0.01, beolvasottMeret), alo);
            Trainer trainer = Trainer.CreateTrainer(y, loss, y_yt_equal, new List<Learner>() { learner });

            for (int tanitasSzamlalo = 0; tanitasSzamlalo <= betanitasHossza; tanitasSzamlalo++)
            {
                double sumLoss = 0;
                double sumEval = 0;

                adatok.Sorcsere();
                for (int tetelSzamlalo = 0; tetelSzamlalo < adatok.SorokSzama / beolvasottMeret; tetelSzamlalo++)
                {
                    Value x_value = Value.CreateBatch(x.Shape, adatok.Bemenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.BemenetMeret, beolvasottMeret * Adatbazis.BemenetMeret), DeviceDescriptor.CPUDevice);
                    Value yt_value = Value.CreateBatch(yt.Shape, adatok.Kimenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.KimenetMeret, beolvasottMeret * Adatbazis.KimenetMeret), DeviceDescriptor.CPUDevice);
                    var inputDataMap = new Dictionary<Variable, Value>()
                    {
                        { x, x_value },
                        { yt, yt_value }
                    };

                    trainer.TrainMinibatch(inputDataMap, false, DeviceDescriptor.CPUDevice);
                    sumLoss += trainer.PreviousMinibatchLossAverage() * trainer.PreviousMinibatchSampleCount();
                    sumEval += trainer.PreviousMinibatchEvaluationAverage() * trainer.PreviousMinibatchSampleCount();
                }
                Console.WriteLine(String.Format("{0}\tloss:{1}\teval:{2}", tanitasSzamlalo, sumLoss / adatok.SorokSzama, sumEval / adatok.SorokSzama));
            }
        }

        public double Evaluate(Adatbazis adatok)
        {
           
            Variable yt = Variable.InputVariable(new int[] { Adatbazis.KimenetMeret }, DataType.Float);

            Function y_rounded = CNTKLib.Round(y);
            Function y_yt_equal = CNTKLib.Equal(y_rounded, yt);
            Evaluator evaluator = CNTKLib.CreateEvaluator(y_yt_equal);

            double sumEval = 0;
           
            for (int tetelSzamlalo = 0; tetelSzamlalo < adatok.SorokSzama / beolvasottMeret; tetelSzamlalo++)
            {
                Value x_value = Value.CreateBatch(x.Shape, adatok.Bemenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.BemenetMeret, beolvasottMeret * Adatbazis.BemenetMeret), DeviceDescriptor.CPUDevice);
                Value yt_value = Value.CreateBatch(yt.Shape, adatok.Kimenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.KimenetMeret, beolvasottMeret * Adatbazis.KimenetMeret), DeviceDescriptor.CPUDevice);
               

                var inputDataMap = new UnorderedMapVariableValuePtr()
                    {
                        { x, x_value },
                        { yt, yt_value }
                    };

                sumEval += evaluator.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * beolvasottMeret;
            }

            return sumEval / adatok.SorokSzama;
            
        }

        public double EvaluateLoss(Adatbazis adatok)
        {

            Variable yt = Variable.InputVariable(new int[] { Adatbazis.KimenetMeret }, DataType.Float);

            Function loss = CNTKLib.SquaredError(y, yt);
            Evaluator evaluator_loss = CNTKLib.CreateEvaluator(loss);

            double sumLoss = 0;

            for (int tetelSzamlalo = 0; tetelSzamlalo < adatok.SorokSzama / beolvasottMeret; tetelSzamlalo++)
            {
                Value x_value = Value.CreateBatch(x.Shape, adatok.Bemenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.BemenetMeret, beolvasottMeret * Adatbazis.BemenetMeret), DeviceDescriptor.CPUDevice);
                Value yt_value = Value.CreateBatch(yt.Shape, adatok.Kimenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.KimenetMeret, beolvasottMeret * Adatbazis.KimenetMeret), DeviceDescriptor.CPUDevice);


                var inputDataMap = new UnorderedMapVariableValuePtr()
                    {
                        { x, x_value },
                        { yt, yt_value }
                    };

                sumLoss += evaluator_loss.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * beolvasottMeret;
            }

            return sumLoss / adatok.SorokSzama;

        }

        public double EvaluateFN(Adatbazis adatok)
        {
            Variable harom = Variable.InputVariable(new int[] { Adatbazis.KimenetMeret }, DataType.Float);
            Variable ketto = Variable.InputVariable(new int[] { Adatbazis.KimenetMeret }, DataType.Float);
            Variable egy = Variable.InputVariable(new int[] { Adatbazis.KimenetMeret  }, DataType.Float);
            Variable yt = Variable.InputVariable(new int[] { Adatbazis.KimenetMeret }, DataType.Float);

            Function y_rounded = CNTKLib.Round(y);


            Function FN = CNTKLib.Equal(CNTKLib.ElementDivide(CNTKLib.Plus(yt, ketto), CNTKLib.Plus(CNTKLib.Round(y), egy)), harom);
            Evaluator evaluatorFN = CNTKLib.CreateEvaluator(FN);


            double sumFN = 0;
                for (int tetelSzamlalo = 0; tetelSzamlalo < adatok.SorokSzama / beolvasottMeret; tetelSzamlalo++)
                {
                    Value x_value = Value.CreateBatch(x.Shape, adatok.Bemenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.BemenetMeret, beolvasottMeret * Adatbazis.BemenetMeret), DeviceDescriptor.CPUDevice);
                    Value yt_value = Value.CreateBatch(yt.Shape, adatok.Kimenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.KimenetMeret, beolvasottMeret * Adatbazis.KimenetMeret), DeviceDescriptor.CPUDevice);
                    Value harom_value = Value.CreateBatch(harom.Shape, new List<float>() { 3.0f}, DeviceDescriptor.CPUDevice);
                    Value ketto_value = Value.CreateBatch(ketto.Shape, new List<float>() { 2.0f }, DeviceDescriptor.CPUDevice);
                    Value egy_value = Value.CreateBatch(egy.Shape, new List<float>() { 1.0f}, DeviceDescriptor.CPUDevice);

                    var inputDataMap = new UnorderedMapVariableValuePtr()
                    {
                        { x, x_value },
                        { yt, yt_value },
                        {harom, harom_value },
                        {ketto, ketto_value },
                        {egy, egy_value }
                    };
               
                    sumFN += evaluatorFN.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * beolvasottMeret;
               
            }


            return sumFN;
        }


        public double EvaluateTP(Adatbazis adatok)
        {
            Variable masfel = Variable.InputVariable(new int[] { 1 }, DataType.Float);
            Variable ketto = Variable.InputVariable(new int[] { 1 }, DataType.Float);
            Variable egy = Variable.InputVariable(new int[] { 1 }, DataType.Float);
            Variable yt = Variable.InputVariable(new int[] { Adatbazis.KimenetMeret }, DataType.Float);

            Function y_rounded = CNTKLib.Round(y);

            Function TP = CNTKLib.Equal(CNTKLib.ElementDivide(CNTKLib.Plus(yt, ketto), CNTKLib.Plus(y_rounded, egy)), masfel);
            Evaluator evaluatorTP = CNTKLib.CreateEvaluator(TP);

            double sumTP = 0;


                for (int tetelSzamlalo = 0; tetelSzamlalo < adatok.SorokSzama / beolvasottMeret; tetelSzamlalo++)
                {
                Value x_value = Value.CreateBatch(x.Shape, adatok.Bemenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.BemenetMeret, beolvasottMeret * Adatbazis.BemenetMeret), DeviceDescriptor.CPUDevice);

                Value yt_value = Value.CreateBatch(yt.Shape, adatok.Kimenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.KimenetMeret, beolvasottMeret * Adatbazis.KimenetMeret), DeviceDescriptor.CPUDevice);
                    Value masfel_value = Value.CreateBatch(new int[] { 1 }, new List<float>() { 1.5f }, DeviceDescriptor.CPUDevice);
                    Value ketto_value = Value.CreateBatch(new int[] { 1 }, new List<float>() { 2.0f }, DeviceDescriptor.CPUDevice);
                    Value egy_value = Value.CreateBatch(new int[] { 1 }, new List<float>() { 1.0f }, DeviceDescriptor.CPUDevice);

                    var inputDataMap = new UnorderedMapVariableValuePtr()
                    {
                        { x, x_value },
                        { yt, yt_value },
                        {masfel, masfel_value },
                        {ketto, ketto_value },
                        {egy, egy_value }
                    };

                    sumTP += evaluatorTP.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * beolvasottMeret;


            }


            return sumTP;
        }

        public double EvaluateTN(Adatbazis adatok)
        {
            Variable ketto = Variable.InputVariable(new int[] { 1 }, DataType.Float);
            Variable egy = Variable.InputVariable(new int[] { 1 }, DataType.Float);
            Variable yt = Variable.InputVariable(new int[] { Adatbazis.KimenetMeret }, DataType.Float);

            Function y_rounded = CNTKLib.Round(y);

            Function TN = CNTKLib.Equal(CNTKLib.ElementDivide(CNTKLib.Plus(yt, ketto), CNTKLib.Plus(y_rounded, egy)), ketto);
            Evaluator evaluatorTN = CNTKLib.CreateEvaluator(TN);

            double sumTN = 0;

                for (int tetelSzamlalo = 0; tetelSzamlalo < adatok.SorokSzama / beolvasottMeret; tetelSzamlalo++)
                {
                    Value x_value = Value.CreateBatch(x.Shape, adatok.Bemenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.BemenetMeret, beolvasottMeret * Adatbazis.BemenetMeret), DeviceDescriptor.CPUDevice);

                    Value yt_value = Value.CreateBatch(yt.Shape, adatok.Kimenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.KimenetMeret, beolvasottMeret * Adatbazis.KimenetMeret), DeviceDescriptor.CPUDevice);

                    Value ketto_value = Value.CreateBatch(new int[] { 1 }, new List<float>() { 2.0f }, DeviceDescriptor.CPUDevice);
                    Value egy_value = Value.CreateBatch(new int[] { 1 }, new List<float>() { 1.0f }, DeviceDescriptor.CPUDevice);

                    var inputDataMap = new UnorderedMapVariableValuePtr()
                    {
                        { x, x_value },
                        { yt, yt_value },
                        {ketto, ketto_value },
                        {egy, egy_value }
                    };


                    sumTN += evaluatorTN.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * beolvasottMeret;

            }


            return sumTN;
        }

        public double EvaluateFP(Adatbazis adatok)
        {
            Variable ketto = Variable.InputVariable(new int[] { 1 }, DataType.Float);
            Variable egy = Variable.InputVariable(new int[] { 1 }, DataType.Float);
            Variable yt = Variable.InputVariable(new int[] { Adatbazis.KimenetMeret }, DataType.Float);

            Function y_rounded = CNTKLib.Round(y);

            Function FP = CNTKLib.Equal(CNTKLib.ElementDivide(CNTKLib.Plus(yt, ketto), CNTKLib.Plus(y_rounded, egy)), egy);
            Evaluator evaluatorFP = CNTKLib.CreateEvaluator(FP);

            double sumFP = 0;
            
                for (int tetelSzamlalo = 0; tetelSzamlalo < adatok.SorokSzama / beolvasottMeret; tetelSzamlalo++)
                {
                    Value x_value = Value.CreateBatch(x.Shape, adatok.Bemenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.BemenetMeret, beolvasottMeret * Adatbazis.BemenetMeret), DeviceDescriptor.CPUDevice);
                    Value yt_value = Value.CreateBatch(yt.Shape, adatok.Kimenet.GetRange(tetelSzamlalo * beolvasottMeret * Adatbazis.KimenetMeret, beolvasottMeret * Adatbazis.KimenetMeret), DeviceDescriptor.CPUDevice);
                    Value ketto_value = Value.CreateBatch(new int[] { 1 }, new List<float>() { 2.0f }, DeviceDescriptor.CPUDevice);
                    Value egy_value = Value.CreateBatch(new int[] { 1 }, new List<float>() { 1.0f }, DeviceDescriptor.CPUDevice);

                    var inputDataMap = new UnorderedMapVariableValuePtr()
                    {
                        { x, x_value },
                        { yt, yt_value },
                        {ketto, ketto_value },
                        {egy, egy_value }
                    };

                sumFP += evaluatorFP.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * beolvasottMeret;


            }


            return sumFP;
        }

        public void Save(string filename)
        {
            y.Save(filename);
        }

    }



    public class Adatbazis
    {
        public const int BemenetMeret = 13;
        public List<float> Bemenet { get; set; } = new List<float>();

        public const int KimenetMeret = 1;
        public List<float> Kimenet { get; set; } = new List<float>();

        public int SorokSzama { get; set; }

        public Adatbazis(string file)
        {
            Beolvasas(file);
        }

        void Beolvasas(string file)
        {
            SorokSzama = 0;
            foreach (String line in File.ReadAllLines(file))
            {
                var floats = Normalize(line.Split('\t').Select(x => float.Parse(x)).ToList());

                Bemenet.AddRange(floats.GetRange(0, BemenetMeret));
                Kimenet.Add(floats[BemenetMeret]);
                SorokSzama++;
            }
        }

        public void Sorcsere()
        {
            Random rnd = new Random();
            for (int sorszamlalo = 0; sorszamlalo < SorokSzama; sorszamlalo++)
            {
                var a = rnd.Next(SorokSzama);
                var b = rnd.Next(SorokSzama);
                if (a != b)
                {
                    float T;
                    for (int i = 0; i < BemenetMeret; i++)
                    {
                        T = Bemenet[a * BemenetMeret + i];
                        Bemenet[a * BemenetMeret + i] = Bemenet[b * BemenetMeret + i];
                        Bemenet[b * BemenetMeret + i] = T;
                    }
                    T = Kimenet[a];
                    Kimenet[a] = Kimenet[b];
                    Kimenet[b] = T;
                }
            }
        }

        static float[] minValues;
        static float[] maxValues;

        public static List<float> Normalize(List<float> floats)
        {
            List<float> normalized = new List<float>();
            for (int i = 0; i < floats.Count; i++)
                normalized.Add((floats[i] - minValues[i]) / (maxValues[i] - minValues[i]));
            return normalized;
        }

        public static void LoadMinMax(string filename)
        {
            foreach (String line in File.ReadAllLines(filename))
            {
                var floats = line.Split('\t').Select(x => float.Parse(x)).ToList();
                if (minValues == null)
                {
                    minValues = floats.ToArray();
                    maxValues = floats.ToArray();
                }
                else
                {
                    for (int i = 0; i < floats.Count; i++)
                        if (floats[i] < minValues[i])
                            minValues[i] = floats[i];
                        else
                            if (floats[i] > maxValues[i])
                            maxValues[i] = floats[i];
                }
            }
        }
    }
    public class Program
    {

        void RunCase(string caseID, Adatbazis tanitoAdatok, Adatbazis teszteloAdatok, int rejtettNeuronokSzama, int retegekSzama, double l1weight, double l2weight, StreamWriter sw)
        {
            ChriniKidneyDiseaseClassification app = new ChriniKidneyDiseaseClassification(rejtettNeuronokSzama, retegekSzama);
            app.Train(tanitoAdatok, l1weight, l2weight);
            app.Save(@"..\TobbreteguRegularizacios_"+rejtettNeuronokSzama+"_"+retegekSzama+"_"+app.GetBetanitashossza()+"_"+l1weight+"_"+l2weight+ ".model");
            
            double tanitoEval = app.Evaluate(tanitoAdatok);
            double teszteloEval = app.Evaluate(teszteloAdatok);
            double tanitoEvalLoss = app.EvaluateLoss(tanitoAdatok);
            double teszteloEvalLoss = app.EvaluateLoss(teszteloAdatok);
            Console.WriteLine(caseID + "::: ");
            sw.WriteLine(caseID + " EREDMENY::: ");
            Console.WriteLine("Eval train: " + tanitoEval);
            sw.WriteLine("Eval train: " + tanitoEval);
            Console.WriteLine("Eval test: " + teszteloEval);
            sw.WriteLine("Eval test: " + teszteloEval);
            Console.WriteLine();
            Console.WriteLine("Loss train: " + tanitoEvalLoss);
            sw.WriteLine("Loss train: " + tanitoEvalLoss);
            Console.WriteLine("Loss test: " + teszteloEvalLoss);
            sw.WriteLine("Loss test: " + teszteloEvalLoss);

            if (app.GetBeolvasottmeret() == 1)
            {
                double tanitoFN = app.EvaluateFN(tanitoAdatok);
                double tanitoTP = app.EvaluateTP(tanitoAdatok);
                double tanitoTN = app.EvaluateTN(tanitoAdatok);
                double tanitoFP = app.EvaluateFP(tanitoAdatok);
                double pontossagTanito = 0;
                pontossagTanito = (tanitoTP + tanitoTN) / (tanitoFN + tanitoTP + tanitoTN + tanitoFP);
                double precizitásTanito = 0;
                precizitásTanito = (tanitoTP) / (tanitoTP + tanitoFP);
                double szenzitivitásTanito = 0;
                szenzitivitásTanito = (tanitoTP) / (tanitoFN + tanitoTP);
                double specificitásTanito = 0;
                specificitásTanito = (tanitoTN) / (tanitoTN + tanitoFP);
                double f1Tanito = 0;
                f1Tanito = 2 * ((pontossagTanito * szenzitivitásTanito) / (pontossagTanito + szenzitivitásTanito));

                double teszteloFN = app.EvaluateFN(teszteloAdatok);
                double teszteloTP = app.EvaluateTP(teszteloAdatok);
                double teszteloTN = app.EvaluateTN(teszteloAdatok);
                double teszteloFP = app.EvaluateFP(teszteloAdatok);
                double pontossagTesztelo = 0;
                pontossagTesztelo = (teszteloTP + teszteloTN) / (teszteloFN + teszteloTP + teszteloTN + teszteloFP);
                double precizitásTesztelo = 0;
                precizitásTesztelo = teszteloTP / (teszteloTP + teszteloFP);
                double szenzitivitásTesztelo = 0;
                szenzitivitásTesztelo = teszteloTP / (teszteloFN + teszteloTP);
                double specificitásTesztelo = 0;
                specificitásTesztelo = teszteloTN / (teszteloTN + teszteloFP);
                double f1Tesztelo = 0;
                f1Tesztelo = 2 * ((pontossagTesztelo * szenzitivitásTesztelo) / (pontossagTesztelo + szenzitivitásTesztelo));



                Console.WriteLine();
                sw.WriteLine();
                Console.WriteLine("False Negative train: " + tanitoFN);
                sw.WriteLine("False Negative train: " + tanitoFN);
                Console.WriteLine("True Positive train: " + tanitoTP);
                sw.WriteLine("True Positive train: " + tanitoTP);
                Console.WriteLine("True Negative train: " + tanitoTN);
                sw.WriteLine("True Negative train: " + tanitoTN);
                Console.WriteLine("False Positive train: " + tanitoFP);
                sw.WriteLine("False Positive train: " + tanitoFP);
                Console.WriteLine("Pontosság-tanító: " + pontossagTanito);
                sw.WriteLine("Pontosság-tanító: " + pontossagTanito);
                Console.WriteLine("Precizitás-tanító: " + precizitásTanito);
                sw.WriteLine("Precizitás-tanító: " + precizitásTanito);
                Console.WriteLine("Szenzitivitás-tanító: " + szenzitivitásTanito);
                sw.WriteLine("Szenzitivitás-tanító: " + szenzitivitásTanito);
                Console.WriteLine("Specificitás-tanító: " + specificitásTanito);
                sw.WriteLine("Specificitás-tanító: " + specificitásTanito);
                Console.WriteLine("F1-tanító: " + f1Tanito);
                sw.WriteLine("F1-tanító: " + f1Tanito);



                Console.WriteLine("False Negative test: " + teszteloFN);
                sw.WriteLine("False Negative test: " + teszteloFN);
                Console.WriteLine("True Positive test: " + teszteloTP);
                sw.WriteLine("True Positive tesz: " + teszteloTP);
                Console.WriteLine("True Negative test: " + teszteloTN);
                sw.WriteLine("True Negative test: " + teszteloTN);
                Console.WriteLine("False Positive test: " + teszteloFP);
                sw.WriteLine("False Positive test: " + teszteloFP);
                Console.WriteLine("Pontosság-tesztelő: " + pontossagTesztelo);
                sw.WriteLine("Pontosság-tesztelő: " + pontossagTesztelo);
                Console.WriteLine("Precizitás-tesztelő: " + precizitásTesztelo);
                sw.WriteLine("Precizitás-tesztelő: " + precizitásTesztelo);
                Console.WriteLine("Szenzitivitás-tesztelő: " + szenzitivitásTesztelo);
                sw.WriteLine("Szenzitivitás-tesztelő: " + szenzitivitásTesztelo);
                Console.WriteLine("Specificitás-tesztelő: " + specificitásTesztelo);
                sw.WriteLine("Specificitás-tesztelő: " + specificitásTesztelo);
                Console.WriteLine("F1-tesztelő: " + f1Tesztelo);
                sw.WriteLine("F1-tesztelő: " + f1Tesztelo);
            }
            Console.WriteLine("------------------------------------------------------------------------");
            sw.WriteLine("------------------------------------------------------------------------");

        }

        void RunAllCases(StreamWriter sw)
        {
            
            Adatbazis.LoadMinMax(@"..\chronic_kidney_disease_file.txt");
            Adatbazis tanitoAdatok = new Adatbazis(@"..\chronic_kidney_disease_file.txt");
            Adatbazis teszteloAdatok = new Adatbazis(@"..\chronic_kidney_disease_test.txt");

            //RunCase("5_1_8000_0_0", tanitoAdatok, teszteloAdatok, 5, 1, 0, 0, sw);
            //RunCase("5_3_8000_0_0", tanitoAdatok, teszteloAdatok, 5, 3, 0, 0, sw);
           //RunCase("5_5_6000_0_0", tanitoAdatok, teszteloAdatok, 5, 5, 0, 0, sw);

            //RunCase("13_1_8000_0_0", tanitoAdatok, teszteloAdatok, 13, 1, 0, 0, sw);
            //RunCase("13_3_8000_0_0", tanitoAdatok, teszteloAdatok, 13, 3, 0, 0, sw);
            //RunCase("13_5_6000_0_0", tanitoAdatok, teszteloAdatok, 13, 5, 0, 0, sw);

            //RunCase("26_1_8000_0_0", tanitoAdatok, teszteloAdatok, 26, 1, 0, 0, sw);
            //RunCase("26_3_5000_0_0", tanitoAdatok, teszteloAdatok, 26, 3, 0, 0, sw);
            //RunCase("26_5_6000_0_0", tanitoAdatok, teszteloAdatok, 26, 5, 0, 0, sw);

            //RunCase("50_1_8000_0_0", tanitoAdatok, teszteloAdatok, 50, 1, 0, 0, sw);
            //RunCase("50_3_8000_0_0", tanitoAdatok, teszteloAdatok, 50, 3, 0, 0, sw);
            //RunCase("50_5_6000_0_0", tanitoAdatok, teszteloAdatok, 50, 5, 0, 0, sw);


            //RunCase("5_1_5000_0.0001_0", tanitoAdatok, teszteloAdatok, 5, 1, 0.0001, 0, sw);
            //RunCase("5_3_100_0.0001_0", tanitoAdatok, teszteloAdatok, 5, 3, 0.0001, 0, sw);
            //RunCase("5_5_100_0.0001_0", tanitoAdatok, teszteloAdatok, 5, 5, 0.0001, 0, sw);

            //RunCase("13_1_5000_0.0001_0", tanitoAdatok, teszteloAdatok, 13, 1,  0.0001, 0, sw);
            //RunCase("13_3_100_0.0001_0", tanitoAdatok, teszteloAdatok, 13, 3, 0.0001, 0, sw);
            //RunCase("13_5_100_0.0001_0", tanitoAdatok, teszteloAdatok, 13, 5, 0.0001, 0, sw);

            //RunCase("26_1_5000_0.0001_0", tanitoAdatok, teszteloAdatok, 26, 1, 0.0001, 0, sw);
            RunCase("26_3_5000_0.0001_0", tanitoAdatok, teszteloAdatok, 26, 3, 0.0001, 0, sw);
            //RunCase("26_5_100_0.0001_0", tanitoAdatok, teszteloAdatok, 26, 5, 0.0001, 0, sw);

            //RunCase("50_1_5000_0.0001_0", tanitoAdatok, teszteloAdatok, 50, 1, 0.0001, 0, sw);
            //RunCase("50_3_100_0.0001_0", tanitoAdatok, teszteloAdatok, 50, 3, 0.0001, 0, sw);
            //RunCase("50_5_100_0.0001_0", tanitoAdatok, teszteloAdatok, 50, 5, 0.0001, 0, sw);


            //RunCase("5_1_5000_0.001_0", tanitoAdatok, teszteloAdatok, 5, 1, 0.001, 0, sw);
            //RunCase("5_3_100_0.001_0", tanitoAdatok, teszteloAdatok, 5, 3, 0.001, 0, sw);
            //RunCase("5_5_100_0.001_0", tanitoAdatok, teszteloAdatok, 5, 5, 0.001, 0, sw);

            //RunCase("13_1_5000_0.001_0", tanitoAdatok, teszteloAdatok, 13, 1, 0.001, 0, sw);
            //RunCase("13_3_100_0.001_0", tanitoAdatok, teszteloAdatok, 13, 3, 0.001, 0, sw);
            //RunCase("13_5_100_0.001_0", tanitoAdatok, teszteloAdatok, 13, 5, 0.001, 0, sw);

            //RunCase("26_1_5000_0.001_0", tanitoAdatok, teszteloAdatok, 26, 1, 0.001, 0, sw);
            //RunCase("26_3_5000_0.001_0", tanitoAdatok, teszteloAdatok, 26, 3, 0.001, 0, sw);
            //RunCase("26_5_100_0.001_0", tanitoAdatok, teszteloAdatok, 26, 5, 0.001, 0, sw);

            //RunCase("50_1_5000_0.001_0", tanitoAdatok, teszteloAdatok, 50, 1, 0.001, 0, sw);
            //RunCase("50_3_100_0.001_0", tanitoAdatok, teszteloAdatok, 50, 3, 0.001, 0, sw);
            //RunCase("50_5_100_0.001_0", tanitoAdatok, teszteloAdatok, 50, 5, 0.001, 0, sw);


            //RunCase("5_1_5000_0.01_0", tanitoAdatok, teszteloAdatok, 5, 1, 0.01, 0, sw);
            //RunCase("5_3_100_0.01_0", tanitoAdatok, teszteloAdatok, 5, 3, 0.01, 0, sw);
            //RunCase("5_5_100_0.01_0", tanitoAdatok, teszteloAdatok, 5, 5, 0.01, 0, sw);

            //RunCase("13_1_5000_0.01_0", tanitoAdatok, teszteloAdatok, 13, 1, 0.01, 0, sw);
            //RunCase("13_3_100_0.01_0", tanitoAdatok, teszteloAdatok, 13, 3, 0.01, 0, sw);
            //RunCase("13_5_100_0.01_0", tanitoAdatok, teszteloAdatok, 13, 5, 0.01, 0, sw);

            //RunCase("26_1_5000_0.01_0", tanitoAdatok, teszteloAdatok, 26, 1, 0.01, 0, sw);
            //RunCase("26_3_5000_0.01_0", tanitoAdatok, teszteloAdatok, 26, 3, 0.01, 0, sw);
            //RunCase("26_5_100_0.01_0", tanitoAdatok, teszteloAdatok, 26, 5, 0.01, 0, sw);

            //RunCase("50_1_5000_0.01_0", tanitoAdatok, teszteloAdatok, 50, 1, 0.01, 0, sw);
            //RunCase("50_3_100_0.01_0", tanitoAdatok, teszteloAdatok, 50, 3, 0.01, 0, sw);
            //RunCase("50_5_100_0.01_0", tanitoAdatok, teszteloAdatok, 50, 5, 0.01, 0, sw);



            //RunCase("5_1_5000_0_0.0001", tanitoAdatok, teszteloAdatok, 5, 1, 0, 0.0001, sw);
            //RunCase("5_3_100_0_0.0001", tanitoAdatok, teszteloAdatok, 5, 3, 0, 0.0001, sw);
            //RunCase("5_5_100_0_0.0001", tanitoAdatok, teszteloAdatok, 5, 5, 0, 0.0001, sw);

            //RunCase("13_1_5000_0_0.0001", tanitoAdatok, teszteloAdatok, 13, 1, 0, 0.0001, sw);
            //RunCase("13_3_100_0_0.0001", tanitoAdatok, teszteloAdatok, 13, 3, 0, 0.0001, sw);
            //RunCase("13_5_100_0_0.0001", tanitoAdatok, teszteloAdatok, 13, 5, 0, 0.0001, sw);

            //RunCase("26_1_5000_0_0.0001", tanitoAdatok, teszteloAdatok, 26, 1, 0, 0.0001, sw);
            //RunCase("26_3_5000_0_0.0001", tanitoAdatok, teszteloAdatok, 26, 3, 0, 0.0001, sw);
            //RunCase("26_5_100_0_0.0001", tanitoAdatok, teszteloAdatok, 26, 5, 0, 0.0001, sw);

            //RunCase("50_1_5000_0_0.0001", tanitoAdatok, teszteloAdatok, 50, 1, 0, 0.0001, sw);
            //RunCase("50_3_100_0_0.0001", tanitoAdatok, teszteloAdatok, 50, 3, 0, 0.0001, sw);
            //RunCase("50_5_100_0_0.0001", tanitoAdatok, teszteloAdatok, 50, 5, 0, 0.0001, sw);


            //RunCase("5_1_5000_0_0.001", tanitoAdatok, teszteloAdatok, 5, 1, 0, 0.001, sw);
            //RunCase("5_3_100_0_0.001", tanitoAdatok, teszteloAdatok, 5, 3, 0, 0.001, sw);
            //RunCase("5_5_100_0_0.001", tanitoAdatok, teszteloAdatok, 5, 5, 0, 0.001, sw);

            //RunCase("13_1_5000_0_0.001", tanitoAdatok, teszteloAdatok, 13, 1, 0, 0.001, sw);
            //RunCase("13_3_100_0_0.001", tanitoAdatok, teszteloAdatok, 13, 3, 0, 0.001, sw);
            //RunCase("13_5_100_0_0.001", tanitoAdatok, teszteloAdatok, 13, 5, 0, 0.001, sw);

            //RunCase("26_1_5000_0_0.001", tanitoAdatok, teszteloAdatok, 26, 1, 0, 0.001, sw);
            //RunCase("26_3_5000_0_0.001", tanitoAdatok, teszteloAdatok, 26, 3, 0, 0.001, sw);
            //RunCase("26_5_100_0_0.001", tanitoAdatok, teszteloAdatok, 26, 5, 0, 0.001, sw);

            //RunCase("50_1_5000_0_0.001", tanitoAdatok, teszteloAdatok, 50, 1, 0, 0.001, sw);
            //RunCase("50_3_100_0_0.001", tanitoAdatok, teszteloAdatok, 50, 3, 0, 0.001, sw);
            //RunCase("50_5_100_0_0.001", tanitoAdatok, teszteloAdatok, 50, 5, 0, 0.001, sw);


            //RunCase("5_1_5000_0_0.01", tanitoAdatok, teszteloAdatok, 5, 1, 0, 0.01, sw);
            //RunCase("5_3_100_0_0.01", tanitoAdatok, teszteloAdatok, 5, 3, 0, 0.01, sw);
            //RunCase("5_5_100_0_0.01", tanitoAdatok, teszteloAdatok, 5, 5, 0, 0.01, sw);

            //RunCase("13_1_5000_0_0.01", tanitoAdatok, teszteloAdatok, 13, 1, 0, 0.01, sw);
            //RunCase("13_3_100_0_0.01", tanitoAdatok, teszteloAdatok, 13, 3, 0, 0.01, sw);
            //RunCase("13_5_100_0_0.01", tanitoAdatok, teszteloAdatok, 13, 5, 0, 0.01, sw);

            //RunCase("26_1_5000_0_0.01", tanitoAdatok, teszteloAdatok, 26, 1, 0, 0.01, sw);
            //RunCase("26_3_5000_0_0.01", tanitoAdatok, teszteloAdatok, 26, 3, 0, 0.01, sw);
            //RunCase("26_5_100_0_0.01", tanitoAdatok, teszteloAdatok, 26, 5, 0, 0.01, sw);

            //RunCase("50_1_5000_0_0.01", tanitoAdatok, teszteloAdatok, 50, 1, 0, 0.01, sw);
            //RunCase("50_3_100_0_0.01", tanitoAdatok, teszteloAdatok, 50, 3, 0, 0.01, sw);
            //RunCase("50_5_100_0_0.01", tanitoAdatok, teszteloAdatok, 50, 5, 0, 0.01, sw);

        }
        static void Main(string[] args)
        {
            string eredmenyPath = "TobbreteguRegularizacios_eredmenyek.txt";
            StreamWriter sw = new StreamWriter(eredmenyPath,true);
            new Program().RunAllCases(sw);
            sw.Close();
        }
    }
}
