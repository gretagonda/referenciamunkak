﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DY4BZG_féléves
{
    class Feladat : IFeladat
    {
       
       
       public  DateTime kezdodatum { get; set; }
       public  int idoigeny { get; set; }

        public Feladat(DateTime kezdodatum, int idoigeny)
        {
            this.kezdodatum = kezdodatum;
            this.idoigeny = idoigeny;
        }

//Egy feladat értékeinek string-gé való alakítása
        public override string ToString()
        {
             return "A " + kezdodatum.ToString() + "-kor kezdődő " + idoigeny.ToString()+" óra időtartalmú feladat ";
            
        }

    }
}
