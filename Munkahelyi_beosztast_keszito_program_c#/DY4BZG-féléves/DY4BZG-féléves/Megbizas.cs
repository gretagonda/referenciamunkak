﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DY4BZG_féléves
{
    class Megbizas
    {
        public SajatLista<DateTime, Feladat> FeladatLista { get; }


        public SajatLista<int, Beosztott> BeosztottLista { get; }


        public Megbizas(SajatLista<DateTime, Feladat> feladatLista, SajatLista<int, Beosztott> beosztottLista)
        {
            FeladatLista = feladatLista;
            BeosztottLista = beosztottLista;
        }

//A Teljesítés metódus a feladatListában szereplő összes feladathoz ad megoldást
//3 eset van megoldásra:

        public void Teljesites(BeosztasKezelo kezeles, SajatLista<int,Beosztott > Beosztottak)
        {
            ListaElem<DateTime, Feladat> activFeladat = FeladatLista.head;


            SajatLista<int, Beosztott> torlendoBeosztottak;

            

            while (activFeladat.next != null)
            {


                IdoigenyTullepveEventHandler tullepesHandler = new IdoigenyTullepveEventHandler(IdoigenyTullepve);



                try
                {
                    torlendoBeosztottak = kezeles.beosztasKeszito(Beosztottak, activFeladat.next.value.idoigeny);
 //1.: A feladat időigénye pontosan kitölthető a BeosztottAK lista rendelkezére álló elemeivel
                    if (torlendoBeosztottak != null)
                    {
                        Console.WriteLine(activFeladat.next.value.ToString()+" megoldásához használt munkaidő-értékelés párok: ");
                        int index = 0;
                        int size = torlendoBeosztottak.size();
                        Beosztott torlendoBeosztott;

//Az adott feladat megoldásban felhasznált beosztottak kitörlésre kerülnek a Beosztottak listából, mivel egy beosztott sem dolgozhat kétszer


                        while (index != size)
                        {
                            Console.WriteLine(torlendoBeosztottak.get(index).ToString());
                            torlendoBeosztott = torlendoBeosztottak.get(index);

                            Beosztottak.TartalomAlapjanTorol(torlendoBeosztott);
                            index++;

                        }

 //2. : A feladat teljesíthető, de csak az időigény túllépésével                       
                    }
                    else if (torlendoBeosztottak == null)
                    {
                        

                        Beosztott maxbeosztott = Beosztottak.get(Beosztottak.size() - 1);
                        

                        int maxmunkakepesseg = maxbeosztott.munkakepesseg;

                        int indexer = 0;

//Az időigény egyenkénti emelésével keressük azt a megoldást ami a lehető legkevesebb idővel lóg túl az időigényen
//Ez a extra időigény maximum a Beosztottak lista legnagyobb munkaképességű beosztottjának munkaképességével lehet hoszabb az eredeti időigényénél


                        while (indexer<maxmunkakepesseg&&torlendoBeosztottak == null)
                        {

                            torlendoBeosztottak = kezeles.beosztasKeszito(Beosztottak, (activFeladat.next.value.idoigeny + indexer + 1));

 //Ha találunk ilyen megoldást az eseménykezelő tájékoztatja a felhasználót arról, hogy a feladat túllépte a rendelkezésre álló időigényt.                           
                            if (torlendoBeosztottak != null)
                            {
                                Console.WriteLine(activFeladat.next.value.ToString() + " megoldásához használt munkaidő-értékelés párok: ");
                                tullepesHandler(activFeladat.next);
                                int index = 0;
                                int size = torlendoBeosztottak.size();
                                Beosztott torlendoBeosztott;

                                while (index != size)
                                {
                                    
                                    Console.WriteLine(torlendoBeosztottak.get(index).ToString());
                                    torlendoBeosztott = torlendoBeosztottak.get(index);

                                    Beosztottak.TartalomAlapjanTorol(torlendoBeosztott);
                                    index++;

                                }

                            }
                            indexer++;




                        }
                    }

                }
//3. : A Beosztottak listában lévő beosztottak munkaképességének az összege nem éri el a feladat időigényét,
//tehát nincs elegendő számú ember a feladat teljesítéséhez
//Elkapásra kerül a BeosztasKezelo által dobott kivétel
                catch (NincsElegendoEmberException ex)
                {
                    Console.WriteLine(activFeladat.next.value.ToString() +  ":" + ex.Message);
                }
               
                activFeladat = activFeladat.next;

            }
        }

//Az időigény túllépéséről értesítő delegált létrehozása:
        public delegate void IdoigenyTullepveEventHandler(ListaElem<DateTime, Feladat> feladat);

        public static void IdoigenyTullepve(ListaElem<DateTime, Feladat> feladat)
        {
            Console.WriteLine("    + "+feladat.value.ToString() + "túllépte az időkeretet!");
            
        }

    }
}
