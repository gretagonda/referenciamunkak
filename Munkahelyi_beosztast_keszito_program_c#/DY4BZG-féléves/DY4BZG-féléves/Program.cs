﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DY4BZG_féléves
{
    partial class Program
    {
        static void Main(string[] args)
        {

//A beosztottak listája a main-ben kerül létrehozásra
//A kommenttelt beosztás részhez, a DY4BZG-féléves/bin/Debug/Bemenet.txt file tartozik
            SajatLista<int, Beosztott> Beosztottak = new SajatLista<int, Beosztott>();

            /*
            Beosztott Mariann = new Beosztott(10, 2);
            Beosztott Peter = new Beosztott(3,5);
            Beosztott Maja= new Beosztott(2,3);
            Beosztott Zsigmond= new Beosztott(8,4);
            Beosztott Ivett= new Beosztott(5,5);
            Beosztott Gergely= new Beosztott(4,6);
            Beosztott Ferenc= new Beosztott(1,3);
            Beosztott Botond= new Beosztott(7,2);
            Beosztott Sonja= new Beosztott(6,9);
            Beosztott Erik= new Beosztott(12,7);
            Beosztott Eszter= new Beosztott(11,6);
            Beosztott Konstantin= new Beosztott(7,3);
            Beosztott Eliza= new Beosztott(6,2);
            Beosztott Evelin= new Beosztott(9,5);
            Beosztott Janka= new Beosztott(1,1);
            Beosztott Zsanett= new Beosztott(4,6);
            Beosztott Emil= new Beosztott(3,2);
            Beosztott Klotild= new Beosztott(8,5);
            Beosztott Norman= new Beosztott(8,1);
            Beosztott Norbert= new Beosztott(2,2);
            Beosztott Robin= new Beosztott(3,3);
            Beosztott Roberta= new Beosztott(3,1);
            Beosztott Antal= new Beosztott(6,2);
            Beosztott Jakab= new Beosztott(5,2);
            Beosztott Borisz= new Beosztott(3,2);
           

            Beosztottak.Beszur(Mariann.munkakepesseg, Mariann) ;
            Beosztottak.Beszur(Peter.munkakepesseg, Peter);
            Beosztottak.Beszur(Maja.munkakepesseg, Maja);
            Beosztottak.Beszur(Zsigmond.munkakepesseg, Zsigmond);
            Beosztottak.Beszur(Ivett.munkakepesseg, Ivett);
            Beosztottak.Beszur(Gergely.munkakepesseg, Gergely);
            Beosztottak.Beszur(Ferenc.munkakepesseg, Ferenc);
            Beosztottak.Beszur(Botond.munkakepesseg, Botond);
            Beosztottak.Beszur(Sonja.munkakepesseg, Sonja);
            Beosztottak.Beszur(Erik.munkakepesseg, Erik);
            Beosztottak.Beszur(Eszter.munkakepesseg, Eszter);
            Beosztottak.Beszur(Konstantin.munkakepesseg, Konstantin);
            Beosztottak.Beszur(Eliza.munkakepesseg, Eliza);
            Beosztottak.Beszur(Evelin.munkakepesseg, Evelin);
            Beosztottak.Beszur(Janka.munkakepesseg, Janka);
            Beosztottak.Beszur(Zsanett.munkakepesseg, Zsanett);
            Beosztottak.Beszur(Emil.munkakepesseg, Emil);
            Beosztottak.Beszur(Klotild.munkakepesseg, Klotild);
            Beosztottak.Beszur(Norman.munkakepesseg, Norman);
            Beosztottak.Beszur(Norbert.munkakepesseg, Norbert);
            Beosztottak.Beszur(Robin.munkakepesseg, Robin);
            Beosztottak.Beszur(Roberta.munkakepesseg, Roberta);
            Beosztottak.Beszur(Antal.munkakepesseg, Antal);
            Beosztottak.Beszur(Jakab.munkakepesseg, Jakab);
            Beosztottak.Beszur(Borisz.munkakepesseg, Borisz);
            
            */


            Beosztott Kata = new Beosztott(2,3);
            Beosztottak.Beszur(Kata.munkakepesseg, Kata);
            Beosztott Sanyi = new Beosztott(3, 8);
            Beosztottak.Beszur(Sanyi.munkakepesseg, Sanyi);
            Beosztott Marci = new Beosztott(4,7);
            Beosztottak.Beszur(Marci.munkakepesseg, Marci);
            Beosztott Greti = new Beosztott(5, 6);
            Beosztottak.Beszur(Greti.munkakepesseg, Greti);
            Beosztott Jakab = new Beosztott(6, 7);
            Beosztottak.Beszur(Jakab.munkakepesseg, Jakab);




//A FeladatLista létrehozása:
            Bemenetkezelo beolvasva = new Bemenetkezelo("BemenetKicsi.txt");


            SajatLista<DateTime, Feladat> FeladatLista = beolvasva.BeolvasoFeladatlistaba();


           
            

            
//Megbízás létrehozása:
            Megbizas aktualisMegbizas = new Megbizas(FeladatLista, Beosztottak);

//BeosztásKezelő létrehozása:            
            BeosztasKezelo kezeles = new BeosztasKezelo(aktualisMegbizas);

            
//A megbízást teljesítő metódus meghívása:
            aktualisMegbizas.Teljesites(kezeles, Beosztottak);

        

            


        }
    }
}
