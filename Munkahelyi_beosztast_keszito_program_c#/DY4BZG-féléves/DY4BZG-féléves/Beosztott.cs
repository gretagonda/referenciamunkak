﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DY4BZG_féléves
{
    class Beosztott
    {

       public int munkakepesseg {get; set; }
       public int ertekeles { get; set; }

        public Beosztott(int munkakepesseg, int ertekeles)
        {
            this.munkakepesseg = munkakepesseg;
            this.ertekeles = ertekeles;
        }

//Egy beosztott értékeinek string-gé való alakítása
        public override string ToString()
        {
            return munkakepesseg.ToString() + "- " + ertekeles.ToString();
        }
    }
}
