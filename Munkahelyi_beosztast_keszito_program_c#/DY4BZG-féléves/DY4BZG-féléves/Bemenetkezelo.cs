﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DY4BZG_féléves
{
    class Bemenetkezelo
    {
        //Ez az osztály egy fileban előre megírt bemenetet olvas be
        //Egy feladat megadása egy külön sorban történik, melynek formátuma: kezdődátum-időigény
        //pl:2020.08.26-13

        string filename;
        public Bemenetkezelo(string filename)
        {
            this.filename = filename;
        }

        //A következő metódus dátum szerint rendezett listává alakítja a kapott feladatokat és az elkészített listát adja vissza
        //visszatérési értékként
        //A lista elemei kulcs-érték párok, ahol a kulcs a feladat kezdődátuma, az érték pedig a Feladat osztály egy példánya
        //a dátum tárolása DateTime-ban az időigényé int-ben történik
        public SajatLista<DateTime, Feladat> BeolvasoFeladatlistaba()
        {
            SajatLista<DateTime, Feladat> Feladatok = new SajatLista<DateTime, Feladat>();
            StreamReader myfile = new StreamReader(filename);
            string sorbeolvasva;

            DateTime kezdodate;

            int idoigeny;
            string[] sor;


            Feladat egyfeladat;


            while (!myfile.EndOfStream)
            {
                sorbeolvasva = myfile.ReadLine();
                sor = sorbeolvasva.Split('-');
                kezdodate = DateTime.Parse(sor[0]);
                idoigeny = int.Parse(sor[1]);
                egyfeladat = new Feladat(kezdodate, idoigeny);

                Feladatok.Beszur(egyfeladat.kezdodatum, egyfeladat);


            }

            return Feladatok;
        }

    }
}
