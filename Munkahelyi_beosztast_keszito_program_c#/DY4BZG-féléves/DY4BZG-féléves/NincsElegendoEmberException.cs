﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DY4BZG_féléves
{
    class NincsElegendoEmberException : Exception
    {
        public NincsElegendoEmberException(string message) : base(message)
        {
        }
    }
}
