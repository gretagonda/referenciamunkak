﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DY4BZG_féléves
{
    //Az alábbi osztály fő feladata az egy feladathoz tartozó optimális beosztás elkészítése
    class BeosztasKezelo
    {
       
        

        SajatLista<DateTime, Feladat> Feladatok;

        SajatLista<int, Beosztott> MegolasokListaja = new SajatLista<int, Beosztott>();

        SajatLista<int, SajatLista<int, Beosztott>> feladatmegldasLista = new SajatLista<int, SajatLista<int, Beosztott>>();

        

        public BeosztasKezelo(Megbizas aktivmegbizas)
        {
            
            Feladatok = aktivmegbizas.FeladatLista;
        }
        
   //A következő metódus paraméterként megkapja az adott feladat elvégzéséhez még rendelkezésre álló beosztottak listáját
   //valamint a feladat időigényét
        public SajatLista<int, Beosztott> beosztasKeszito(SajatLista<int,Beosztott> beosztottak, int sum)
        {

            Console.WriteLine("\n A jelenlegi teljesítési ideje az alábbi feladatnak: "+sum +" óra.");
           

            SajatLista<int, SajatLista<int, Beosztott>> megoldasokListaja = new SajatLista<int, SajatLista<int, Beosztott>>();

            
            ListaElem<int, Beosztott> beosztottakAktiv = beosztottak.head;
            ListaElem<DateTime, Feladat> feladatokAktiv = Feladatok.head;

   //Itt ellenőrzésre kerül, hogy a feladat elvégzésekor rendelkezésre álló beosztott lista össz munkaképessége eléri-e a feladat elvégzéséhez szükséges időigényt, 
   //ha nem akkor nincs már elegendő emberünk a feladat elvégzéséhez
   //ez a lehetőséget NincsElegendoEmberException-ként kerül kezelésre
            int összmunkaképesség = 0;
            while(beosztottakAktiv.next!=null)
            {
                összmunkaképesség = összmunkaképesség + beosztottakAktiv.next.value.munkakepesseg;
                beosztottakAktiv = beosztottakAktiv.next;
                
            }
            
            if (összmunkaképesség<sum)
            {
                throw new NincsElegendoEmberException(" Nincs elegendő ember a feladat teljesítéséhez!");
            }

    //int x: az összes lehetséges beosztottkombináció száma
                int x = (int)Math.Pow(2, beosztottak.size());

            
    //minden beosztottkombinációhoz ellenőrzésre kerül, hogy a munkaképességük összege kiadja-e a szükséges időigényt. 
    //Ehhez mehívásra kerül a beosztasGyüjtő metódus.
                for(int i = 0; i<x; i++)
                {
                     SajatLista<int, Beosztott> egyMegoldas =  beosztasGyujto(beosztottak, i, sum);

    //Ha  a beosztasGyujto metódus ad visszatérésként egy helyes megoldást, kiszamolasra kerül a megoldasban szereplő beosztottak összértékelése 
    //és ezt az összértékelést kulcsként használva bekerül a megoldás a megoldasokListaja listába.
                    if (egyMegoldas != null)
                    {
                    
                         ListaElem<int, Beosztott> current1 = egyMegoldas.head;
                         int ertekelesOsszeg = 0;
                         while (current1.next != null)
                         {
                             ertekelesOsszeg = ertekelesOsszeg + current1.next.value.ertekeles;
                             current1 = current1.next;
                         }
                    
                          megoldasokListaja.Beszur(ertekelesOsszeg, egyMegoldas);



                    }
                
                
                }

            
     //mivel a megoldasokListaja lista is kulcs szerint növekedve tartalmazza az elemeket, 
     //ezért a legnagyobb összértékelésű megoldás a lista utolsó eleme lesz:
            ListaElem < int,SajatLista<int, Beosztott> > current2 = megoldasokListaja.head;
            int size = megoldasokListaja.size();
            
            int index = 0;

            while(current2.next!=null)
            {
                if(index == size-1)
                {
                    return current2.next.value;
                }
                current2 = current2.next;
                index++;
            }

            return null;

        }

       

//A beosztasGyujto metódus ellenőrzi, hogy egy adott beosztott kombinació munkaképességének összege kiadja-e
//a feladat elvégzéséhez szükséges időigényt, ha igen visszadja a feladatot, ha nem null értékkel tér vissza:
        public SajatLista<int, Beosztott> beosztasGyujto(SajatLista<int, Beosztott> allista, int n, int sum)
        {
            MegolasokListaja = new SajatLista<int, Beosztott>();

            ListaElem<int, Beosztott> beosztottakAktiv = allista.head;
            ListaElem<DateTime, Feladat> feladatokAktiv = Feladatok.head;

            int[] x = new int[allista.size()];
            int j = allista.size() - 1;


 //binárisan felírásra kerül, hogy az összes rendelkezésre álló beosztott közül melyik szerepel és melyik nem az adott beosztottkombinációban 
            while(n>0)
            {
                x[j] = n % 2;
                n = n / 2;
                j--;
            }
            

//a bináris felírás alapján összeadjuk azoknak a beosztottaknak a munkaképességét, amelyek szerepelnek az adott kombinációban

            int osszeg = 0;

            for (int i = 0; i < allista.size(); i++)
            {
              
                if (x[i]==1)
                {
                    osszeg = osszeg + allista.get(i).munkakepesseg;
                }
            }
 //amennyiben az utóbbi összeg egyenlő a feladat időigényével abban az esetben ez a kombináció, a feladat egy megoldása
 //Amennyiben a kombináció megoldás a metódus visszaad egy listát a benne szereplő beosztottakból
 //Amennyiben a kombináció nem megoldás a metódus null értékkel tér vissza
            if (osszeg == sum)
            {
                


                SajatLista<int, Beosztott> egyMegoldas = new SajatLista<int, Beosztott>();

                for (int i = 0; i < allista.size(); i++)
                {
                    if(x[i]==1)
                    {
                        

                        MegolasokListaja.Beszur(allista.get(i).munkakepesseg, allista.get(i)) ;

                        egyMegoldas.Beszur(allista.get(i).munkakepesseg, allista.get(i));


                    }
                    
                }

               
                return egyMegoldas;
                

            }


            return null;
            

        }

       
        

        
        
    }
}
