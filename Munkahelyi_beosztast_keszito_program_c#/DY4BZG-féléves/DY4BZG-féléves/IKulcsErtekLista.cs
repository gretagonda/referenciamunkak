﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DY4BZG_féléves
{
    interface IKulcsErtekLista<K,T>
    {
        void Beszur(K kulcs, T tartalom);

        T KulcsAlapjanKeres(K kulcs);

        K AdottTartalomKulcsa(T tartalom);

        void KulcsAlapjanTorol(K kulcs);
        void TartalomAlapjanTorol(T tartalom);

        T[] TombkentATartalmak();
    }
}
