﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DY4BZG_féléves
{
    class NotFoundException:Exception
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
