﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DY4BZG_féléves
{

//A kulcs-érték lista egy elme:
    class ListaElem<K,T>
    {
        public T value;

        public K key;

        public ListaElem<K, T> next;

        public ListaElem(K key, T value, ListaElem<K, T> next)
        {
            this.value = value;
            this.key = key;
            this.next = next;
        }
        public ListaElem()
        {

        }


    }
}
