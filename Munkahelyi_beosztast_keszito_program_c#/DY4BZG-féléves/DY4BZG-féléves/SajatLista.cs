﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Collections;

namespace DY4BZG_féléves
{

 // Ennek az osztálynak a céja a kulcs-érték listák létrehozás és kezelése
    class SajatLista<K, T> : IKulcsErtekLista<K, T> where K : IComparable
    {

        public ListaElem<K, T> head = new ListaElem<K, T>();
        public int count = 1;

 //Ez a metódus visszaadja a lista méretét, amit a listán való végig iterálással határoz meg:       
        public int size()
        {
            int counter = 0;

            if (head.next == null)
            {
                return 0;
            }


            ListaElem<K,T> current = head;
            while (current.next != null)
            {
                counter++;
                current = current.next;
            }

            return counter;
        }

//A következő metódus a lista i-edik elemét adja vissza, amit szintén a listán való iterálással kap meg:
        public T get(int key) {
            int i = 0;
            ListaElem<K, T> active = head.next;
            
                while (i != key)
                {
                    active = active.next;
                    i++;
                }
            
           
            return active.value;

        }


//A metódus visszaadja az adott tartalomhoz tartozó kulcsot:
        public K AdottTartalomKulcsa(T tartalom)
        {
            ListaElem<K,T> current = head;

            while (current.next != null)
            {
                if (current.next.value.Equals(tartalom))
                {
                    return current.next.key;
                }
                current = current.next;
            }
            throw new NotFoundException("Nincs ilyen elem");

        }

 //A metódus úgy szúr be egy ListaElemet, hogy a kapott listában az elemek kulcs szerint növekvő sorrenben legyenek:      
        public void Beszur(K kulcs, T tartalom)
        {
            ListaElem<K,T> current = head;

            ListaElem<K,T> newNode = new ListaElem<K,T>(kulcs, tartalom, null);


            if (head.next == null)
            {
                head.next = newNode;
            }
            else
            {
                while (current.next != null)
                {
                    if (current.next.key.CompareTo(kulcs) > 0)
                    {

                        newNode.next = current.next;
                        current.next = newNode;
                        count++;
                        return;

                    }
                    current = current.next;
                }
                current.next = newNode;
            }
        }

//A metódus visszadja az adott kulcshoz tartozó tartalmat:
        public T KulcsAlapjanKeres(K kulcs)
        {
            ListaElem<K,T> current = head;

            while (current.next != null)
            {
                if (current.next.key.Equals(kulcs))
                {
                    return current.next.value;
                }
                current = current.next;
            }
            throw new NotFoundException("Nincs ilyen elem");
        }

//A metódus kitörli az adott kulcshoz tartozó ListaElemet        
        public void KulcsAlapjanTorol(K kulcs)
        {

            ListaElem<K,T> current = head;

            while (current.next != null)
            {
                if (current.next.key.Equals(kulcs))
                {
                    ListaElem<K,T> toremove = current.next;
                    current.next = toremove.next;
                    count--;
                    return;
                }
                current = current.next;
            }
            throw new NotFoundException("Nincs ilyen elem");
        }

 //A metódus kitörli az adott tartalomhoz tartozó ListaElemet 
        public void TartalomAlapjanTorol(T tartalom)
        {


            ListaElem<K,T> current = head;

            while (current.next != null)
            {
                if (current.next.value.Equals(tartalom))
                {
                    ListaElem<K,T> toremove = current.next;
                    current.next = toremove.next;
                    count--;
                    return;
                }
                current = current.next;
            }
            throw new NotFoundException("Nincs ilyen elem");
        }

//A metódus a lista elmeit egy tömben adja vissza:
        public T[] TombkentATartalmak()
        {
            if (size() == 0)
            {
                return new T[0];
            }

            T[] arr = new T[size()];

            int counter = 0;
            ListaElem<K,T> current = head;
            while (current.next != null)
            {
                arr[counter] = current.next.value;
                current = current.next;
                counter++;
            }
            return arr;

        }

       
    }

   
}
